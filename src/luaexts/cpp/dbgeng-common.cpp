#include "stdafx.h"

#include "dbgeng-common.h"
#include <lua.hpp>

namespace
{
    char const * const dbgeng_global = "dbgeng";
}

namespace dbgeng
{
    // [-0,+3]
    // adds an HRESULT to the stack as a standard Lua error: nil, message, error code.
    // pushes:
    //   nil
    //   text version of HRESULT
    //   numeric value of HRESULT
    // returns:
    //   number of values pushed onto the stack
    int xlua_push_hresult_error( lua_State * L, HRESULT hr )
    {
        lua_pushnil( L );
        lua_pushstring( L, hr_string( hr ) );
        lua_pushinteger( L, hr );
        return 3;
    }

    // [-0,+1]
    // pushes:
    //   global dbgeng table
    void xlua_get_dbgeng_table( lua_State * L )
    {
        int type = lua_getglobal( L, dbgeng_global );
        if ( LUA_TTABLE != type )
        {
            lua_pop( L, 1 );
            lua_newtable( L );
            lua_pushvalue( L, -1 );
            lua_setglobal( L, dbgeng_global );
        }
    }

    // [-0,+1]
    // pushes:
    //   table at the named index of the global dbgeng table; creates one if value at the index isn't a table
    void xlua_get_dbgeng_subtable( lua_State * L, char const * const subtable )
    {
        xlua_get_dbgeng_table( L );
        luaL_getsubtable( L, -1, subtable );
        lua_remove( L, -2 );
    }

    // [-0,+0]
    // registers enum values in the array arr into the table at the top of the stack
    void xlua_set_enums( lua_State * L, enum_mapping const * arr )
    {
        if ( nullptr == arr )
            luaL_error( L, "enum array is null" );

        for ( auto elem = arr; nullptr != elem->name; ++elem )
        {
            lua_pushinteger( L, elem->value );
            lua_setfield( L, -2, elem->name );
        }
    }

    // [-0,+0]
    // within the global 'dbgeng' table, sets a table under the key 'hr' that
    // contains all of the commonly used HRESULTS names as keys, with their numeric
    // values as values.
    // for example: dbgeng.hr = { S_OK = 0x0, S_FALSE = 0x1, ... };
    void xlua_init_hresults( lua_State * L )
    {
        HRESULT hrcodes[] =
        {
            S_OK, S_FALSE, E_FAIL, E_INVALIDARG, E_NOINTERFACE, E_OUTOFMEMORY, E_UNEXPECTED, E_NOTIMPL,
            HRESULT_FROM_WIN32( ERROR_ACCESS_DENIED ),
        };

        xlua_get_dbgeng_subtable( L, "hr" );
        for ( auto hr : hrcodes )
        {
            lua_pushinteger( L, hr );
            lua_setfield( L, -2, hr_string( hr ) );
        }
        lua_pop( L, 1 );
    }

    // [-0,+0]
    // within the global 'dbgeng' table, sets a table under the key 'symtag' that
    // contains all of the values from the SymTagEnum enumeration
    void xlua_init_symtags( lua_State * L )
    {
        xlua_get_dbgeng_subtable( L, "symtag" );

        lua_pushinteger( L, SymTagNull );
        lua_setfield( L, -2, "Null" );
        lua_pushinteger( L, SymTagExe );
        lua_setfield( L, -2, "Exe" );
        lua_pushinteger( L, SymTagCompiland );
        lua_setfield( L, -2, "Compiland" );
        lua_pushinteger( L, SymTagCompilandDetails );
        lua_setfield( L, -2, "CompilandDetails" );
        lua_pushinteger( L, SymTagCompilandEnv );
        lua_setfield( L, -2, "CompilandEnv" );
        lua_pushinteger( L, SymTagFunction );
        lua_setfield( L, -2, "Function" );
        lua_pushinteger( L, SymTagBlock );
        lua_setfield( L, -2, "Block" );
        lua_pushinteger( L, SymTagData );
        lua_setfield( L, -2, "Data" );
        lua_pushinteger( L, SymTagAnnotation );
        lua_setfield( L, -2, "Annotation" );
        lua_pushinteger( L, SymTagLabel );
        lua_setfield( L, -2, "Label" );
        lua_pushinteger( L, SymTagPublicSymbol );
        lua_setfield( L, -2, "PublicSymbol" );
        lua_pushinteger( L, SymTagUDT );
        lua_setfield( L, -2, "UDT" );
        lua_pushinteger( L, SymTagEnum );
        lua_setfield( L, -2, "Enum" );
        lua_pushinteger( L, SymTagFunctionType );
        lua_setfield( L, -2, "FunctionType" );
        lua_pushinteger( L, SymTagPointerType );
        lua_setfield( L, -2, "PointerType" );
        lua_pushinteger( L, SymTagArrayType );
        lua_setfield( L, -2, "ArrayType" );
        lua_pushinteger( L, SymTagBaseType );
        lua_setfield( L, -2, "BaseType" );
        lua_pushinteger( L, SymTagTypedef );
        lua_setfield( L, -2, "Typedef" );
        lua_pushinteger( L, SymTagBaseClass );
        lua_setfield( L, -2, "BaseClass" );
        lua_pushinteger( L, SymTagFriend );
        lua_setfield( L, -2, "Friend" );
        lua_pushinteger( L, SymTagFunctionArgType );
        lua_setfield( L, -2, "FunctionArgType" );
        lua_pushinteger( L, SymTagFuncDebugStart );
        lua_setfield( L, -2, "FuncDebugStart" );
        lua_pushinteger( L, SymTagFuncDebugEnd );
        lua_setfield( L, -2, "FuncDebugEnd" );
        lua_pushinteger( L, SymTagUsingNamespace );
        lua_setfield( L, -2, "UsingNamespace" );
        lua_pushinteger( L, SymTagVTableShape );
        lua_setfield( L, -2, "VTableShape" );
        lua_pushinteger( L, SymTagVTable );
        lua_setfield( L, -2, "VTable" );
        lua_pushinteger( L, SymTagCustom );
        lua_setfield( L, -2, "Custom" );
        lua_pushinteger( L, SymTagThunk );
        lua_setfield( L, -2, "Thunk" );
        lua_pushinteger( L, SymTagCustomType );
        lua_setfield( L, -2, "CustomType" );
        lua_pushinteger( L, SymTagManagedType );
        lua_setfield( L, -2, "ManagedType" );
        lua_pushinteger( L, SymTagDimension );
        lua_setfield( L, -2, "Dimension" );
        lua_pushinteger( L, SymTagCallSite );
        lua_setfield( L, -2, "CallSite" );

        lua_pop( L, 1 );
    }

    struct enum_mapping const outctl[] =
    {
        { "THIS_CLIENT"       , DEBUG_OUTCTL_THIS_CLIENT       },
        { "ALL_CLIENTS"       , DEBUG_OUTCTL_ALL_CLIENTS       },
        { "ALL_OTHER_CLIENTS" , DEBUG_OUTCTL_ALL_OTHER_CLIENTS },
        { "IGNORE"            , DEBUG_OUTCTL_IGNORE            },
        { "LOG_ONLY"          , DEBUG_OUTCTL_LOG_ONLY          },
        { "SEND_MASK"         , DEBUG_OUTCTL_SEND_MASK         },
        { "NOT_LOGGED"        , DEBUG_OUTCTL_NOT_LOGGED        },
        { "OVERRIDE_MASK"     , DEBUG_OUTCTL_OVERRIDE_MASK     },
        { "DML"               , DEBUG_OUTCTL_DML               },
        { "AMBIENT_DML"       , DEBUG_OUTCTL_AMBIENT_DML       },
        { "AMBIENT_TEXT"      , DEBUG_OUTCTL_AMBIENT_TEXT      },
        { "AMBIENT"           , DEBUG_OUTCTL_AMBIENT           },
        { nullptr, 0 }
    };

    // [-0,+0]
    // initialize values used throughout dbgeng api
    void xlua_init( lua_State * L )
    {
        // named hresult values
        xlua_init_hresults( L );

        // symtag values
        xlua_init_symtags( L );

        // push dbgeng table
        xlua_get_dbgeng_table( L );

        // setup the table of outctl values
        luaL_getsubtable( L, -1, "outctl" );
        xlua_set_enums( L, outctl );
        lua_pop( L, 1 );

        // ANY_ID
        lua_pushinteger( L, DEBUG_ANY_ID );
        lua_setfield( L, -2, "ANY_ID" );

        // INVALID_OFFSET
        lua_pushinteger( L, DEBUG_INVALID_OFFSET );
        lua_setfield( L, -2, "INVALID_OFFSET" );

        // pop dbgeng table
        lua_pop( L, 1 );
    }

    // returns a string literal, with a text version of the given HRESULT
    char const * const hr_string( HRESULT hr )
    {
        switch ( hr )
        {
        case S_OK          : return "S_OK"          ;
        case S_FALSE       : return "S_FALSE"       ;
        case E_FAIL        : return "E_FAIL"        ;
        case E_INVALIDARG  : return "E_INVALIDARG"  ;
        case E_NOINTERFACE : return "E_NOINTERFACE" ;
        case E_OUTOFMEMORY : return "E_OUTOFMEMORY" ;
        case E_UNEXPECTED  : return "E_UNEXPECTED"  ;
        case E_NOTIMPL     : return "E_NOTIMPL"     ;
        }

        if ( HRESULT_FROM_WIN32( ERROR_ACCESS_DENIED ) == hr )
            return "ERROR_ACCESS_DENIED";

        return "UNKNOWN HRESULT";
    }

}
