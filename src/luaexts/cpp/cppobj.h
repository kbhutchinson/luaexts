// C++ <--> Lua interop for Lua userdata representing C++ objects in the debug target's memory

#pragma once

#include <lua.hpp>

#include "luaexts.h"
#include "util.h"

struct lua_State;

// Functions and metamethods for dealing with C++ objects
namespace cppobj
{
    // kinds of types
    enum class kind_t
    {
        unknown_,
        base_,
        enum_,
        class_,
        function_,
        pointer_,
        array_,
    };

    std::string kind_tostring( kind_t k );

    // a cppobj is essentially a pointer that Lua holds onto to keep track of the debug engine
    // C++ object representations being used
    typedef EXT_CLASS::registered_object_t cppobj_t;

    // check that the value at the given index holds a cppobj and, if so, return it
    // never returns if the value isn't a cppobj
    cppobj_t * check_cppobj( lua_State * L, int index );
    // same as above, but return null on type fail instead of causing an error
    cppobj_t * test_cppobj( lua_State * L, int index );

    // check that a given index holds a cppobj and, if so, return the underlying object it points to
    auto check_and_deref_cppobj( lua_State * L, int index ) -> decltype( util::declval< cppobj_t >().lock() );
    // check that a given index holds a cppobj and, if so, return the underlying object it points to
    // if not, return an empty object
    auto test_and_deref_cppobj( lua_State * L, int index ) -> decltype( util::declval< cppobj_t >().lock() );

    // create a new cppobj userdatum from the given ExtRemoteTyped, leaving it on top of the stack
    void push_cppobj( lua_State * L, std::string name, EXT_CLASS::registered_object_t::element_type const & object );

    // pops the c++ expression string on the top of the stack, and
    // creates and pushes a userdatum from the c++ expression
    int new_cppobj( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes the object's C++ type as a string
    int type( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes the object's "kind" as a string
    int kind( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes the object's short name, usually just the member's name
    int name( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes the object's long name, usually an expression valid in the current scope
    int long_name( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes the object's C++ size
    int size_of( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes a new cppobj representing a pointer to the object
    int address_of( lua_State * L );

    // pops the cppobj on the top of the stack, which should be a pointer, and
    // pushes a new cppobj representing the object being pointed at
    int dereference( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes a table with a list of the object's members
    //   if base_classes is true, only baseclass representations of this object are included
    //     otherwise only data members are included
    //   if direct_only is true, only direct members of this class are included
    //     otherwise, all inherited members are included as well
    int push_member_list( lua_State * L, bool base_classes, bool direct_only );

    // pops the cppobj on the top of the stack, and
    // if the cppobj represents a class instance, pushes a table holding a list of its data members
    // if the cppobj represents any other kind of C++ variable or data, pushes nil
    int members( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // if the cppobj represents a class instance, pushes a table holding a list of base classes representations
    // if the cppobj represents any other kind of C++ variable or data, pushes nil
    int bases( lua_State * L );

    // pops the cppobj at index -2 and argument at index -1, and:
    // for number arguments, treats the object as an array and pushes the specified element
    // for string arguments, pushes the named subfield
    int index( lua_State * L );

    // pops the cppobj on the top of the stack, and:
    // for fundamental types, pushes the object's value as a Lua value
    // for other types, pushes nil
    int flatten( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // pushes the dbgeng string interpretation of the object's value
    int tostring( lua_State * L );

    // for the cppobj at index -2 and argument at index -1:
    // performs one iteration of pairs by
    //  popping the argument at index -1, and
    //  pushing the next key index, key name, and value
    // the cppobj on the stack stays on the stack
    int pairs_iterate( lua_State * L );

    // replaces a cppobj on the top of the stack with:
    //  index -1: nil
    //  index -2: the cppobj
    //  index -3: an iterator function
    // used to start iteration of the object's fields
    int pairs( lua_State * L );

    // subtraction
    // index -2 is the first operand, index -1 is the second
    // pop the operands and push the result
    int sub( lua_State * L );

    // equality
    // index -2 is the first operand, index -1 is the second
    // pop the operands and push the result
    int eq( lua_State * L );

    // pops the cppobj on the top of the stack, and
    // destructs the C++ side of the object
    int gc( lua_State * L );

    // register the functions used by cppobj
    int register_funcs( lua_State * L );

} // namespace cppobj
