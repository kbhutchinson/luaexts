--[[
-- Definitions for cppobj objects that present a Lua representation of a C++ object from the debug engine.
--]]

local luaexts = _ENV[ EXTNAME ]

-- finds the index of a symbol within a symbol group by matching memory offset and type_id
-- params:
--   symgrp: symbol group to search
--   offset: memory offset of the symbol
--   type_id: type id of the symbol within its module
-- returns:
--   zero-based index of the symbol within the symbol group, or nil if not found
local function find_symbol_index( symgrp, offset, type_id  )
    for i = 0, symgrp:get_number_symbols() - 1 do
        local se = symgrp:get_symbol_entry_information( i )
        if se and se.offset == offset and se.type_id == type_id then
            return i
        end
    end

    return nil
end

-- is_baseclass( symgrp, index )
--
-- Is the given symbol in the symbol group a baseclass?
-- params:
--   symgrp: symbol group to check
--   index: index of the symbol to check
-- returns:
--   true if the symbol is a baseclass, false if not
local function is_baseclass( symgrp, index )
    -- baseclasses appear as a symbol with the same name as its type
    local symname = symgrp:get_symbol_name( index )
    local typname = symgrp:get_symbol_type_name( index )
     -- for some reason, the above call to GetSymbolTypeName() will sometimes mess up the symbol group, but calling GetNumberSymbols() seems to fix it
    symgrp:get_number_symbols()

    typname = typname:gsub( '^class ', '' ):gsub( '^struct ', '' )
    return symname == typname
end

-- is_vtable( symgrp, index )
--
-- Is the given symbol in the symbol group a vtable pointer?
-- params:
--   symgrp: symbol group to check
--   index: index of the symbol to check
-- returns:
--   true if the symbol is a vtable ptr, false if not
local function is_vtable( symgrp, index )
    local symname = symgrp:get_symbol_name( index )
    return '__vfptr' == symname
end

-- tag_to_kind( tag )
--
-- Convert a SymTagEnum to a kind string.
-- params:
--   tag: SymTagEnum value
-- returns:
--   the kind of the symtag
local function tag_to_kind( tag )
    if tag == dbgeng.symtag.BaseType then
        return 'base'
    end
    if tag == dbgeng.symtag.Enum then
        return 'enum'
    end
    if tag == dbgeng.symtag.UDT then
        return 'class'
    end
    if tag == dbgeng.symtag.FunctionType then
        return 'function'
    end
    if tag == dbgeng.symtag.PointerType then
        return 'pointer'
    end
    if tag == dbgeng.symtag.ArrayType then
        return 'array'
    end

    return 'unknown'
end


--[[ private data ]]--

local weakkeys = { __mode = 'k' }
-- symbol group holding the symbol
local p_symgrp = setmetatable( {}, weakkeys )
-- memory offset of the symbol
local p_offset = setmetatable( {}, weakkeys )
-- module the type is found in
local p_type_module = setmetatable( {}, weakkeys )
-- type id of the symbol
local p_type_id = setmetatable( {}, weakkeys )
-- size of the symbol
local p_size = setmetatable( {}, weakkeys )
-- string name of the type
local p_typename = setmetatable( {}, weakkeys )
-- kind of the type
local p_kind = setmetatable( {}, weakkeys )
-- short name, usually just the variable or field name
local p_short_name = setmetatable( {}, weakkeys )
-- long name, which should be a fully qualified expression valid for the current scope
local p_long_name = setmetatable( {}, weakkeys )
-- does this object have members? nil means we haven't checked yet
local p_has_members = setmetatable( {}, weakkeys )
-- data members not inherited
local p_direct_data_members = setmetatable( {}, weakkeys )
-- data members inherited from base classes
local p_inherited_data_members = setmetatable( {}, weakkeys )
-- base class representations of this object
local p_base_classes = setmetatable( {}, weakkeys )


--[[ class method declarations ]]--

-- identification and creation
local is_cppobj
local new_from_symbol_group_entry
local new

-- property accessors
local kind_func
local long_name_func
local offset_func
local short_name_func
local size_func
local type_func
local type_id_func
local type_module_func

-- other public methods
local address_of
local bases
local dereference
local members
local value

-- private methods
local symidx
local flushidxs

local cache_members
local expand
local long_name
local reseat
local safe_long_name
local short_name


--[[ metamethods ]]--

local cppobj_metatable =
{
    __name = EXTNAME .. '.cppobj',
}

-- tostring( obj )
--
-- Returns the dbgeng interpretation of the object as a string.
-- params:
--   obj: cppobj to stringify
-- returns
--   dbgeng interpretation of the object as a string
function cppobj_metatable.__tostring( obj )
    local grp = p_symgrp[ obj ]
    local idx = symidx( obj )
    return grp:get_symbol_value_text( idx )
end

-- obj.member_name
--
-- Retrieves an object data member directly.
-- params:
--   obj: cppobj to retrieve the member for
--   member_name: member to retrieve
-- returns
--   requested data member as a cppobj
function cppobj_metatable.__index( obj, member_name )
    local elemidx = tonumber( member_name )
    if elemidx ~= nil then
        local objkind = p_kind[ obj ]
        if 'array' ~= objkind and 'pointer' ~= objkind then
            error( string.format( "kind of '%s' is not an array or pointer for array indexing", p_long_name[ obj ] ), 2 )
        end

        return new( safe_long_name( obj ) .. '[' .. elemidx .. ']' )

    else
        local data_members = members( obj )
        -- search the list backwards so that members from derived classes
        -- are found before members with the same name from base classes
        for i = #data_members, 1, -1 do
            local member = data_members[ i ]
            if member_name == p_short_name[ member ] then
                return member
            end
        end

        return nil, string.format( "'%s' does not have a member '%s'", p_long_name[ obj ], member_name )
    end
end

-- op1 == op2
--
-- Equality comparisons with C++ objects. The following rules are applied:
--  * arrays are treated as pointers
--  * pointers, base, and enum types are compared as numbers
--  * class types are considered equal if two instances represent the same type at the same memory offset
-- params:
--  op1: cppobj or Lua number
--  op2: cppobj or Lua number
-- returns:
--   true if operands are considered equal according to the rules above; false otherwise
function cppobj_metatable.__eq( op1, op2 )
    local op1_iscppobj = cppobj_metatable == getmetatable( op1 )
    local op2_iscppobj = cppobj_metatable == getmetatable( op2 )

    -- treat arrays like pointers
    if op1_iscppobj and 'array' == p_kind[ op1 ] then
        op1 = address_of( dereference( op1 ) )
    end
    if op2_iscppobj and 'array' == p_kind[ op2 ] then
        op2 = address_of( dereference( op2 ) )
    end

    local op1_kind
    local op2_kind
    local op1_type
    local op2_type
    if op1_iscppobj then
        op1_kind = p_kind[ op1 ]
        op1_type = p_typename[ op1 ]
    end
    if op2_iscppobj then
        op2_kind = p_kind[ op2 ]
        op2_type = p_typename[ op2 ]
    end

    -- both operands are cppobjs
    if op1_iscppobj and op2_iscppobj then

        if 'pointer' == op1_kind or 'base' == op1_kind or 'enum' == op1_kind then

            -- both operands can be compared as numbers
            if 'pointer' == op2_kind or 'base' == op2_kind or 'enum' == op2_kind then
                local op1_val = value( op1 )
                local op2_val = value( op2 )
                return op1_val == op2_val

            -- incompatible kinds for comparison
            else
                return false
            end

        elseif 'class' == op1_kind then

            -- both operands can be compared as classes
            if 'class' == op2_kind then
                local op1_offset = p_offset[ op1 ]
                local op2_offset = p_offset[ op2 ]
                return op1_offset == op2_offset and op1_type == op2_type

            -- incompatible kinds for comparison
            else
                return false
            end

        -- unsupported kind for equality
        else
            return false
        end

    -- only op1 is a cppobj
    elseif op1_iscppobj then

        -- op1 can be compared is a number
        if 'pointer' == op1_kind or 'base' == op1_kind or 'enum' == op1_kind then
            local op1_val = value( op1 )
            return op1_val == op2

        -- incompatible kinds for comparison
        else
            return false
        end

    -- only op2 is a cppobj
    elseif op2_iscppobj then

        -- op2 can be compared is a number
        if 'pointer' == op2_kind or 'base' == op2_kind or 'enum' == op2_kind then
            local op2_val = value( op2 )
            return op1 == op2_val

        -- incompatible kinds for comparison
        else
            return false
        end
    end

    -- neither operand is a cppobj?
    return false
end

-- op1 - op2
--
-- Subtraction with C++ objects. The following rules are applied:
--  * if the 1st operand is a cppobj pointer or array, the 2nd operand can be:
--     * cppobj pointer or array of the same type; result is the scaled distance between the two memory locations
--     * cppobj base integral type; result is a new pointer, offset by the given scaled distance
--     * Lua integer; result is a new pointer, offset by the given scaled distance
--  * if the 1st operand is a cppobj base type or enum, the 2nd operand can be:
--     * cppobj base type or enum or Lua number; result is the difference of the two operands
--  * if the 1st operand is a Lua value, the 2nd operand can be:
--     * cppobj base type or enum or Lua number; result is the difference of the two operands
-- params:
--   op1: cppobj or Lua number
--   op2: cppobj or Lua number
-- returns:
--   If the result is a pointer, it will be a cppobj. If the result is a number, it will be a normal Lua number.
function cppobj_metatable.__sub( op1, op2 )
    local op1_iscppobj = cppobj_metatable == getmetatable( op1 )
    local op2_iscppobj = cppobj_metatable == getmetatable( op2 )

    -- treat arrays like pointers
    if op1_iscppobj and 'array' == p_kind[ op1 ] then
        op1 = address_of( dereference( op1 ) )
    end
    if op2_iscppobj and 'array' == p_kind[ op2 ] then
        op2 = address_of( dereference( op2 ) )
    end

    local op1_kind
    local op2_kind
    local op1_type
    local op2_type
    if op1_iscppobj then
        op1_kind = p_kind[ op1 ]
        op1_type = p_typename[ op1 ]
    end
    if op2_iscppobj then
        op2_kind = p_kind[ op2 ]
        op2_type = p_typename[ op2 ]
    end

    -- both operands are cppobjs
    if op1_iscppobj and op2_iscppobj then

        -- op1 is pointer
        if 'pointer' == op1_kind then

            -- ptr - ptr: scaled distance
            if 'pointer' == op2_kind then
                if op1_type ~= op2_type then
                    error( string.format( 'types are not the same in pointer subtraction: %s, %s', op1_type, op2_type ), 2 )
                end
                local diff = value( op1 ) - value( op2 )
                local scaled = diff // p_size[ dereference( op1 ) ]
                return scaled

            -- ptr - integral: pointer to offset location using scaled distance
            elseif 'base' == op2_kind then
                if op2_type:find( '%f[%a]double%f[%A]' ) or op2_type:find( '%f[%a]float%f[%A]' ) then
                    error( string.format( 'only integral arithmetic is supported with pointers; attempting to subtract \'%s\' from \'%s\'', op2_type, op1_type ) )
                end
                local op2_val = value( op2 )
                return address_of( op1[ - op2_val ] )

            -- ptr - other: nil
            else
                return nil
            end

        -- op1 is base or enum
        elseif 'base' == op1_kind or 'enum' == op1_kind then

            -- base/enum - base/enum: difference
            if 'base' == op2_kind or 'enum' == op2_kind then
                local op1_val = value( op1 )
                local op2_val = value( op2 )
                return op1_val - op2_val

            -- base/enum - other: nil
            else
                return nil

            end

        -- no support for other cppobj kinds in 1st operand
        else
            return nil
        end

    -- only op1 is a cppobj
    elseif op1_iscppobj then

        -- ptr - integral: pointer to offset location using scaled distance
        if 'pointer' == op1_kind then
            local op2_int = math.tointeger( op2 )
            if not op2_int then
                error( string.format( 'only integral arithmetic is supported with pointers; attempting to subtract \'%s\' from \'%s\'', op2, op1_type ) )
            end
            return address_of( op1[ - op2 ] )

        -- base/enum - number: difference
        elseif 'base' == op1_kind or 'enum' == op1_kind then
            local op1_val = value( op1 )
            return op1_val - op2

        -- no support for other cppobj kinds in 1st operand
        else
            return nil
        end

    -- only op2 is a cppobj
    elseif op2_iscppobj then
        -- op1 should be a regular Lua value, so we only attempt regular subtraction
        if 'base' == op2_kind or 'enum' == op2_kind then
           local op2_val = value( op2 )
           return op1 - op2_val

        -- no support for other cppobj kinds in 2st operand
        else
           return nil
        end
    end

    -- neither operand is a cppobj?
    return nil
end


--[[ class methods ]]--

-- is_cppobj( value )
--
-- Is the given value a cppobj?
-- params:
--   value: value to check
-- returns:
--   true if the value's metatable is the cppobj class metatable; false otherwise
function is_cppobj( value )
    return getmetatable( value ) == cppobj_metatable
end

-- new_from_symbol_group_entry( symgrp, index, parentobj )
--
-- Create a Lua projection for a C++ object, using an entry from the given symbol group.
-- params:
--   symgrp: symbol group containing the symbol
--   index: index of the symbol in the group
--   parentobj: cppobj containing this new object, if any
--   class_reps: list of class representations processed so far of the original parent
-- returns:
--   new cppobj
function new_from_symbol_group_entry( symgrp, index, parentobj, class_reps )
    local info, hr, hrcode = symgrp:get_symbol_entry_information( index )
    if hrcode == dbgeng.hr.E_NOINTERFACE then
        -- sometimes symbol data cannot be fetched
        return nil
    end
    if not info then
        error( string.format( '%s: index %d', hr, index ), 2 )
    end

    local obj = setmetatable( {}, cppobj_metatable )
    p_symgrp     [ obj ] = symgrp
    p_offset     [ obj ] = info.offset
    p_type_module[ obj ] = info.module
    p_type_id    [ obj ] = info.type_id
    p_size       [ obj ] = info.size
    p_kind       [ obj ] = tag_to_kind( info.tag )
    p_typename   [ obj ] = dbgeng.symbols.get_type_name( info.module, info.type_id )
    p_short_name [ obj ] = short_name( obj, parentobj )
    p_long_name  [ obj ] = long_name( obj, parentobj, class_reps )

    -- simplify processing by never having to worry about nils in the p_*_members values
    p_direct_data_members   [ obj ] = {}
    p_base_classes          [ obj ] = {}
    p_inherited_data_members[ obj ] = {}

    return obj
end

-- new( expr )
--
-- Create a Lua projection for a C++ object, from an expression.
-- params:
--   expr: string containing a C++ expression evaluating to a symbol
-- returns:
--   new cppobj
function new( expr )
    luaexts.arg_type_check( 'cppobj.new', { { 'string', expr } } )

    local symgrp = dbgeng.symbols.create_symbol_group()
    local index, hr, hrcode  = symgrp:add_symbol( expr, dbgeng.ANY_ID )
    if not index then
        error( string.format( '%s: failed to create cppobj for \'%s\'', hr, expr ), 2 )
    end

    return new_from_symbol_group_entry( symgrp, index )
end

-- kind_func( obj )
--
-- Returns an object's kind.
-- params:
--   obj: cppobj to return kind for
-- returns:
--   object's kind as a string
function kind_func( obj )
    return p_kind[ obj ]
end

-- long_name_func( obj )
--
-- Returns an object's long name, which should be a fully qualified expression.
-- that is valid for the current scope
-- params:
--   obj: cppobj to return name for
-- returns:
--   object's long name as a string
function long_name_func( obj )
    return p_long_name[ obj ]
end

-- offset_func( obj )
--
-- Return an object's memory offset.
-- params:
--   obj: cppobj to return offset for
-- returns:
--   object's offset in the target's memory
function offset_func( obj )
    return p_offset[ obj ]
end

-- short_name_func( obj )
--
-- Returns an object's short name, usually the variable or field name.
-- params:
--   obj: cppobj to return name for
-- returns:
--   object's short name as a string
function short_name_func( obj )
    return p_short_name[ obj ]
end

-- size_func( obj )
--
-- Return an object's size.
-- params:
--   obj: cppobj to return size for
-- returns:
--   object's size
function size_func( obj )
    return p_size[ obj ]
end

-- type_func( obj )
--
-- Return an object's type.
-- params:
--   obj: cppobj to return type for
-- returns:
--   object's type as a string
function type_func( obj )
    return p_typename[ obj ]
end

-- type_id_func( obj )
--
-- Returns the type id of an object's type.
-- params:
--   obj: cppobj to return type id for
-- returns:
--   type id of an object's type
function type_id_func( obj )
    return p_type_id[ obj ]
end

-- type_module_func( obj )
--
-- Returns the module that an object's type is from.
-- params:
--   obj: cppobj to return module for
-- returns:
--   module base address of an object's type
function type_module_func( obj )
    return p_type_module[ obj ]
end

-- address_of( obj )
--
-- Returns a new obj representing a pointer to the one given.
-- params:
--   obj: cppobj to create a pointer to
-- returns:
--   new cppobj
function address_of( obj )
    return new( '&' .. safe_long_name( obj ) )
end

-- bases( obj )
--
-- Retrieves an object's base class representations as cppobjs.
-- params:
--   obj: cppobj to retrieve base classes for
-- returns
--   table containing base class representations
function bases( obj )
    cache_members( obj )
    return { table.unpack( p_base_classes[ obj ] ) }
end

-- dereference( obj )
--
-- Given a cppobj representing a pointer, returns a new cppobj representing the pointee.
-- params:
--   obj: pointer cppobj
-- returns:
--   new cppobj
function dereference( obj )
    return new( '*' .. safe_long_name( obj ) )
end

-- members( obj )
--
-- Retrieves an object's data members as cppobjs.
-- params:
--   obj: cppobj to retrieve data members for
-- returns
--   table containing data members
function members( obj )
    cache_members( obj )
    local result = { table.unpack( p_inherited_data_members[ obj ] ) }
    for _, direct_member in ipairs( p_direct_data_members[ obj ] ) do
        table.insert( result, direct_member )
    end
    return result
end

-- value( obj )
--
-- For fundamental types, return the object's value as a Lua value.
-- For other types, return nil.
-- params:
--   obj: cppobj to retrieve the value from
-- returns:
--   object's value as a Lua value
function value( obj )
    local kind = p_kind[ obj ]
    local long_name = p_long_name[ obj ]
    local offset = p_offset[ obj ]
    local size = p_size[ obj ]

    if 'enum' == kind or 'pointer' == kind then
        return dbgeng.extremotedata.new( long_name, offset, size ):get_data( size )
    end
    if 'array' == kind then
        return offset
    end
    if 'base' == kind then
        local typename = p_typename[ obj ]
        if typename:find( '%f[%a]bool%f[%A]' ) then
            return dbgeng.extremotedata.new( long_name, offset, size ):get_std_bool()
        end
        if typename:find( '%f[%a]float%f[%A]' ) then
            return dbgeng.extremotedata.new( long_name, offset, size ):get_float()
        end
        if typename:find( '%f[%a]double%f[%A]' ) then
            return dbgeng.extremotedata.new( long_name, offset, size ):get_double()
        end

        -- other base types should be integral types, so just return the raw data
        return dbgeng.extremotedata.new( long_name, offset, size ):get_data( size )
    end

    return nil
end

do
    local grpcache = setmetatable( {}, weakkeys )

    -- symidx( obj )
    --
    -- Returns the cached index of a symbol in its symbol group.
    -- params:
    --   obj: cppobj to return the index for
    -- returns:
    --   cached index of the symbol in the symbol group
    function symidx( obj )
        local grp = p_symgrp[ obj ]
        local idxcache = grpcache[ grp ]
        if not idxcache then
            idxcache = setmetatable( {}, weakkeys )
            grpcache[ grp ] = idxcache
        end
        local idx = idxcache[ obj ]
        if not idx then
            idx = find_symbol_index( grp, p_offset[ obj ], p_type_id[ obj ]  )
            idxcache[ obj ] = idx
        end
        if idx == nil then
            error( string.format( 'could not locate index for item in %s, offset %x, type id %d', grp, p_offset[ obj ], p_type_id[ obj ] ), 2 )
        end
        return idx
    end

    -- flushidxs( symgrp )
    --
    -- Flushes the cached indexes of symbols in a symbol group.
    -- params:
    --   symgrp: symbol group to flush cached indexes for
    -- returns:
    --   nothing
    function flushidxs( symgrp )
        grpcache[ symgrp ] = nil
    end
end

-- cache_members( obj )
--
-- Creates cppobjs for each of an object's members and caches them in corresponding
-- private tables for easy retrieval by the member retrieval functions.
-- params:
--   obj: cppobj to cache members for
--   original_parent: used in recursive calls to keep track of the cppobj that spawned the call tree
--   class_reps: list of class representations processed so far of the original parent
-- returns:
--   nothing
function cache_members( obj, original_parent, class_reps )
    if p_has_members[ obj ] ~= nil then
        -- members have already been cached
        return
    end

    local has_members = expand( obj )
    p_has_members[ obj ] = has_members
    if not has_members then
        return
    end

    class_reps = class_reps or {}

    local group = p_symgrp[ obj ]

    local directtbl  = p_direct_data_members   [ obj ]
    local basetbl    = p_base_classes          [ obj ]
    local inherittbl = p_inherited_data_members[ obj ]

    -- STEP ONE: expand and cache all of this object's direct members
    local bases = {}
    local idx_cppobj = symidx( obj )
    local count = group:get_number_symbols()
    local symparams = group:get_symbol_parameters( 0, count )
    for idx_cur = 0, count - 1 do
        local idx_parent = symparams[ idx_cur + 1 ].parent_symbol
        if idx_parent == idx_cppobj then
            if is_baseclass( group, idx_cur ) then
                local baseobj = new_from_symbol_group_entry( group, idx_cur, original_parent or obj )
                table.insert( bases, baseobj )

            elseif is_vtable( group, idx_cur ) then
                -- do something with vtables

            else
                local dataobj = new_from_symbol_group_entry( group, idx_cur, obj, class_reps )
                table.insert( directtbl, dataobj )

            end
        end
    end

    class_reps[ #class_reps + 1 ] = obj

    -- STEP TWO: recursively cache base class members
    for _, baseobj in ipairs( bases ) do
        cache_members( baseobj, original_parent or obj, class_reps )
        class_reps[ #class_reps + 1 ] = baseobj

        table.insert( basetbl, baseobj )
        local basebases = p_base_classes[ baseobj ]
        if basebases then
            for _, basebaseobj in ipairs( basebases ) do
                table.insert( basetbl, basebaseobj )
            end
        end

        for _, baseinheritobj in ipairs( p_inherited_data_members[ baseobj ] ) do
            table.insert( inherittbl, baseinheritobj )
        end
        for _, basedirectobj in ipairs( p_direct_data_members[ baseobj ] ) do
            table.insert( inherittbl, basedirectobj )
        end
    end
end

-- expand( obj )
--
-- Expands a symbol in a symbol group.
-- params:
--   obj: cppobj to expand
-- returns:
--   true if symbol has children, false if not
function expand( obj )
    if p_has_members[ obj ] ~= nil then
        return p_has_members[ obj ]
    end

    local kind = p_kind[ obj ]
    if 'class' ~= kind and 'pointer' ~= kind and 'array' ~= kind then
        return false
    end

    local grp = p_symgrp[ obj ]
    local index = symidx( obj )
    local success, hr, hrcode = grp:expand_symbol( index, true )

    if success then
        flushidxs( grp )
        return true

    elseif success == false then
        return false

    elseif success == nil then
        if hrcode == dbgeng.hr.E_INVALIDARG then
            -- this object is at the expansion depth, need to reseat it for expansion
            grp = reseat( obj )
            flushidxs( grp )
            index = symidx( obj )

            -- now try again
            success, hr, hrcode = grp:expand_symbol( index, true )
            if success then
                flushidxs( grp )
                return true

            elseif success == false then
                return false -- no children? why did expansion fail?

            elseif success == nil then
                if hrcode == dbgeng.hr.E_INVALIDARG then
                    error( string.format( "could not successfully expand '%s'", p_long_name[ obj ] ) )
                else
                    error( string.format( "unexpected error when expanding '%s': %s", p_long_name[ obj ], hr ) )
                end
            end

        else
            error( string.format( "unexpected error when expanding '%s': %s", p_long_name[ obj ], hr ) )
        end
    end

    error( string.format( "something strange happened while expanding '%s'", p_long_name[ obj ] ) )
end

-- long_name( obj, parentobj )
--
-- Determine an object's long name.
-- params:
--   obj: cppobj to determine the long name for
--   parentobj: cppobj containing this object, if any
--   class_reps: list of class representations processed so far of the original parent
-- returns
--   long name as a string
function long_name( obj, parentobj, class_reps )
    local name = p_long_name[ obj ]
    if name then
        return name
    end

    local group = p_symgrp[ obj ]
    local index = symidx( obj )
    local is_base = is_baseclass( group, index )
    local shrt_name = short_name( obj, parentobj )
    local need_deref = '*' == shrt_name

    if not parentobj then
        name = shrt_name

    else
        local expr
        if is_base then
            local base_name = group:get_symbol_name( index )
            expr = string.format( '*static_cast<%s*>(&%s)', base_name, p_long_name[ parentobj ] )
        else
            -- if there are any name collisions with other inherited members, the long name will need a base-class cast
            for _, class_rep in ipairs( class_reps ) do
                for _, member in ipairs( p_direct_data_members[ class_rep ] ) do
                    if shrt_name == p_short_name[ member ] then
                        expr = p_long_name[ parentobj ]
                    end
                end
            end
            if not expr then
                expr = p_long_name[ class_reps[ 1 ] or parentobj ]
            end

            -- check parent kind to know whether to use arrow, dot, or subscript
            local parent_kind = p_kind[ parentobj ]
            if 'pointer' == parent_kind then
                if need_deref then
                    expr = string.format( '*(%s)', expr )
                else
                    expr = string.format( '(%s)->%s', expr, shrt_name )
                end

            elseif 'class' == parent_kind then
                expr = string.format( '(%s).%s', expr, shrt_name )

            elseif 'array' == parent_kind then
                -- the short name for an array element should include the brackets
                expr = string.format( '(%s)%s', expr, shrt_name )

            else
                -- no other kind should have a child
                error( 'cppobj.long_name: unsupported kind while processing long_name' )
            end
        end

        name = expr
    end

    p_long_name[ obj ] = name
    return name
end

-- reseat( obj )
--
-- Reseat an object to expansion depth zero. necessary for getting a full hierarchy of a deep object graph.
-- Note that child symbols can't be removed from a symbol group, only symbols with no parent in the group.
-- Therefore, we move the symbol to a different symbol group.
-- params:
--   obj: cppobj to reseat
-- returns:
--   new group of the object
function reseat( obj )
    local expr = p_long_name[ obj ]
    local symgrp = dbgeng.symbols.create_symbol_group()
    local index  = symgrp:add_symbol( expr, 0 )
    assert( index == 0, string.format( 'failed to reseat \'%s\'', expr ) )
    p_symgrp[ obj ] = symgrp
    return symgrp
end

-- safe_long_name( obj )
--
-- Wrap a cppobj's long name in parens, if necessary.
-- params:
--   obj: the cppobj whose name to wrap
-- returns
--   the cppobj's safe long name
function safe_long_name( obj )
    local longname = p_long_name[ obj ]
    if not longname then
        return nil
    end

    local needs_parens = true
    if longname:match( '^%b()$' ) then
        -- doesn't need parens because it's already got them
        needs_parens = false
    elseif longname:match( '^[_%a][_%w]$' ) then
        -- simple identifier, no parens
        needs_parens = false
    end

    if needs_parens then
        longname = '(' .. longname .. ')'
    end

    return longname
end

-- short_name( obj, parentobj )
--
-- Determine an object's short name.
-- params:
--   obj: cppobj to determine the short name for
--   parentobj: cppobj containing this object, if any
-- returns
--   short name as a string
function short_name( obj, parentobj )
    local name = p_short_name[ obj ]
    if name then
        return name
    end

    local group = p_symgrp[ obj ]
    local index = symidx( obj )
    local is_base = is_baseclass( group, index )

    if is_base then
        name = p_short_name[ parentobj ]
    else
        name = group:get_symbol_name( index )
    end

    p_short_name[ obj ] = name
    return name
end


return
{
    is_cppobj   = is_cppobj,
    new         = new,

    address_of  = address_of,
    bases       = bases,
    dereference = dereference,
    kind        = kind_func,
    long_name   = long_name_func,
    members     = members,
    name        = short_name_func,
    offset      = offset_func,
    size        = size_func,
    type        = type_func,
    type_id     = type_id_func,
    type_module = type_module_func,
    value       = value,
}

