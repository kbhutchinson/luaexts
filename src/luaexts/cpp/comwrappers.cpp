//
// Convenience wrappers around COM calls
//
// All functions take an optional HRESULT pointer, which, if not null, will be
// set to the last HRESULT received by the function.

#include "stdafx.h"

#include "comwrappers.h"
#include "util.h"

////////////////////////////////////////////////////////////////////////////////
// SymbolGroup methods

ULONG SymbolGroup::GetNumberSymbols( HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    ULONG count = 0;
    hr = p->GetNumberSymbols( &count );

    if ( phr ) { *phr = hr; }

    return count;
}

std::string SymbolGroup::GetSymbolName( ULONG index, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    std::string symname;

    ULONG namesize = 0;
    if ( SUCCEEDED( hr = p->GetSymbolName( index, nullptr, 0, &namesize ) ) && namesize > 0 )
    {
        util::charbuf namebuf( namesize + 1 ); // for null terminator
        if ( SUCCEEDED( hr = p->GetSymbolName( index, namebuf, namesize, nullptr ) ) )
        {
            symname = namebuf;
        }
    }

    if ( phr ) { *phr = hr; }

    return symname;
}

std::string SymbolGroup::GetSymbolTypeName( ULONG index, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    std::string typname;
    ULONG namesize = 0;
    if ( SUCCEEDED( hr = p->GetSymbolTypeName( index, nullptr, 0, &namesize ) ) )
    {
        util::charbuf namebuf( namesize + 1 ); // for null terminator
        if ( SUCCEEDED( hr = p->GetSymbolTypeName( index, namebuf, namesize, nullptr ) ) )
        {
            typname = namebuf;
        }
    }
    
    if ( 0 == index )
    {
        ULONG count = 0;
        p->GetNumberSymbols( &count );
    }

    if ( phr ) { *phr = hr; }

    return typname;
}

ULONG64 SymbolGroup::GetSymbolOffset( ULONG index, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    ULONG64 offset = 0;
    hr = p->GetSymbolOffset( index, &offset );

    if ( phr ) { *phr = hr; }

    return offset;
}

std::string SymbolGroup::GetSymbolValueText( ULONG index, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    std::string value;
    ULONG valuesize = 0;
    if ( SUCCEEDED( hr = p->GetSymbolValueText( index, nullptr, 0, &valuesize ) ) )
    {
        util::charbuf valuebuf( valuesize + 1 ); // for null terminator
        if ( SUCCEEDED( hr = p->GetSymbolValueText( index, valuebuf, valuesize, nullptr ) ) )
        {
            value = valuebuf;
        }
    }

    if ( phr ) { *phr = hr; }

    return value;
}

ULONG SymbolGroup::GetSymbolSize( ULONG index, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    ULONG size = 0;
    hr = p->GetSymbolSize( index, &size );

    if ( phr ) { *phr = hr; }

    return size;
}

DEBUG_SYMBOL_ENTRY SymbolGroup::GetSymbolEntryInformation( ULONG index, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

// unfortunately, this is too slow, so the GetNumberSymbols() calls have been moved to the identified problem
// areas. this probably means GetSymbolEntryInformation() will not always be correct since there may be other,
// unidentified areas that need a GetNumberSymbols() call.
#if 0
    // symbol group seems to have some bugs. sometimes it will get wedged in a way that will prevent
    // the GetSymbolEntryInformation() call below to work properly or at all. calling GetNumberSymbols()
    // first seems to fix it, so we'll just always do that.
    // so far the following broken scenarios are known:
    //   - calling GetSymbolTypeName() for index 0
    //   - calling GetSymbolEntryInformation() directly after AddSymbol()
    {
        ULONG count = 0;
        p->GetNumberSymbols( &count );
    }
#endif

    DEBUG_SYMBOL_ENTRY dse;
    memset( &dse, 0, sizeof( dse ) );

    hr = p->GetSymbolEntryInformation( index, &dse );

    if ( phr ) { *phr = hr; }

    return dse;
}

DEBUG_SYMBOL_PARAMETERS SymbolGroup::GetSymbolParameters( ULONG index, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    DEBUG_SYMBOL_PARAMETERS dsp;
    memset( &dsp, 0, sizeof( dsp ) );
    hr = p->GetSymbolParameters( index, 1, &dsp );

    if ( phr ) { *phr = hr; }

    return dsp;
}

std::vector< DEBUG_SYMBOL_PARAMETERS > SymbolGroup::GetSymbolParameters( ULONG index, ULONG count, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    auto numsyms = GetNumberSymbols();
    std::vector< DEBUG_SYMBOL_PARAMETERS > dsps( numsyms );
    memset( dsps.data(), 0, ( numsyms * sizeof( DEBUG_SYMBOL_PARAMETERS ) ) );
    hr = p->GetSymbolParameters( index, count, dsps.data() );

    if ( phr ) { *phr = hr; }

    return dsps;
}

void SymbolGroup::OutputSymbols( ULONG output_ctrl, ULONG flags, ULONG start, ULONG count, HRESULT * phr /* = nullptr */ ) const
{
    HRESULT hr = S_OK;

    hr = p->OutputSymbols( output_ctrl, flags, start, count );

    if ( phr ) { *phr = hr; }
}

ULONG SymbolGroup::AddSymbol( std::string name, ULONG index /* = DEBUG_ANY_ID */, HRESULT * phr /* = nullptr */ )
{
    HRESULT hr = S_OK;

    hr = p->AddSymbol( name.c_str(), &index );

    // GetSymbolEntryInformation() won't always work correctly right after a call to AddSymbol() unless
    // GetNumberSymbols() is called first, so let's do it pre-emptively
    {
        ULONG count = 0;
        p->GetNumberSymbols( &count );
    }

    if ( phr ) { *phr = hr; }

    return index;
}

void SymbolGroup::ExpandSymbol( ULONG index, BOOL expand, HRESULT * phr /* = nullptr */ )
{
    HRESULT hr = S_OK;

    hr = p->ExpandSymbol( index, expand );

    if ( phr ) { *phr = hr; }
}

////////////////////////////////////////////////////////////////////////////////
// Register related functions

namespace Registers
{
    ULONG GetNumberPseudoRegisters( HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        ULONG count = -1;
        hr = g_Ext->m_Registers2->GetNumberPseudoRegisters( &count );

        if ( phr ) { *phr = hr; }

        return count;
    }

    ULONG GetPseudoIndexByName( PCSTR name, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        ULONG idx = -1;
        hr = g_Ext->m_Registers2->GetPseudoIndexByName( name, &idx );

        if ( phr ) { *phr = hr; }

        return idx;
    }

    void GetPseudoDescription( ULONG registr, std::string * regname, ULONG64 * typeModule, ULONG * typeID, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        if ( regname )
        {
            ULONG namesize = 0;
            if ( SUCCEEDED( hr = g_Ext->m_Registers2->GetPseudoDescription( registr, nullptr, 0, &namesize, nullptr, nullptr ) ) && namesize > 0 )
            {
                util::charbuf namebuf( namesize + 1 ); // for null terminator
    
                if ( SUCCEEDED( hr = g_Ext->m_Registers2->GetPseudoDescription( registr, namebuf, namesize, nullptr, typeModule, typeID ) ) )
                {
                    *regname = namebuf;
                }
            }
        }
        else
        {
            hr = g_Ext->m_Registers2->GetPseudoDescription( registr, nullptr, 0, nullptr, typeModule, typeID );
        }

        if ( phr ) { *phr = hr; }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Symbols functions

namespace Symbols
{
    std::string GetModuleNameString( ULONG which, ULONG index, ULONG64 base, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        std::string modname;
        ULONG namesize = 0;

        if ( SUCCEEDED( hr = g_Ext->m_Symbols2->GetModuleNameString( which, index, base, nullptr, 0, &namesize ) ) && namesize > 0 )
        {
            util::charbuf namebuf( namesize + 1 ); // for null terminator

            if ( SUCCEEDED( hr = g_Ext->m_Symbols2->GetModuleNameString( which, index, base, namebuf, namesize, nullptr ) ) )
            {
                modname = namebuf;
            }
        }

        if ( phr ) { *phr = hr; }

        return modname;
    }

    std::string GetTypeName( ULONG64 module, ULONG type, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        std::string typname;
        ULONG namesize = 0;

        if ( SUCCEEDED( hr = g_Ext->m_Symbols->GetTypeName( module, type, nullptr, 0, &namesize ) ) && namesize > 0 )
        {
            util::charbuf namebuf( namesize + 1 ); // for null terminator

            if ( SUCCEEDED( hr = g_Ext->m_Symbols->GetTypeName( module, type, namebuf, namesize, nullptr ) ) )
            {
                typname = namebuf;
            }
        }

        if ( phr ) { *phr = hr; }

        return typname;
    }

    std::string GetNameByOffset( ULONG64 offset, ULONG64 * displacement, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        std::string symbolname;
        ULONG namesize = 0;

        if ( SUCCEEDED( hr = g_Ext->m_Symbols->GetNameByOffset( offset, nullptr, 0, &namesize, nullptr ) ) && namesize > 0 )
        {
            util::charbuf namebuf( namesize + 1 ); // for null terminator

            if ( SUCCEEDED( hr = g_Ext->m_Symbols->GetNameByOffset( offset, namebuf, namesize, nullptr, displacement ) ) )
            {
                symbolname = namebuf;
            }
        }

        if ( phr ) { *phr = hr; }

        return symbolname;
    }

    SymbolGroup CreateSymbolGroup( HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        SymbolGroup symgroup;
        hr = g_Ext->m_Symbols3->CreateSymbolGroup2( &( symgroup.p ) );

        if ( phr ) { *phr = hr; }

        return symgroup;
    }

    SymbolGroup GetScopeSymbolGroup( ULONG flags, SymbolGroup update, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        SymbolGroup symgroup;

        if ( !! update )
        {
            hr = g_Ext->m_Symbols3->GetScopeSymbolGroup2( flags, update.p, nullptr );
        }
        else
        {
            if ( FAILED( hr = g_Ext->m_Symbols3->GetScopeSymbolGroup2( flags, nullptr, &( symgroup.p ) ) ) )
            {
                symgroup = SymbolGroup();
            }
        }

        if ( phr ) { *phr = hr; }

        return symgroup;
    }

    std::string GetFieldName( ULONG64 module, ULONG type, ULONG field_index, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        std::string fieldname;
        ULONG namesize = 0;

        if ( SUCCEEDED( hr = g_Ext->m_Symbols2->GetFieldName( module, type, field_index, nullptr, 0, &namesize ) ) && namesize > 0 )
        {
            util::charbuf namebuf( namesize + 1 ); // for null terminator

            if ( SUCCEEDED( hr = g_Ext->m_Symbols2->GetFieldName( module, type, field_index, namebuf, namesize, nullptr ) ) )
            {
                fieldname = namebuf;
            }
        }

        if ( phr ) { *phr = hr; }

        return fieldname;
    }

    std::vector< std::string > GetFieldNames( ULONG64 module, ULONG type, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        std::string tmp;
        std::vector< std::string > fieldnames;

        for ( ULONG i = 0; i < std::numeric_limits< ULONG >::max(); ++i )
        {
            tmp = GetFieldName( module, type, i, &hr );
            if ( SUCCEEDED( hr ) )
            {
                fieldnames.push_back( tmp );
            }
            else
            {
                break;
            }
        }

        if ( phr ) { *phr = hr; }

        return fieldnames;
    }

    std::tuple< ULONG64, ULONG > GetOffsetTypeId( ULONG64 offset, HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        ULONG64 module;
        ULONG type_id;
        hr = g_Ext->m_Symbols->GetOffsetTypeId( offset, &type_id, &module );

        if ( phr ) { *phr = hr; }

        return std::make_tuple( module, type_id );
    }
}

namespace Control
{
    bool IsPointer64Bit( HRESULT * phr /* = nullptr */ )
    {
        HRESULT hr = S_OK;

        hr = g_Ext->m_Control->IsPointer64Bit();

        if ( phr ) { *phr = hr; }

        return S_OK == hr;
    }
}
