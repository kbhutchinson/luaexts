#pragma once

#include <lua.hpp>

namespace dbgeng
{
    // [-0,+3]
    // adds an HRESULT to the stack as a standard Lua error: nil, message, error code.
    // pushes:
    //   nil
    //   text version of HRESULT
    //   numeric value of HRESULT
    // returns:
    //   number of values pushed onto the stack
    int xlua_push_hresult_error( lua_State * L, HRESULT hr );

    // [-0,+1]
    // pushes:
    //   global dbgeng table
    void xlua_get_dbgeng_table( lua_State * L );

    // [-0,+1]
    // pushes:
    //   table at the named index of the global dbgeng table; creates one if not present
    void xlua_get_dbgeng_subtable( lua_State * L, char const * const subtable );

    struct enum_mapping
    {
        char const * name;
        lua_Integer value;
    };

    // [-0,+0]
    // registers enum values in the array arr into the table at the top of the stack
    void xlua_set_enums( lua_State * L, enum_mapping const * arr );

    // [-0,+0]
    // initialize values used throughout dbgeng api
    void xlua_init( lua_State * L );

    // returns a string literal, with a text version of the given HRESULT
    char const * const hr_string( HRESULT hr );

} // namespace dbgeng
