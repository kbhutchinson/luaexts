// Implementation of the main extension class and its methods

#include "stdafx.h"

#include "luaexts.h"
#include "comwrappers.h"
#include "util.h"

namespace luaexts
{
    // current version of the extension
    unsigned short const major_version = 1;
    unsigned short const minor_version = 1;
    unsigned short const patch_version = 0;

    // event names
    char const * const event_command_begin = EXTNAME ".event.command_begin";
    char const * const event_command_end   = EXTNAME ".event.command_end";
    char const * const event_registered_command_begin = EXTNAME ".event.registered_command_begin";
    char const * const event_registered_command_end   = EXTNAME ".event.registered_command_end";
    char const * const event_help_command_begin = EXTNAME ".event.help_command_begin";
    char const * const event_help_command_end   = EXTNAME ".event.help_command_end";
}

namespace
{
    // [-0,+1]
    // pushes onto the stack the function that runs event handlers
    void xlua_push_event_handler_runner( lua_State * L )
    {
        lua_getglobal( L, EXTNAME );
        if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
            g_Ext->ThrowStatus( E_FAIL, "'" EXTNAME "' table not found" );

        lua_getfield( L, -1, "event" );
        if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
            g_Ext->ThrowStatus( E_FAIL, "'" EXTNAME ".event' table not found" );

        lua_getfield( L, -1, "run_event_handlers" );
        if ( lua_isnil( L, -1 ) || ! lua_isfunction( L, -1 ) )
            g_Ext->ThrowStatus( E_FAIL, "'" EXTNAME ".event.run_event_handlers' function not found" );

        lua_remove( L, -2 );
        lua_remove( L, -2 );
    }

    // these event-firing functions are written separately so that if eventargs ever need to be added,
    // they can be done for each one individually without affecting the others

    // [-0,+0]
    // runs event handlers for command_begin
    void xlua_fire_event_command_begin( lua_State * L )
    {
        xlua_push_event_handler_runner( L );
        lua_pushstring( L, luaexts::event_command_begin );
        if ( lua_pcall( L, 1, 0, 0 ) )
            g_Ext->ThrowStatus( E_FAIL, lua_tostring( L, -1 ) );
    }

    // [-0,+0]
    // runs event handlers for command_end
    void xlua_fire_event_command_end( lua_State * L )
    {
        xlua_push_event_handler_runner( L );
        lua_pushstring( L, luaexts::event_command_end );
        if ( lua_pcall( L, 1, 0, 0 ) )
            g_Ext->ThrowStatus( E_FAIL, lua_tostring( L, -1 ) );
    }

    // [-0,+0]
    // runs event handlers for registered_command_begin
    void xlua_fire_event_registered_command_begin( lua_State * L )
    {
        xlua_push_event_handler_runner( L );
        lua_pushstring( L, luaexts::event_registered_command_begin );
        if ( lua_pcall( L, 1, 0, 0 ) )
            g_Ext->ThrowStatus( E_FAIL, lua_tostring( L, -1 ) );
    }

    // [-0,+0]
    // runs event handlers for registered_command_end
    void xlua_fire_event_registered_command_end( lua_State * L )
    {
        xlua_push_event_handler_runner( L );
        lua_pushstring( L, luaexts::event_registered_command_end );
        if ( lua_pcall( L, 1, 0, 0 ) )
            g_Ext->ThrowStatus( E_FAIL, lua_tostring( L, -1 ) );
    }

    // [-0,+0]
    // runs event handlers for help_command_begin
    void xlua_fire_event_help_command_begin( lua_State * L )
    {
        xlua_push_event_handler_runner( L );
        lua_pushstring( L, luaexts::event_help_command_begin );
        if ( lua_pcall( L, 1, 0, 0 ) )
            g_Ext->ThrowStatus( E_FAIL, lua_tostring( L, -1 ) );
    }

    // [-0,+0]
    // runs event handlers for help_command_end
    void xlua_fire_event_help_command_end( lua_State * L )
    {
        xlua_push_event_handler_runner( L );
        lua_pushstring( L, luaexts::event_help_command_end );
        if ( lua_pcall( L, 1, 0, 0 ) )
            g_Ext->ThrowStatus( E_FAIL, lua_tostring( L, -1 ) );
    }
}

EXT_DECLARE_GLOBALS();

// static
EXT_CLASS & EXT_CLASS::inst()
{
    return g_ExtInstance;
}

HRESULT EXT_CLASS::Initialize()
{
    m_ExtMajorVersion = luaexts::major_version;
    m_ExtMinorVersion = luaexts::minor_version;

    reinitialize();

    return S_OK;
}

void EXT_CLASS::Uninitialize()
{
}

EXT_COMMAND(
    reinitialize,
    "resets the internal Lua virtual machine, and reinitializes the extension configuration",
    nullptr )
{
    m_string_library.clear();
    m_command_descriptions.clear();
    m_inited = bridge::init();
}

EXT_COMMAND(
    lua,
    "runs a Lua chunk",
    "{;x;Lua chunk to run}" )
{
    if ( initialization_failure() ) return;

    xlua_fire_event_command_begin( m_inited.L );

    std::string const code( GetUnnamedArgStr( 0 ) );
    auto err = bridge::run( m_inited.L, code );
    if ( ! err.empty() )
        Err( "%s\n", err.c_str() );

    xlua_fire_event_command_end( m_inited.L );
}

EXT_COMMAND(
    luacmd,
    "translate a debugger extension command into a Lua function call",
    "{{custom}}{{s: <cmd> <args>}}{{l:cmd - name that is registered with " EXTNAME " to run as a command\nargs - rest of the command line to be parsed and sent to the command}}" )
{
    if ( initialization_failure() ) return;

    xlua_fire_event_command_begin( m_inited.L );
    xlua_fire_event_registered_command_begin( m_inited.L );

    std::string const command_line = GetRawArgStr();
    auto err = bridge::run_registered_command( m_inited.L, command_line );
    if ( ! err.empty() )
        Err( "%s\n", err.c_str() );

    xlua_fire_event_registered_command_end( m_inited.L );
    xlua_fire_event_command_end( m_inited.L );
}

EXT_COMMAND(
    luahelp,
    "show help for registered Lua commands",
    "{;s,o;command;if given, specifies the command to show help for. if not, help will be shown for all registered commands.}" )
{
    if ( initialization_failure() ) return;

    xlua_fire_event_command_begin( m_inited.L );
    xlua_fire_event_help_command_begin( m_inited.L );

    std::string const command = GetNumUnnamedArgs() == 1 ? GetUnnamedArgStr( 0 ) : "";
    auto err = bridge::help_registered_command( m_inited.L, command );
    if ( ! err.empty() )
        Err( "%s\n", err.c_str() );

    xlua_fire_event_help_command_end( m_inited.L );
    xlua_fire_event_command_end( m_inited.L );
}

// create and register a new command description object, returning a token to it
EXT_CLASS::cmd_token_t EXT_CLASS::register_command_description( std::string const & extname, std::string const & cmdname, std::string const & description, std::string const & arguments )
{
    // the ExtCommandDesc object only stores a char pointer, so we need to arrange for these strings to hang around as long as the help is available
    m_string_library.push_back( util::make_string() << "luacmd " << extname << '.' << cmdname );
    auto & display_name = m_string_library.back();

    m_string_library.push_back( description );
    auto & display_description = m_string_library.back();

    m_string_library.push_back( arguments );
    auto & display_args = m_string_library.back();

    auto ecd = std::make_shared< ExtCommandDesc >( display_name.c_str(), nullptr, display_description.c_str(), display_args.c_str() );
    if ( ! ecd )
        return nullptr;

    ecd->ExInitialize( g_Ext );
    ExtCommandDesc::s_Commands = nullptr;
    ExtCommandDesc::s_LongestCommandName = 0;

    m_command_descriptions.insert( std::make_pair( ecd.get(), ecd ) );
    return ecd.get();
}

// retrieve the command description associated with this token
// if it doesn't exist, the shared_ptr will be empty
std::shared_ptr< ExtCommandDesc > EXT_CLASS::get_command_description( cmd_token_t token )
{
    if ( 0 == m_command_descriptions.count( token ) )
        return nullptr;

    return m_command_descriptions[ token ];
}

// simply checks for initialization failure, and reports it
bool EXT_CLASS::initialization_failure()
{
    if ( m_inited.success )
        return false; // init didn't fail

    Err( EXTNAME " failed to initialize: %s\n", m_inited.errmsg.c_str() );
    return true;
}
