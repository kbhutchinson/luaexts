cppobj
======

.. module:: cppobj
    :synopsis: high-level manipulation of C++ objects

*cppobj* provides a high level interface for working with C++ objects. The goal is to allow
intuitive manipulation of those objects through familiar syntax.

For example, here's how to wrap an object named *foo* from the current scope, and print the value of
its *bar* data member:

::

  local foo = cppobj.new( 'foo' )
  print( foo.bar )

Lua's metatable feature is used to overload the indexing operation performed by ``foo.bar``, in
order to return a new cppobj for the *bar* data member, which is then stringified. Stringification
is overloaded to return a dbgeng "view" of an object, i.e., the same string that the dbgeng COM API
would return when asked to return an object as text. A native Lua value can be retrieved for
fundamental types by using :func:`cppobj.value()`.

Due to this overloading of the indexing operation, and the fact that object methods in Lua are also
implemented by overloading indexing, most of the functions that operate on cppobj objects are
instead implemented as class functions that simply take the cppobj as a paramater.

Class Functions
---------------

Class functions are accessible through the global *cppobj* table. Many of them operate on a
cppobj, and return information about the wrapped C++ object.

.. function:: new( expr ) -> cppobj

    Creates a new cppobj, representing a C++ symbol.

    :param string expr: C++ expression that evaluates to a symbol
    :returns: New cppobj representing the given symbol

.. function:: is_cppobj( value ) -> boolean

    Checks whether the given Lua value is a cppobj.

    :param value: Lua value to check
    :returns: True if *value* is a cppobj, otherwise false

.. function:: value( obj ) -> Lua value

    For fundamental types (:func:`cppobj.kind` returns "base", "enum", or "pointer"), returns an object's value as an
    appropriately typed Lua value.
    
    For array types, (:func:`cppobj.kind` returns "array), returns the memory offset of the array.

    For other types, returns ``nil``.

    :param cppobj obj: Object whose value to return

.. function:: kind( obj ) -> string

    Returns the "kind" of a C++ object. The kind will be one of the following values:

    - ``base``: Fundamental type like ``bool``, ``char``, ``int``, ``float``, etc
    - ``enum``: Enum value
    - ``pointer``
    - ``array``
    - ``class``: Class instance
    - ``function``
    - ``unknown``: Some other, unrecognized kind

    :param cppobj obj: Object whose kind to return

.. function:: type( obj ) -> string

    Returns an object's type.

    :param cppobj obj: Object whose type to return

.. function:: name( obj ) -> string

    Returns an object's name, usually the variable or field name.

    :param cppobj obj: Object whose name to return

.. function:: long_name( obj ) -> string

    Returns an object's "long" name, which is a fully qualified expression that is valid for the
    current scope.

    :param cppobj obj: Object whose long name to return

.. function:: offset( obj ) -> integer

    Returns an object's memory offset in the debugging target's memory.

    :param cppobj obj: Object whose offset to return

.. function:: size( obj ) -> integer

    Returns an object's size.

    :param cppobj obj: Object whose size to return

.. function:: type_id( obj ) -> integer

    Returns an object's type id. This id is used in various dbgeng COM API functions.

    :param cppobj obj: Object whose type id to return

.. function:: type_module( obj ) -> integer

    Returns the base memory offset of the module that the object's type is from. This is effectively
    an id for the module, and is used in various dbgeng COM API functions.

    :param cppobj obj: Object whose module to return

.. function:: address_of( obj ) -> cppobj

    Returns a new cppobj representing a pointer to *obj*.

    :param cppobj obj: Object to create a pointer to

.. function:: dereference( obj ) -> cppobj

    Given a cppobj representing a pointer, returns a new cppobj representing the pointee.

    :param cppobj obj: Object to dereference

.. function:: bases( obj ) -> array[ cppobj ]

    Returns an array of cppobj objects that are the base class representations of *obj*.

    :param cppobj obj: Object whose base class representations to return

.. function:: members( obj ) -> array[ cppobj ]

    Returns an array of cppobj objects that are the data members of *obj*.

    :param cppobj obj: Object whose data members to return


Overloaded Operators
--------------------

Overloaded operators comprise the remainder of the interactions with and between cppobj objects.


.. method:: cppobj.__index( name ) -> cppobj

    The indexing operator (``.`` or ``[]``) is overloaded to return a new cppobj representing a data
    member of the cppobj being indexed.

    Assuming *foo* is a formerly created cppobj, the following retrieves its *bar* data member as a
    cppobj:

    ::

      local bar = foo.bar
      -- equivalent:
      local bar = foo[ 'bar' ]

.. method:: cppobj.__eq( cppobj ) -> boolean

    The equality operator (``==``) is overloaded to test C++ objects for equality, according to the
    following rules:

    * arrays are treated as pointers
    * pointers, base, and enum types are compared as numbers
    * class types are considered equal if two objects represent the same type at the same memory
      offset

    If the first operand is a cppobj, any other equality result will be false.

.. method:: cppobj.__sub( cppobj ) -> number
.. method:: cppobj.__sub( number ) -> cppobj or number

    The subtraction operator (``-``) is overloaded to perform subtraction with C++ objects, according
    to the following rules:

    * if the 1st operand is a cppobj pointer or array, and the 2nd operand is:

      * cppobj pointer or array of the same type

        * result is the scaled distance between the two memory locations, scaled by the object size

      * cppobj base integral type

        * result is a new pointer, offset by the given scaled distance

      * Lua integer

        * result is a new pointer, offset by the given scaled distance

    * if the 1st operand is a cppobj base type or enum, and the 2nd operand is:

      * cppobj base type or enum or Lua number; result is the difference of the two operands

    * if the 1st operand is a Lua value, and the 2nd operand is:

      * cppobj base type or enum or Lua number; result is the difference of the two operands

    * otherwise, the result is ``nil``
