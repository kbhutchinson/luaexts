dbgeng.symbols
==============

.. module:: dbgeng.symbols
    :synopsis: functions dealing with symbols

The symbols functions are accessible from the global *dbgeng.symbols* table, and represent the
functions from the IDebugSymbols_ interfaces of the `dbgeng COM API`_.

.. _IDebugSymbols: https://msdn.microsoft.com/en-us/library/ff550856

Functions
---------

.. function:: create_symbol_group() -> dbgeng.symbolgroup

    Creates a new symbol group.
    See `CreateSymbolGroup2 <https://msdn.microsoft.com/en-us/library/ff540096>`_.

    :rtype: dbgeng.symbolgroup

.. function:: get_module_name_string( which, index, base ) -> string

    Returns the name of the specified module.
    See `GetModuleNameString <https://msdn.microsoft.com/en-us/library/ff547149>`_.

    :param integer which: which of the module's names to return; should be a value from the
        :data:`.modname` table
    :param integer index: index of the module; if set to :data:`dbgeng.ANY_ID`, the *base*
        parameter is used instead
    :param integer base: if the *index* paramate is :data:`dbgeng.ANY_ID`, specifies the module
        base address; otherwise, it's ignored
    :returns: name of the specified module

.. function:: get_symbol_type_id( symbol ) -> integer, integer

    Returns the module and type id for a symbol.
    See `GetSymbolTypeId <https://msdn.microsoft.com/en-us/library/ff549173>`_.

    :param string symbol: expression of the symbol whose type id is requested
    :returns: two values, the first being the module base address containing the symbol; the second
        being the type id for the given expression

.. function:: get_type_id( module, name ) -> integer

    Returns the type id for a type given its module and name.
    See `GetTypeId <https://msdn.microsoft.com/en-us/library/ff549376>`_.

    :param integer module: module base address of the module to which the type belongs
    :param string name: type name or symbol expression whose type id is requested
    :returns: type id of the symbol

.. function:: get_type_name( module, typeid ) -> string

    Returns the name of a type specified by its module and type id.
    See `GetTypeName <https://msdn.microsoft.com/en-us/library/ff549408>`_.

    :param integer module: module base address of the module to which the type belongs
    :param integer typeid: specifies the type id of the type
    :returns: name of the type

.. function:: get_type_size( module, typeid ) -> integer

    Returns the size of a type specified by its module and type id.
    See `GetTypeSize <https://msdn.microsoft.com/en-us/library/ff549457>`_.

    :param integer module: module base address of the module to which the type belongs
    :param integer typeid: specifies the type id of the type
    :returns: number of bytes of memory an instance of the specified type requires

Other
-----

.. data:: modname

    Table containing values that control the operation of :func:`.get_module_name_string`. Contains
    the following values:

    * ``IMAGE``        : image name; this is the name of the executable file, including the extension
    * ``MODULE``       : module name; this is usually just the file name without the extension
    * ``LOADED_IMAGE`` : loaded image name; unless Microsoft CodeView symbols are present, this is the same as the image name
    * ``SYMBOL_FILE``  : symbol file name; the path and name of the symbol file; if no symbols have been loaded, this is the name of the executable file instead
    * ``MAPPED_IMAGE`` : mapped image name; in most cases, this is NULL; if the debugger is mapping an image file (for example, during minidump debugging), this is the name of the mapped image

    See table in the documentation for `GetModuleNameString <https://msdn.microsoft.com/en-us/library/ff547149>`_.
