#include "stdafx.h"

#include "dbgeng-control.h"
#include "dbgeng-common.h"
#include "dbgeng-breakpoint.h"
#include "util.h"
#include <lua.hpp>

// Lua projections for the IDebugControl interface
namespace dbgeng { namespace control
{
    // add_assembly_options( options )
    // turns on some of the assembly and disassembly options. the given options will be added to the
    // existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.asmopt' table
    // returns:
    //   true if successful
    int l_add_assembly_options( lua_State * L )
    {
        auto options = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control3->AddAssemblyOptions( options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // add_breakpoint( type, id )
    // adds a breakpoint. type can be one of:
    //   - dbgeng.breakpoint.type.CODE: software breakpoint
    //   - dbgeng.breakpoint.type.DATA: processor breakpoint
    // params:
    //   type: breakpoint type
    //   id  : breakpoint id to use. use 'dbgeng.ANY_ID' to have the engine assign the id.
    // returns:
    //   new userdata representing a breakpoint
    int l_add_breakpoint( lua_State * L )
    {
        auto type = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );
        auto id   = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        IDebugBreakpoint * bp = nullptr;

        HRESULT hr = g_Ext->m_Control->AddBreakpoint( type, id, &bp );
        if ( SUCCEEDED( hr ) )
        {
            breakpoint::xlua_push_breakpoint( L, bp );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // add_engine_options( options )
    // turns on some of the debugger engine's options. the given options will be added to the existing
    // options.
    // params:
    //   options: bitfield that contains a combination of values from the 'dbgeng.control.engopt' table.
    // returns:
    //   true if successful
    int l_add_engine_options( lua_State * L )
    {
        auto options = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control->AddEngineOptions( options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // add_extension( path )
    // loads an extension library into the debugger engine. if the extension library is already
    // loaded, simply returns the handle. the library will not be loaded again.
    // params:
    //   path: fully qualified path to the extension library to load
    // returns:
    //   handle to the loaded extension library
    int l_add_extension( lua_State * L )
    {
        auto path = luaL_checkstring( L, 1 );
        ULONG64 handle = 0;

        HRESULT hr = g_Ext->m_Control->AddExtension( path, 0, &handle );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, handle );
            return 1;
        }
        
        return xlua_push_hresult_error( L, hr );
    }

    // call_extension( handle, function, arguments )
    // calls a debugger extension
    // params:
    //   handle: handle to the extension library containing the extension to call,
    //     returned by add_extension. if set to 0, the engine will walk the extension
    //     library chain searching for the extension.
    //   function: string name of the extension to call
    //   arguments: string that will be parsed into arguments by the extension as if
    //     from the command prompt
    // returns:
    //   true if successful
    int l_call_extension( lua_State * L )
    {
        auto handle = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );
        auto func   = luaL_checkstring( L, 2 );
        auto args   = luaL_checkstring( L, 3 );

        HRESULT hr = g_Ext->m_Control->CallExtension( handle, func, args );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // disassemble( offset, flags )
    // disassembles a processor instruction in the target's memory
    // params:
    //   offset: location in the target's memory of the instruction to disassemble
    //   flags: bitfield of flags that affect the behavior of this function. currently, the
    //     only flag that can be set is dbgeng.control.disasm_flag.EFFECTIVE_ADDRESS. when set,
    //     the engine will compute and display the effective address from the current registers
    //     information.
    // returns:
    //   disassembled instruction as a string
    //   location in the target's memory of the instruction following the disassembled instruction
    int l_disassemble( lua_State * L )
    {
        auto offset = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );
        auto flags  = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        ULONG bufsize = 0;
        ULONG64 endoffset = 0;

        HRESULT hr = g_Ext->m_Control->Disassemble( offset, flags, nullptr, 0, &bufsize, &endoffset );
        if ( SUCCEEDED( hr ) )
        {
            util::charbuf buffer( bufsize + 1 ); // for null-terminator
            hr = g_Ext->m_Control->Disassemble( offset, flags, buffer, bufsize, nullptr, &endoffset );
            if ( SUCCEEDED( hr ) )
            {
                lua_pushstring( L, buffer );
                lua_pushinteger( L, endoffset );
                return 2;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // execute( outctl, command, flags )
    // executes the specified debugger commands, as if typed at the command prompt
    // params:
    //   outctl: a combination of values from the 'dbgeng.outctl' table. for more
    //     information, see: https://msdn.microsoft.com/en-us/library/ff541517.aspx
    //   command: command string to execute. may contain multiple commands.
    //   flags: bitfield combination of values from the 'dbgeng.control.execute_flag' table:
    //     ECHO: command string is send to output
    //     NOT_LOGGED: command string is not logged; this is overridden by ECHO
    //     NO_REPEAT: if command string is empty, do not repeat the last command, and do not
    //       save the current command string for repeat execution later
    // returns:
    //   true if successful
    int l_execute( lua_State * L )
    {
        auto outctl  = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );
        auto command = luaL_checkstring( L, 2 );
        auto flags   = static_cast< ULONG >( luaL_checkinteger( L, 3 ) );

        HRESULT hr = g_Ext->m_Control->Execute( outctl, command, flags );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_assembly_options()
    // returns the assembly and disassembly options that affect how the debugger engine assembles
    // and disassembles processor instructions for the target
    // returns:
    //   bitfield containing the assembly options, which is a combination of the values in the
    //   'dbgeng.control.asmopt' table. for more information on their meanings, see:
    //   https://msdn.microsoft.com/en-us/library/ff541443.aspx
    int l_get_assembly_options( lua_State * L )
    {
        ULONG options = 0;

        HRESULT hr = g_Ext->m_Control3->GetAssemblyOptions( &options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, options );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_breakpoint_by_id( id )
    // returns the breakpoint with the specified breakpoint id
    // params:
    //   id: breakpoint id of the breakpoint to return
    // returns
    //   breakpoint with the given id
    int l_get_breakpoint_by_id( lua_State * L )
    {
        auto id = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );
        IDebugBreakpoint * bp = nullptr;

        HRESULT hr = g_Ext->m_Control->GetBreakpointById( id, &bp );
        if ( SUCCEEDED( hr ) )
        {
            breakpoint::xlua_push_breakpoint( L, bp );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_breakpoint_by_index( index )
    // returns the breakpoint located at the specified index
    // params:
    //   index: zero-based index of the breakpoint to return. this is specific to the current
    //     process. the value should be between zero and the total number of breakpoints minus
    //     one. the total number of breakpoints can be retrieved by calling get_number_breakpoints().
    // returns:
    //   breakpoint with the given index
    int l_get_breakpoint_by_index( lua_State * L )
    {
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );
        IDebugBreakpoint * bp = nullptr;

        HRESULT hr = g_Ext->m_Control->GetBreakpointByIndex( index, &bp );
        if ( SUCCEEDED( hr ) )
        {
            breakpoint::xlua_push_breakpoint( L, bp );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_code_level()
    // returns the current code level and is mainly used when stepping through code
    // returns:
    //   current code level, which can be one of the values from the 'dbgeng.control.level' table:
    //     SOURCE: when single stepping, the size of a step will be one source line
    //     ASSEMBLY: when single stepping, the size of a step will be a single processor instruction
    int l_get_code_level( lua_State * L )
    {
        ULONG level = 0;

        HRESULT hr = g_Ext->m_Control->GetCodeLevel( &level );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, level );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_engine_options()
    // returns the engine's options
    // returns:
    //   bitfield containing the engine options, which is a combination of the values in the
    //   'dbgeng.control.engopt' table. for more information on their meanings, see:
    //   https://msdn.microsoft.com/en-us/library/ff541475.aspx
    int l_get_engine_options( lua_State * L )
    {
        ULONG options = 0;

        HRESULT hr = g_Ext->m_Control->GetEngineOptions( &options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, options );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_expression_syntax()
    // returns the current syntax that the engine is using for evaluating expressions
    // returns:
    //   one of the following values from the 'dbgeng.control.expr' table:
    //     MASM: expressions will be evaluated according to MASM syntax
    //     CPLUSPLUS: expressions will be evaluated according to C++ syntax
    int l_get_expression_syntax( lua_State * L )
    {
        ULONG expr = 0;

        HRESULT hr = g_Ext->m_Control3->GetExpressionSyntax( &expr );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, expr );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_expression_syntax_names( index )
    // returns the full and abbreviated names of an expression syntax
    // params:
    //   index: index of the expression syntax. this should be between zero and the number returned by
    //     get_number_expression_syntaxes() minus one.
    // returns:
    //   abbreviated name of the expression syntax
    //   full name of the expression syntax
    int l_get_expression_syntax_names( lua_State * L )
    {
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );
        ULONG fullnamesize = 0;
        ULONG abbrevnamesize = 0;

        HRESULT hr = g_Ext->m_Control3->GetExpressionSyntaxNames( index, nullptr, 0, &fullnamesize, nullptr, 0, &abbrevnamesize );
        if ( SUCCEEDED( hr ) )
        {
            util::charbuf fullnamebuf  { fullnamesize   + 1 }; // for null-terminator
            util::charbuf abbrevnamebuf{ abbrevnamesize + 1 }; // for null-terminator

            hr = g_Ext->m_Control3->GetExpressionSyntaxNames( index, fullnamebuf, fullnamesize, nullptr, abbrevnamebuf, abbrevnamesize, nullptr );
            if ( SUCCEEDED( hr ) )
            {
                lua_pushstring( L, abbrevnamebuf );
                lua_pushstring( L, fullnamebuf   );
                return 2;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_interrupt()
    // checks whether a user interrupt was issued. if a user interrupt was issued, it's cleared
    // when this function is called. examples of user interrupts include pressing Ctrl+C or pressing
    // the Stop button in a debugger. calling set_interrupt() also causes a user interrupt.
    // returns:
    //   true if an interrupt has been requested.
    //   false if an interrupt has not been requested.
    int l_get_interrupt( lua_State * L )
    {
        HRESULT hr = g_Ext->m_Control->GetInterrupt();
        if ( S_OK == hr )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }
        if ( S_FALSE == hr )
        {
            lua_pushboolean( L, 0 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_number_expression_syntaxes()
    // returns the number of expression syntaxes that are supported by the engine
    // returns:
    //   number of supported syntaxes
    int l_get_number_expression_syntaxes( lua_State * L )
    {
        ULONG count = 0;

        HRESULT hr = g_Ext->m_Control3->GetNumberExpressionSyntaxes( &count );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, count );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_stack_trace( frmoff, stkoff, instoff, numframes )
    // returns the frames at the top of the specified call stack
    // params:
    //   frmoff: memory offset of the stack frame at the top of the stack. if 0, the current frame
    //     pointer is used.
    //   stkoff: memory offset of the current stack. if 0, current stack pointer is used.
    //   instoff: memory offset of instruction of interest for the function at the top of the stack.
    //     if 0, current instruction pointer is used.
    //   numframes: number of frames to retrieve
    // returns:
    //   array containing the requested stack frames, as tables with named fields:
    //     instruction_offset: memory offset of the related instruction for the stack frame.
    //       typically, the return address for the next stack frame, or the current instruction 
    //       pointer if the frame is at the top of the stack.
    //     return_offset: memory offset of the return address for the stack frame. typically, the
    //       related instruction for the previous stack frame.
    //     frame_offset: memory offset of the stack frame, if known
    //     stack_offset: memory offset of the processor stack
    //     func_table_entry: memory offset of the function entry point for this frame, if available.
    //     params: array containing the first four stack slots passed to the function, if available.
    //     virtual: boolean, set to true if this stack frame was generated by the debugger during
    //       unwinding. set to false if it was formed from a thread's current context.
    //     frame_number: index of the frame from the top of the stack, which has index 0.
    int l_get_stack_trace( lua_State * L )
    {
        auto frmoff    = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );
        auto stkoff    = static_cast< ULONG64 >( luaL_checkinteger( L, 2 ) );
        auto instoff   = static_cast< ULONG64 >( luaL_checkinteger( L, 3 ) );
        auto numframes = static_cast< ULONG   >( luaL_checkinteger( L, 4 ) );
        ULONG framesfilled = 0;
        util::buffer< DEBUG_STACK_FRAME > buffer{ numframes };

        HRESULT hr = g_Ext->m_Control->GetStackTrace( frmoff, stkoff, instoff, buffer, numframes, &framesfilled );
        if ( SUCCEEDED( hr ) )
        {
            // return array
            lua_createtable( L, framesfilled, 0 );

            for ( ULONG i = 0; i < framesfilled; ++i )
            {
                auto & f = buffer[ i ];

                lua_pushinteger( L, i + 1 );
                lua_createtable( L, 0, 8 );

                lua_pushinteger( L, f.InstructionOffset );
                lua_setfield( L, -2, "instruction_offset" );
                lua_pushinteger( L, f.ReturnOffset );
                lua_setfield( L, -2, "return_offset" );
                lua_pushinteger( L, f.FrameOffset );
                lua_setfield( L, -2, "frame_offset" );
                lua_pushinteger( L, f.StackOffset );
                lua_setfield( L, -2, "stack_offset" );
                lua_pushinteger( L, f.FuncTableEntry );
                lua_setfield( L, -2, "func_table_entry" );
                lua_createtable( L, 4, 0 );
                for ( int j = 0; j < 4; ++j )
                {
                    lua_pushinteger( L, j + 1 );
                    lua_pushinteger( L, f.Params[ j ] );
                    lua_settable( L, -3 );
                }
                lua_setfield( L, -2, "params" );
                lua_pushboolean( L, TRUE == f.Virtual );
                lua_setfield( L, -2, "virtual" );
                lua_pushinteger( L, f.FrameNumber );
                lua_setfield( L, -2, "frame_number" );

                lua_settable( L, -3 );
            }

            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // is_pointer_64bit()
    // returns:
    //   bool indicating whether the effective processor uses 64-bit pointers
    int l_is_pointer_64bit( lua_State * L )
    {
        HRESULT hr = g_Ext->m_Control->IsPointer64Bit();

        if ( S_OK == hr )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        if ( S_FALSE == hr )
        {
            lua_pushboolean( L, 0 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // output_disassembly_lines( outctl, previous_lines, total_lines, offset, flags )
    // disassembles several processor instructions and sends the resulting assembly instructions to
    // the output callbacks
    // params:
    //   outctl: combination of flags from DEBUG_OUTCTL_XXX that control output
    //   previous_lines: number of lines of instructions before *offset* to include in the output
    //   total_lines: number of lines of instructions to include in the output
    //   offset: memory location to disassemble, along with surrounding instructions depending on
    //      other arguments
    //   flags: bitfield that determines format of the output
    // returns:
    //   table containing the following fields:
    //     offset_line: line number in the output that contains the instruction at *offset*
    //     start_offset: memory location of the first instruction included in the output
    //     end_offset: memory location of the instruction after the last disassembled instruction
    //     line_offsets: array that contains the memory location of each instruction included in the
    //       output; if the output for an instruction spans multiple lines, the array element
    //       corresponding to the first line of output will contain the address of the instruction
    int l_output_disassembly_lines( lua_State * L )
    {
        auto outctl         = static_cast< ULONG   >( luaL_checkinteger( L, 1 ) );
        auto previous_lines = static_cast< ULONG   >( luaL_checkinteger( L, 2 ) );
        auto total_lines    = static_cast< ULONG   >( luaL_checkinteger( L, 3 ) );
        auto offset         = static_cast< ULONG64 >( luaL_checkinteger( L, 4 ) );
        auto flags          = static_cast< ULONG   >( luaL_checkinteger( L, 5 ) );

        ULONG   offset_line  = 0;
        ULONG64 start_offset = 0;
        ULONG64 end_offset   = 0;
        util::buffer< ULONG64 > line_offsets{ total_lines };

        HRESULT hr =
            g_Ext->m_Control->OutputDisassemblyLines(
                outctl, previous_lines, total_lines, offset, flags, &offset_line, &start_offset, &end_offset, line_offsets );

        if ( SUCCEEDED( hr ) )
        {
            lua_createtable( L, 0, 4 );

            lua_pushinteger( L, offset_line );
            lua_setfield( L, -2, "offset_line" );
            lua_pushinteger( L, start_offset );
            lua_setfield( L, -2, "start_offset" );
            lua_pushinteger( L, end_offset );
            lua_setfield( L, -2, "end_offset" );

            lua_createtable( L, total_lines, 0 );
            for ( ULONG i = 0; i < total_lines; ++i )
            {
                lua_pushinteger( L, i + 1 );
                lua_pushinteger( L, line_offsets[ i ] );
                lua_settable( L, -3 );
            }
            lua_setfield( L, -2, "line_offsets" );

            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // remove_assembly_options( options )
    // turns off some of the assembly and disassembly options. the given options will be removed from
    // the existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.asmopt' table.
    // returns:
    //   true if successful
    int l_remove_assembly_options( lua_State * L )
    {
        auto options = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control3->RemoveAssemblyOptions( options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // remove_breakpoint( breakpoint )
    // removes a breakpoint
    // params:
    //   breakpoint: a breakpoint userdata
    // returns:
    //   true if successful
    int l_remove_breakpoint( lua_State * L )
    {
        IDebugBreakpoint * bp = breakpoint::xlua_check_breakpoint( L, 1 );

        HRESULT hr = g_Ext->m_Control->RemoveBreakpoint( bp );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // remove_engine_options( options )
    // turns off some of the engine's options. the given options will be removed from the existing
    // options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.engopt' table.
    // returns:
    //   true if successful
    int l_remove_engine_options( lua_State * L )
    {
        auto options = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control->RemoveEngineOptions( options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // remove_extension( handle )
    // unloads an extension library
    // params:
    //   handle: handle of the extension library to unload, returned from add_extension
    // returns
    //   true if successful
    int l_remove_extension( lua_State * L )
    {
        auto handle = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control->RemoveExtension( handle );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // set_assembly_options( options )
    // changes the assembly and disassembly options. the given options will completely replace the
    // existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.asmopt' table.
    // returns:
    //   true if successful
    int l_set_assembly_options( lua_State * L )
    {
        auto options = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control3->SetAssemblyOptions( options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // set_code_level( level )
    // sets the current code level and is mainly used when stepping through code
    // params:
    //   level: one of the values from the 'dbgeng.control.level' table:
    //     SOURCE: when single stepping, the size of a step will be one source line
    //     ASSEMBLY: when single stepping, the size of a step will be a single processor instruction
    // returns:
    //   true if successful
    int l_set_code_level( lua_State * L )
    {
        auto level = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control->SetCodeLevel( level );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // set_engine_options( options )
    // changes the engine's options. the given options will completely replace the existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.engopt' table.
    // returns:
    //   true if successful
    int l_set_engine_options( lua_State * L )
    {
        auto options = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control->SetEngineOptions( options );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // set_expression_syntax( syntax )
    // sets the syntax that the engine will use to evaluate expressions
    // params:
    //   syntax: one of the values from the 'dbgeng.control.expr' table:
    //     MASM: expressions will be evaluated according to MASM syntax
    //     CPLUSPLUS: expressions will be evaluated according to C++ syntax
    // returns:
    //   true if successful
    int l_set_expression_syntax( lua_State * L )
    {
        auto expr = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control3->SetExpressionSyntax( expr );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // set_expression_syntax_by_name( syntax )
    // sets the syntax that the engine will use to evaluate expressions, by syntax name
    // params:
    //   syntax: one of the following string values:
    //     "MASM": expressions will be evaluated according to MASM syntax
    //     "CPLUSPLUS": expressions will be evaluated according to C++ syntax
    // returns:
    //   true if successful
    int l_set_expression_syntax_by_name( lua_State * L )
    {
        auto expr = luaL_checkstring( L, 1 );

        HRESULT hr = g_Ext->m_Control3->SetExpressionSyntaxByName( expr );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // set_interrupt( flags )
    // registers a user interrupt or breaks into the debugger
    // params:
    //   flags: one of the values from the 'dbgeng.control.interrupt' table:
    //     ACTIVE: if the target is running, the engine will request a break into the debugger. when
    //       the target is suspended, the engine will register a user interrupt.
    //     PASSIVE: the engine will register a user interrupt.
    //     EXIT: see https://msdn.microsoft.com/en-us/library/ff556722.aspx
    // returns:
    //   true if successful
    int l_set_interrupt( lua_State * L )
    {
        auto flags = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );

        HRESULT hr = g_Ext->m_Control->SetInterrupt( flags );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    struct enum_mapping const assembly_options[] =
    {
        { "VERBOSE"             , DEBUG_ASMOPT_VERBOSE             },
        { "NO_CODE_BYTES"       , DEBUG_ASMOPT_NO_CODE_BYTES       },
        { "IGNORE_OUTPUT_WIDTH" , DEBUG_ASMOPT_IGNORE_OUTPUT_WIDTH },
        { "SOURCE_LINE_NUMBER"  , DEBUG_ASMOPT_SOURCE_LINE_NUMBER  },
        { nullptr, 0 }
    };

    struct enum_mapping const code_levels[] =
    {
        { "SOURCE"   , DEBUG_LEVEL_SOURCE   },
        { "ASSEMBLY" , DEBUG_LEVEL_ASSEMBLY },
        { nullptr, 0 }
    };

    struct enum_mapping const disasm_flags[] =
    {
        { "EFFECTIVE_ADDRESS"  , DEBUG_DISASM_EFFECTIVE_ADDRESS  },
        { "MATCHING_SYMBOLS"   , DEBUG_DISASM_MATCHING_SYMBOLS   },
        { "SOURCE_LINE_NUMBER" , DEBUG_DISASM_SOURCE_LINE_NUMBER },
        { "SOURCE_FILE_NAME"   , DEBUG_DISASM_SOURCE_FILE_NAME   },
        { nullptr, 0 }
    };

    struct enum_mapping const engine_options[] =
    {
        { "IGNORE_DBGHELP_VERSION"      , DEBUG_ENGOPT_IGNORE_DBGHELP_VERSION      },
        { "IGNORE_EXTENSION_VERSIONS"   , DEBUG_ENGOPT_IGNORE_EXTENSION_VERSIONS   },
        { "ALLOW_NETWORK_PATHS"         , DEBUG_ENGOPT_ALLOW_NETWORK_PATHS         },
        { "DISALLOW_NETWORK_PATHS"      , DEBUG_ENGOPT_DISALLOW_NETWORK_PATHS      },
        { "NETWORK_PATHS"               , DEBUG_ENGOPT_NETWORK_PATHS               },
        { "IGNORE_LOADER_EXCEPTIONS"    , DEBUG_ENGOPT_IGNORE_LOADER_EXCEPTIONS    },
        { "INITIAL_BREAK"               , DEBUG_ENGOPT_INITIAL_BREAK               },
        { "INITIAL_MODULE_BREAK"        , DEBUG_ENGOPT_INITIAL_MODULE_BREAK        },
        { "FINAL_BREAK"                 , DEBUG_ENGOPT_FINAL_BREAK                 },
        { "NO_EXECUTE_REPEAT"           , DEBUG_ENGOPT_NO_EXECUTE_REPEAT           },
        { "FAIL_INCOMPLETE_INFORMATION" , DEBUG_ENGOPT_FAIL_INCOMPLETE_INFORMATION },
        { "ALLOW_READ_ONLY_BREAKPOINTS" , DEBUG_ENGOPT_ALLOW_READ_ONLY_BREAKPOINTS },
        { "SYNCHRONIZE_BREAKPOINTS"     , DEBUG_ENGOPT_SYNCHRONIZE_BREAKPOINTS     },
        { "DISALLOW_SHELL_COMMANDS"     , DEBUG_ENGOPT_DISALLOW_SHELL_COMMANDS     },
        { "KD_QUIET_MODE"               , DEBUG_ENGOPT_KD_QUIET_MODE               },
        { "DISABLE_MANAGED_SUPPORT"     , DEBUG_ENGOPT_DISABLE_MANAGED_SUPPORT     },
        { "DISABLE_MODULE_SYMBOL_LOAD"  , DEBUG_ENGOPT_DISABLE_MODULE_SYMBOL_LOAD  },
        { "DISABLE_EXECUTION_COMMANDS"  , DEBUG_ENGOPT_DISABLE_EXECUTION_COMMANDS  },
        { "DISALLOW_IMAGE_FILE_MAPPING" , DEBUG_ENGOPT_DISALLOW_IMAGE_FILE_MAPPING },
        { "PREFER_DML"                  , DEBUG_ENGOPT_PREFER_DML                  },
        { "DISABLESQM"                  , DEBUG_ENGOPT_DISABLESQM                  },
        { "DISABLE_STEPLINES_OPTIONS"   , DEBUG_ENGOPT_DISABLE_STEPLINES_OPTIONS   },
        { nullptr, 0 }
    };

    struct enum_mapping const expr_syntaxes[] =
    {
        { "MASM"      , DEBUG_EXPR_MASM      },
        { "CPLUSPLUS" , DEBUG_EXPR_CPLUSPLUS },
        { nullptr, 0 }
    };

    struct enum_mapping const execute_flags[] =
    {
        { "ECHO"       , DEBUG_EXECUTE_ECHO       },
        { "NOT_LOGGED" , DEBUG_EXECUTE_NOT_LOGGED },
        { "NO_REPEAT"  , DEBUG_EXECUTE_NO_REPEAT  },
        { nullptr, 0 }
    };

    struct enum_mapping const interrupt_flags[] =
    {
        { "ACTIVE"  , DEBUG_INTERRUPT_ACTIVE  },
        { "PASSIVE" , DEBUG_INTERRUPT_PASSIVE },
        { "EXIT"    , DEBUG_INTERRUPT_EXIT    },
        { nullptr, 0 }
    };

    struct luaL_Reg const library_funcs[] =
    {
        { "add_assembly_options", l_add_assembly_options },
        { "add_breakpoint", l_add_breakpoint },
        { "add_engine_options", l_add_engine_options },
        { "add_extension", l_add_extension },
        { "call_extension", l_call_extension },
        { "disassemble", l_disassemble },
        { "execute", l_execute },
        { "get_assembly_options", l_get_assembly_options },
        { "get_breakpoint_by_id", l_get_breakpoint_by_id },
        { "get_breakpoint_by_index", l_get_breakpoint_by_index },
        { "get_code_level", l_get_code_level },
        { "get_engine_options", l_get_engine_options },
        { "get_expression_syntax", l_get_expression_syntax },
        { "get_expression_syntax_names", l_get_expression_syntax_names },
        { "get_interrupt", l_get_interrupt },
        { "get_number_expression_syntaxes", l_get_number_expression_syntaxes },
        { "get_stack_trace", l_get_stack_trace },
        { "is_pointer_64bit", l_is_pointer_64bit },
        { "output_disassembly_lines", l_output_disassembly_lines },
        { "remove_assembly_options", l_remove_assembly_options },
        { "remove_breakpoint", l_remove_breakpoint },
        { "remove_engine_options", l_remove_engine_options },
        { "remove_extension", l_remove_extension },
        { "set_assembly_options", l_set_assembly_options },
        { "set_code_level", l_set_code_level },
        { "set_engine_options", l_set_engine_options },
        { "set_expression_syntax", l_set_expression_syntax },
        { "set_expression_syntax_by_name", l_set_expression_syntax_by_name },
        { "set_interrupt", l_set_interrupt },
        { nullptr, nullptr },
    };

    // [-0,+0]
    // dbgeng::control api init
    void xlua_init( lua_State * L )
    {
        // push dbgeng.control table
        xlua_get_dbgeng_subtable( L, "control" );
        luaL_setfuncs( L, library_funcs, 0 );

        // setup the table of assembly options
        luaL_getsubtable( L, -1, "asmopt" );
        xlua_set_enums( L, assembly_options );
        lua_pop( L, 1 );

        // setup the table of code levels
        luaL_getsubtable( L, -1, "level" );
        xlua_set_enums( L, code_levels );
        lua_pop( L, 1 );

        // setup the table of disasm flags
        luaL_getsubtable( L, -1, "disasm_flag" );
        xlua_set_enums( L, disasm_flags );
        lua_pop( L, 1 );

        // setup the table of engine options
        luaL_getsubtable( L, -1, "engopt" );
        xlua_set_enums( L, engine_options );
        lua_pop( L, 1 );

        // setup the table of expression syntaxes
        luaL_getsubtable( L, -1, "expr" );
        xlua_set_enums( L, expr_syntaxes );
        lua_pop( L, 1 );

        // setup the table of execute flags
        luaL_getsubtable( L, -1, "execute_flag" );
        xlua_set_enums( L, execute_flags );
        lua_pop( L, 1 );

        // setup the table of interrupt flags
        luaL_getsubtable( L, -1, "interrupt" );
        xlua_set_enums( L, interrupt_flags );
        lua_pop( L, 1 );

        // pop dbgeng.control table
        lua_pop( L, 1 );
    }

}} // namespace dbgeng::control
