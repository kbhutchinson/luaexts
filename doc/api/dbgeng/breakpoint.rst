dbgeng.breakpoint
=================

.. module:: dbgeng.breakpoint
    :synopsis: manipulation of breakpoints

The breakpoint functions are accessible from the global *dbgeng.breakpoint* table, and represent
the functions from the IDebugBreakpoint_ interfaces of the `dbgeng COM API`_.

Breakpoints objects are generated from functions in :mod:`dbgeng.control`:
:func:`.add_breakpoint` to create a new breakpoint, and :func:`.get_breakpoint_by_id` or
:func:`.get_breakpoint_by_index` to retrieve an existing breakpoint.

Breakpoints are removed by calling :func:`dbgeng.control.remove_breakpoint` and passing a breakpoint
object. They are not removed when the Lua variable goes out of scope or is collected by garbage
collection.

All of the *breakpoint* methods are object methods, which means they can be called using Lua object
method syntax. Assuming a variable *bp* containing a breakpoint object, this call would return the
command to be executed when the breakpoint is hit:

::

    local cmd = bp:get_command()

This is really just syntatic sugar for the more verbose:

::

    local cmd = dbgeng.breakpoint.get_command( bp )

.. _IDebugBreakpoint: https://msdn.microsoft.com/en-us/library/ff549812


Object methods
--------------

.. method:: add_flags( flags ) -> nil

    Adds flags to the breakpoint by combining the given bitfield with existing flags using
    bitwise OR. Possible flag values can be found in the :data:`.flag` table.
    See `AddFlags <https://msdn.microsoft.com/en-us/library/ff537903>`_.

    :param integer flags: bitfield, combining values from :data:`.flag`

.. method:: get_command() -> string

    Returns the command string that is executed when a breakpoint is triggered.
    See `GetCommand <https://msdn.microsoft.com/en-us/library/ff545677>`_.

.. method:: get_current_pass_count() -> integer

    Returns the remaining number of times that the target must reach the breakpoint location before
    the breakpoint is triggered.
    See `GetCurrentPassCount <https://msdn.microsoft.com/en-us/library/ff545769>`_.

.. method:: get_data_parameters() -> integer, integer

    Returns two values:

    * The first is the size in bytes of the memory block whose access triggers the breakpoint.
    * The second is the access type, which will equal one of the values from the :data:`.access`
      table.

    See `GetDataParameters <https://msdn.microsoft.com/en-us/library/ff546557>`_.

.. method:: get_flags() -> integer

    Returns the flags for a breakpoint, as a bitfield, combining values from the :data:`.flag`
    table. See `GetFlags <https://msdn.microsoft.com/en-us/library/ff546791>`_.

.. method:: get_id() -> integer

    Returns the breakpoint's id. See `GetId <https://msdn.microsoft.com/en-us/library/ff546827>`_.

.. method:: get_match_thread_id() -> integer or nil

    If a thread has been set for the breakpoint, the breakpoint can be triggered only if that thread
    hits the breakpoint. If a thread has not been set, any thread can trigger the breakpoint.

    Returns the engine thread id that can trigger the breakpoint, or ``nil`` if a thread has not
    been set. See `GetMatchThreadId <https://msdn.microsoft.com/en-us/library/ff547074>`_.

.. method:: get_offset() -> integer

    Returns the location that triggers the breakpoint.
    See `GetOffset <https://msdn.microsoft.com/en-us/library/ff548008>`_.

.. method:: get_offset_expression() -> string

    Returns the expression that evaluates to the location that triggers the breakpoint.
    See `GetOffsetExpression <https://msdn.microsoft.com/en-us/library/ff548048>`_.

.. method:: get_parameters() -> table

    Returns most parameter information for a breakpoint in one call, rather than using separate
    calls for each piece. See `GetParameters <https://msdn.microsoft.com/en-us/library/ff548095>`_.

    :returns: table with the following named fields:

        * ``offset``             : location in the target's memory that will trigger the breakpoint
        * ``id``                 : breakpoint id
        * ``break_type``         : one of the values from the :data:`.type` table
        * ``proc_type``          : type of processor the breakpoint is set for
        * ``flags``              : breakpoint flags
        * ``data_size``          : for a data breakpoint, the size in bytes of the memory location that will trigger the breakpoint; otherwise, zero
        * ``data_access_type``   : for a data breakpoint, one of the values from the :data:`.access` table; otherwise, zero
        * ``pass_count``         : number of times the target will hit the breakpoint before it triggers
        * ``current_pass_count`` : remaining number of times the target will hit the breakpoint before it triggers
        * ``match_thread``       : engine thread id of the thread that can trigger the breakpoint; if any thread can trigger it, :data:`dbgeng.ANY_ID`

.. method:: get_pass_count() -> integer

    Returns the number of times that the target was originally required to reach the breakpoint
    location before the breakpoint is triggered.
    See `GetPassCount <https://msdn.microsoft.com/en-us/library/ff548104>`_.

.. method:: get_type() -> integer, integer

    Returns two values:

    * type of breakpoint, which can be one of the values in the :data:`.type` table
    * type of processor the breakpoint is set for

    See `GetType <https://msdn.microsoft.com/en-us/library/ff549370>`_.

.. method:: remove_flags( flags ) -> nil

    Removes flags from a breakpoint, by combining the complement of the given bitfield with the
    existing flags, using bitwise AND. Possible flag values can be found in the :data:`.flag` table.
    See `RemoveFlags <https://msdn.microsoft.com/en-us/library/ff554504>`_.

    :param integer flags: bitfield, combining values from :data:`.flag`

.. method:: set_command( command ) -> nil

    Sets the command string that is executed when a breakpoint is triggered.
    See `SetCommand <https://msdn.microsoft.com/en-us/library/ff556632>`_.

    :param string command: command string to execute

.. method:: set_data_parameters( size, access_type ) -> nil

    Sets parameters for a processor breakpoint.
    See `SetDataParameters <https://msdn.microsoft.com/en-us/library/ff556655>`_.

    :param integer size: size in bytes of the memory location whose access will trigger the
        breakpoint
    :param integer access_type: one of the values from the :data:`.access` table

.. method:: set_flags( flags ) -> nil

    Sets the flags for a breakpoint.
    See `SetFlags <https://msdn.microsoft.com/en-us/library/ff556703>`_.

    :param integer flags: bitfield, combining values from the :data:`.flag` table

.. method:: set_match_thread_id( id ) -> nil

    Sets the engine thread id that can trigger the breakpoint. If a thread has been set,
    the setting can be removed by passing :data:`dbgeng.ANY_ID` for the id.
    See `SetMatchThreadId <https://msdn.microsoft.com/en-us/library/ff556735>`_.

    :param integer id: id of the thread that can trigger the breakpoint

.. method:: set_offset( offset ) -> nil

    Sets the memory location in the target that triggers the breakpoint.
    See `SetOffset <https://msdn.microsoft.com/en-us/library/ff556741>`_.

    :param integer offset: location in the target's memory

.. method:: set_offset_expression( expression ) -> nil

    Sets the expression string that evaluates to the location that triggers the breakpoint.
    See `SetOffsetExpression <https://msdn.microsoft.com/en-us/library/ff556745>`_.

    :param string expression: expression that evaluates to a location in the target's memory

.. method:: set_pass_count( passes ) -> nil

    Sets the number of times that the target must reach the breakpoint location before the
    breakpoint is triggered.
    See `SetPassCount <https://msdn.microsoft.com/en-us/library/ff556759>`_.

    :param integer passes: number of passes to trigger the breakpoint


Other
-----

.. data:: access

    Table containing the access types that processor breakpoints can be defined with.

    * ``READ``    : Triggered when the CPU reads or writes memory in the breakpoint's block.
    * ``WRITE``   : Triggered when the CPU writes memory in the breakpoint's block.
    * ``EXECUTE`` : Triggered when the CPU fetches the instruction in the breakpoint's block.
    * ``IO``      : Triggered when the I/O port in the breakpoint's block is accessed.

.. data:: flag

    Table containing bit-flags that affect breakpoint operation.

    * ``ENABLED`` : When set, the breakpoint is enabled. When not set, the breakpoint is disabled.
    * ``ADDER_ONLY`` : When set, the breakpoint is a private breakpoint, only visible to the client
      that added it. Other clients will not be able to query for it, and events will not be sent to
      other clients. Callbacks related to the breakpoint will only be sent to this client.
    * ``GO_ONLY`` : When set, the breakpoint will only be triggered if the target is in unrestricted
      execution. It will not be triggered when stepping through instructions.
    * ``ONE_SHOT`` : When set, the breakpoint will automatically be removed the first time it
      triggers.
    * ``DEFERRED`` : When set, the breakpoint is deferred. The engine sets this flag when a
      breakpoint is defined using a symbolic expression, and the engine can't evaluate the
      expression. Every time a module is loaded or unloaded in the target, the engine will attempt
      to reevaluate the expression. This flag cannot be modified by any client.

.. data:: type

    Table containing types of breakpoints that can be created.

    * ``CODE``: software breakpoint
    * ``DATA``: processor breakpoint
