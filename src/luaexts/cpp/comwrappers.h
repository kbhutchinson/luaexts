// Convenience wrappers around COM calls
//
// All functions take an optional HRESULT pointer, which, if not null, will be
// set to the last HRESULT received by the function.

#pragma once

#include <string>

struct SymbolGroup
{
    typedef IDebugSymbolGroup2 SymbolGroupInterface;
    CComPtr< SymbolGroupInterface > p;

    bool operator ! ()
    {
        return ( ! p );
    }

    ULONG GetNumberSymbols( HRESULT * phr = nullptr ) const;
    std::string GetSymbolName( ULONG index, HRESULT * phr = nullptr ) const;
    std::string GetSymbolTypeName( ULONG index, HRESULT * phr = nullptr ) const;
    ULONG64 GetSymbolOffset( ULONG index, HRESULT * phr = nullptr ) const;
    std::string GetSymbolValueText( ULONG index, HRESULT * phr = nullptr ) const;
    ULONG GetSymbolSize( ULONG index, HRESULT * phr = nullptr ) const;

    DEBUG_SYMBOL_ENTRY GetSymbolEntryInformation( ULONG index, HRESULT * phr = nullptr ) const;
    DEBUG_SYMBOL_PARAMETERS GetSymbolParameters( ULONG index, HRESULT * phr = nullptr ) const;
    std::vector< DEBUG_SYMBOL_PARAMETERS > GetSymbolParameters( ULONG index, ULONG count, HRESULT * phr = nullptr ) const;
    void OutputSymbols( ULONG output_ctrl, ULONG flags, ULONG start, ULONG count, HRESULT * phr = nullptr ) const;
    ULONG AddSymbol( std::string name, ULONG index = DEBUG_ANY_ID, HRESULT * phr = nullptr );
    void ExpandSymbol( ULONG index, BOOL expand, HRESULT * phr = nullptr );
};

namespace Registers
{
    ULONG GetNumberPseudoRegisters( HRESULT * phr = nullptr );
    ULONG GetPseudoIndexByName( PCSTR name, HRESULT * phr = nullptr );
    void  GetPseudoDescription( ULONG registr, std::string * regname, ULONG64 * typeModule, ULONG * typeID, HRESULT * phr = nullptr );
}

namespace Symbols
{
    std::string GetModuleNameString( ULONG which, ULONG index, ULONG64 base, HRESULT * phr = nullptr );
    std::string GetTypeName( ULONG64 module, ULONG type, HRESULT * phr = nullptr );
    std::string GetNameByOffset( ULONG64 offset, ULONG64 * displacement, HRESULT * phr = nullptr );
    SymbolGroup CreateSymbolGroup( HRESULT * phr = nullptr );
    SymbolGroup GetScopeSymbolGroup( ULONG flags, SymbolGroup update, HRESULT * phr = nullptr );
    std::string GetFieldName( ULONG64 module, ULONG type, ULONG field_index, HRESULT * phr = nullptr );
    std::vector< std::string > GetFieldNames( ULONG64 module, ULONG type, HRESULT * phr = nullptr );
    std::tuple< ULONG64, ULONG > GetOffsetTypeId( ULONG64 offset, HRESULT * phr = nullptr );
}

namespace Control
{
    bool IsPointer64Bit( HRESULT * phr = nullptr );
}
