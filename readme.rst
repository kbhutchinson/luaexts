Luaexts
#######

Overview
========

Luaexts is an extension for Windbg_ (and the other dbgeng-based debuggers) that provides access to the `dbgeng COM API`_ from Lua_. It also implements a lightweight framework for writing extensions in Lua.

+------------------------+---------------------------------------------+
| Project source:        | https://bitbucket.org/kbhutchinson/luaexts/ |
+------------------------+---------------------------------------------+
| Project documentation: | https://luaexts.readthedocs.io/             |
+------------------------+---------------------------------------------+

.. _Windbg: https://msdn.microsoft.com/en-us/library/ff551063
.. _dbgeng COM API: https://msdn.microsoft.com/en-us/library/ff551059
.. _Lua: http://www.lua.org


Getting started
---------------

* Download `luaexts-x86.zip`_ and/or `luaexts-x64.zip`_, depending on your debugger's bitness, and unpack into your `.extpath`_.
* Within Windbg, run the following command: ``.load luaexts``
* To verify that the extension loaded correctly, run the following command::

    !lua say('success')

The growing list of supported dbgeng API functions can be accessed through the global table *dbgeng*. The table is split up into sub-tables matching the organization of the dbgeng API itself. So the functions from the `IDebugSymbols`_ interface (as well as IDebugSymbols2, etc) can be found in the *dbgeng.symbols* table.

As an example, here's how to call the `IDebugSymbols::GetSymbolTypeId`_ function, use the return values to call the `IDebugSymbols::GetTypeName`_ function, and then print the type name of a variable named *foo* that exists in the current scope::

  !lua local module, typeid = dbgeng.symbols.get_symbol_type_id( 'foo' ) say( dbgeng.symbols.get_type_name( module, typeid ) )

or without the temporary variables::

  !lua say( dbgeng.symbols.get_type_name( dbgeng.symbols.get_symbol_type_id( 'foo' ) ) )

Small side note: A global function called ``say()`` is available that takes the place of Lua's normal ``print()`` function: it prints whatever is given to it and appends a newline. The ``print()`` function has been overridden to output without a newline.

.. _luaexts-x86.zip: https://bitbucket.org/kbhutchinson/luaexts/downloads/luaexts-x86.zip
.. _luaexts-x64.zip: https://bitbucket.org/kbhutchinson/luaexts/downloads/luaexts-x64.zip
.. _.extpath: https://msdn.microsoft.com/en-us/library/ff563047
.. _IDebugSymbols: https://msdn.microsoft.com/en-us/library/ff550856
.. _`IDebugSymbols::GetSymbolTypeId`: https://msdn.microsoft.com/en-us/library/ff549173
.. _`IDebugSymbols::GetTypeName`: https://msdn.microsoft.com/en-us/library/ff549408

