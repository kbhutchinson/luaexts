// C++ <--> Lua interop for Lua userdata representing C++ objects in the debug target's memory

#include "stdafx.h"

#include "cppobj.h"

#include "bridge.h"
#include "comwrappers.h"
#include "util.h"

#include <iostream>
#include <sstream>
#include <memory>
#include <filesystem>
#include <regex>

#define CPPOBJ_EXPIRED "cppobj has expired"

namespace {
    char const * const cppobj_typename = "cppobj";

    inline std::string elem_name( LONG64 index )
    {
        return util::make_string() << '[' << index << ']';
    }

#if 0
    // a little indirection so the exact method can be changed easily later
    inline std::string gettypename( ExtRemoteTyped const & object )
    {
        // Symbols::GetTypeName() returns a more stripped down type name than ExtRemoteTyped::GetTypeName(),
        // which is seems a bit more appropriate for string matching against.
        // Some examples:
        // For a class, Symbols::GetTypeName() returns "someclass", while ExtRemoteTyped::GetTypeName() returns "class someclass".
        // For an array, Symbols::GetTypeName() returns "int[]", while ExtRemoteTyped::GetTypeName() returns "int [5]".
        // For an array of class types, Symbols::GetTypeName returns "someclass[]", while ExtRemoteTyped::GetTypeName() returns "class someclass [5]".
        return Symbols::GetTypeName( object.m_Typed.ModBase, object.m_Typed.TypeId );
    }

    inline std::string gettypename( SymbolGroup const & symgroup, ULONG index )
    {
        auto const dse = symgroup.GetSymbolEntryInformation( index );
        return Symbols::GetTypeName( dse.ModuleBase, dse.TypeId );
    }

    // a little indirection so the exact method can be changed easily later
    inline std::string getobjectname( ExtRemoteTyped const & object )
    {
        return object.m_Name;
    }
#endif

    // is the given symbol in the symbol group a baseclass?
    bool is_baseclass( SymbolGroup const & symgroup, ULONG idx )
    {
        // baseclasses show up as a symbol with the same name as its type
        auto symname = symgroup.GetSymbolName( idx );
        auto typname = symgroup.GetSymbolTypeName( idx );
        typname = std::regex_replace( std::regex_replace( typname, std::regex( "^class " ), "" ), std::regex( "^struct " ), "" );
        return symname == typname;
    }

    // debugging function to list the types of everything in the stack
    void list_stack( lua_State * L )
    {
        auto const count = lua_gettop( L );
        for ( int i = 1; i <= count; ++i )
        {
            EXT_CLASS::inst().Out( "%d: ", i );
            switch ( lua_type( L, i ) )
            {
            case LUA_TNONE          : EXT_CLASS::inst().Out( "LUA_TNONE\n" ); break;
            case LUA_TNIL           : EXT_CLASS::inst().Out( "LUA_TNIL\n" ); break;
            case LUA_TNUMBER        : EXT_CLASS::inst().Out( "LUA_TNUMBER\n" ); break;
            case LUA_TBOOLEAN       : EXT_CLASS::inst().Out( "LUA_TBOOLEAN\n" ); break;
            case LUA_TSTRING        : EXT_CLASS::inst().Out( "LUA_TSTRING\n" ); break;
            case LUA_TTABLE         : EXT_CLASS::inst().Out( "LUA_TTABLE\n" ); break;
            case LUA_TFUNCTION      : EXT_CLASS::inst().Out( "LUA_TFUNCTION\n" ); break;
            case LUA_TUSERDATA      : EXT_CLASS::inst().Out( "LUA_TUSERDATA\n" ); break;
            case LUA_TTHREAD        : EXT_CLASS::inst().Out( "LUA_TTHREAD\n" ); break;
            case LUA_TLIGHTUSERDATA : EXT_CLASS::inst().Out( "LUA_TLIGHTUSERDATA\n" ); break;
            default                 : EXT_CLASS::inst().Out( "unknown\n" );
            }
        }
    }
}

// Functions and metamethods for dealing with C++ objects
namespace cppobj
{
    std::string kind_tostring( kind_t k )
    {
        switch ( k )
        {
        case kind_t::unknown_  : return "unknown"  ; 
        case kind_t::base_     : return "base"     ; 
        case kind_t::enum_     : return "enum"     ; 
        case kind_t::class_    : return "class"    ; 
        case kind_t::function_ : return "function" ; 
        case kind_t::pointer_  : return "pointer"  ; 
        case kind_t::array_    : return "array"    ; 
        }

        return std::string();
    }

    // check that the value at the given index holds a cppobj and, if so, return it
    // never returns if the value isn't a cppobj
    cppobj_t * check_cppobj( lua_State * L, int index )
    {
        return reinterpret_cast< cppobj_t * >( luaL_checkudata( L, lua_absindex( L, index ), cppobj_typename ) );
    }
    // same as above, but return null on type fail instead of causing an error
    cppobj_t * test_cppobj( lua_State * L, int index )
    {
        return reinterpret_cast< cppobj_t * >( luaL_testudata( L, index, cppobj_typename ) );
    }

    // check that a given index holds a cppobj and, if so, return the underlying object it points to
    auto check_and_deref_cppobj( lua_State * L, int index ) -> decltype( util::declval< cppobj_t >().lock() )
    {
        auto p = check_cppobj( L, index );
        if ( p->expired() )
            luaL_error( L, CPPOBJ_EXPIRED ); // this never returns
        return p->lock();
    }
    // check that a given index holds a cppobj and, if so, return the underlying object it points to
    // if not, return an empty object
    auto test_and_deref_cppobj( lua_State * L, int index ) -> decltype( util::declval< cppobj_t >().lock() )
    {
        auto p = test_cppobj( L, index );
        if ( p )
        {
            if ( p->expired() )
                luaL_error( L, CPPOBJ_EXPIRED ); // this never returns
            else
                return p->lock();
        }
        return nullptr;
    }

    // create a new cppobj userdatum from the given ExtRemoteTyped, leaving it on top of the stack
    void push_cppobj( lua_State * L, std::string name, const EXT_CLASS::registered_object_t::element_type & object )
    {
#if 0
        auto const registered = EXT_CLASS::inst().register_cppobj( name, object );
        auto const paddr = lua_newuserdata( L, sizeof( cppobj_t ) );
        auto const pobj = new( paddr ) cppobj_t( registered );

        luaL_getmetatable( L, cppobj_typename );
        lua_setmetatable( L, -2 );

#endif
    }

    // pops the c++ expression string on the top of the stack, and
    // creates and pushes a userdatum from the c++ expression
    int new_cppobj( lua_State * L )
    {
        luaL_argcheck( L, ( LUA_TSTRING == lua_type( L, -1 ) ), 1, "'string' expected" );
        std::string const name = lua_tostring( L, -1 );
        lua_pop( L, 1 );
        cppobj_impl object( name );
        push_cppobj( L, name, object );
        return 1;
    }

    // if the top argument on the stack is a cppobj and is not expired, does nothing
    // otherwise, pops the argument and pushes nil
    int validate( lua_State * L )
    {
        auto p = reinterpret_cast< cppobj_t * >( luaL_testudata( L, -1, cppobj_typename ) );
        if ( nullptr == p || p->expired() )
        {
            lua_pop( L, 1 );
            lua_pushnil( L );
        }
        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes the object's C++ type as a string
    int type( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        std::string const typname = pobj->type();
        lua_pop( L, 1 );
        lua_pushstring( L, typname.c_str() );
        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes the object's "kind" as a string
    int kind( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );

        std::string const kindstr = kind_tostring( pobj->kind() );

        if ( kindstr.empty() )
        {
            lua_pushnil( L );
            lua_pushstring( L, ( util::make_string() << "unsupported kind for '" << pobj->long_name() << "'" ).c_str() );
            return 2;
        }

        lua_pushstring( L, kindstr.c_str() );
        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes the object's short name, usually just the member's name
    int name( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );
        lua_pushstring( L, pobj->short_name().c_str() );
        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes the object's long name, usually an expression valid in the current scope
    int long_name( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );
        lua_pushstring( L, pobj->long_name().c_str() );
        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes the object's C++ size
    int size_of( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );
        lua_pushinteger( L, static_cast< lua_Integer >( pobj->size() ) );
        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes a new cppobj representing a pointer to the object
    int address_of( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );
        try
        {
            auto ppobj = pobj->reference();
            push_cppobj( L, ppobj.long_name(), ppobj );
        }
        catch ( ExtRemoteException const & )
        {
            lua_pushnil( L );
            lua_pushliteral( L, "invalid memory reference" );
            return 2;
        }
        return 1;
    }

    // pops the cppobj on the top of the stack, which should be a pointer, and
    // pushes a new cppobj representing the object being pointed at
    int dereference( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );
        try
        {
            auto obj = pobj->dereference();
            push_cppobj( L, obj.long_name(), obj );
        }
        catch ( ExtRemoteException const & )
        {
            lua_pushnil( L );
            lua_pushliteral( L, "invalid memory dereference" );
            return 2;
        }

        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes a table with a list of the object's members
    //   if base_classes is true, only baseclass representations of this object are included
    //     otherwise only data members are included
    //   if direct_only is true, only direct members of this class are included
    //     otherwise, all inherited members are included as well
    int push_member_list( lua_State * L, bool base_classes, bool direct_only )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );

        auto const k = pobj->kind();

        if ( ! ( cppobj::kind_t::class_ == k || cppobj::kind_t::pointer_ == k || cppobj::kind_t::array_ == k ) )
        {
            lua_pushnil( L );
            return 1;
        }

        lua_newtable( L );

        auto const & members =
            base_classes ? pobj->base_classes()   :
            direct_only  ? pobj->direct_members() :
                           pobj->members();

        lua_Unsigned tableidx = 1;
        for ( const auto & member : members )
        {
            lua_pushinteger( L, static_cast< lua_Integer >( tableidx++ ) );
            push_cppobj( L, member.long_name(), member );
            lua_settable( L, -3 );
        }

        return 1;

#if 0
        // use a symbol group to do all the expansion of members first, then put them into the table
        // go through a symbol group instead of using Symbols::GetFieldNames, for a couple reasons:
        // 1. a symbol group will let us easily get baseclass information
        // 2. if desired, we can get just the members that are part of this class, rather than all inherited members
        // 3. for members that have the same name, we need more information, like the member's offset, in order to correctly create a cppobj for it
        auto symgroup = Symbols::CreateSymbolGroup();
        if ( !! symgroup )
        {
            // cast the address since the name might not be qualified enough to be resolved
            std::string const objexpr = util::make_string() << "static_cast< " << Symbols::GetModuleNameString( DEBUG_MODNAME_MODULE, DEBUG_ANY_ID, pobj->m_Typed.ModBase ) << '!' << gettypename( *pobj ) << " * >( 0x" << std::hex << pobj->m_Offset << " )";
            if ( 0 == symgroup.AddSymbol( objexpr ) ) // successfully added at index 0
            {
                symgroup.ExpandSymbol( 0, true );
                if ( ! direct_only )
                {
                    for ( ULONG i = 1; i < symgroup.GetNumberSymbols(); ++i )
                    {
                        if ( is_baseclass( symgroup, i ) )
                        {
                            symgroup.ExpandSymbol( i, true );
                        }
                    }
                }
            }

            lua_Unsigned tableidx = 1;
            for ( ULONG i = 1; i < symgroup.GetNumberSymbols(); ++i )
            {
                bool is_base = is_baseclass( symgroup, i );
                if ( ( base_classes && is_base ) || ( ! base_classes && ! is_base ) )
                {
                    lua_pushunsigned( L, tableidx++ );

                    auto const dse = symgroup.GetSymbolEntryInformation( i );

                    // this method works, but is quite a bit slower
                    //auto typname   = Symbols::GetTypeName( dse.ModuleBase, dse.TypeId );
                    //std::string const membexpr = util::make_string() << "* static_cast < " << Symbols::GetModuleNameString( DEBUG_MODNAME_MODULE, DEBUG_ANY_ID, dse.ModuleBase ) << '!' << typname << " * >( 0x" << std::hex << symgroup.GetSymbolOffset( i ) << " )";
                    //push_cppobj( L, symgroup.GetSymbolName( i ), ExtRemoteTyped( membexpr.c_str() ) );

                    // use the symbol entry information to create a DEBUG_TYPED_DATA, which can be used to construct an ExtRemoteTyped
                    DEBUG_TYPED_DATA dtd = {};
                    dtd.ModBase = dse.ModuleBase;
                    dtd.Offset  = dse.Offset;
                    dtd.Size    = dse.Size;
                    dtd.TypeId  = dse.TypeId;
                    dtd.Tag     = dse.Tag;
                    dtd.Flags = DEBUG_TYPED_DATA_IS_IN_MEMORY;

                    // the docs seem to be wrong on the meaning of DEBUG_TYPED_DATA::BaseTypeId. from what i can tell, it's
                    // the same as TypeId for anything other than a fundamental type. for fundamental types, each one uses a
                    // specific BaseTypeId. so there's a function in `util` that handles the mapping.
                    {
                        auto bt = util::type_basetype( Symbols::GetTypeName( dse.ModuleBase, dse.TypeId ) );
                        if ( util::basetype_t::none_ == bt )
                            dtd.BaseTypeId = dse.TypeId;
                        else
                            dtd.BaseTypeId = util::basetype_id( bt );
                    }

                    if ( SymTagUDT == dse.Tag )
                    {
                        dtd.Data = 0;
                    }
                    else if ( SymTagPointerType == dse.Tag )
                    {
                        ULONG const ptrsz = Control::IsPointer64Bit() ? 8 : 4;
                        ExtRemoteData erd( dtd.Offset, ptrsz );
                        dtd.Data = erd.GetPtr();
                    }
                    else if ( SymTagArrayType == dse.Tag )
                    {
                        dtd.Data = dtd.Offset;
                    }
                    else
                    {
                        ExtRemoteData erd( dtd.Offset, dtd.Size );
                        dtd.Data = erd.GetData( dtd.Size );
                    }

                    push_cppobj( L, symgroup.GetSymbolName( i ), ExtRemoteTyped( &dtd ) );

                    lua_settable( L, -3 );
                }
            }
        }
#endif
    }

    // pops the cppobj on the top of the stack, and
    // if the cppobj represents a class instance, pushes a table holding a list of its data members
    // if the cppobj represents any other kind of C++ variable or data, pushes nil
    int members( lua_State * L )
    {
        return push_member_list( L, false, false );
    }

    // pops the cppobj on the top of the stack, and
    // if the cppobj represents a class instance, pushes a table holding a list of base classes representations
    // if the cppobj represents any other kind of C++ variable or data, pushes nil
    int bases( lua_State * L )
    {
        return push_member_list( L, true, false );
    }

    // pops the cppobj at index -2 and argument at index -1, and:
    // for number arguments, treats the object as an array and pushes the specified element
    // for string arguments, pushes the named member
    int index( lua_State * L )
    {
        luaL_argcheck( L, ( lua_isnumber( L, -1 ) || lua_isstring( L, -1 ) ), 1, "member name or index expected" );
        auto const pobj = check_and_deref_cppobj( L, -2 );

        // number first: see if this object can be indexed
        {
            int ssuccess = 0, usuccess = 0;
            lua_Integer  sidx = 0;
            lua_Unsigned uidx = 0;
            sidx = lua_tointegerx( L, -1, &ssuccess );
#if 0
// lua_tounsignedx is gone, so i'm not sure what to do here
            if ( ! ssuccess )
            {
                // maybe it's just too big for a lua_Integer; try unsigned
                uidx = static_cast< lua_Unsigned >( lua_tounsignedx( L, -1, &usuccess ) );
            }
#endif
            if ( ssuccess || usuccess ) // if both fail, we'll check for a member name below
            {
                try
                {
                    lua_pop( L, 2 );

                    if ( ssuccess )
                    {
                        // auto child = ( *pobj )[ static_cast< LONG64 >( sidx ) ];
                        auto child = pobj->array_element( static_cast< LONG64 >( sidx ) );
                        push_cppobj( L, elem_name( sidx ), child );
                    }
                    else if ( usuccess )
                    {
                        // auto child = ( *pobj )[ static_cast< ULONG64 >( uidx ) ];
                        auto child = pobj->array_element( static_cast< ULONG64 >( uidx ) );
                        push_cppobj( L, elem_name( uidx ), child );
                    }
                    else
                        lua_pushnil( L );
    
                    return 1;
                }
                catch ( ExtRemoteException const & )
                {
                    if ( ssuccess )
                    {
                        lua_pushnil( L );
                        lua_pushstring( L, ( util::make_string() << "'" << pobj->long_name() << "' does not have an index '" << sidx << "'" ).c_str() );
                        return 2;
                    }
                    else if ( usuccess )
                    {
                        lua_pushnil( L );
                        lua_pushstring( L, ( util::make_string() << "'" << pobj->long_name() << "' does not have an index '" << uidx << "'" ).c_str() );
                        return 2;
                    }
                    else
                    {
                        lua_pushnil( L );
                        lua_pushstring( L, ( util::make_string() << "'" << pobj->long_name() << "' indexing failed" ).c_str() );
                        return 2;
                    }
                }
            }
        }

        std::string const member_name = lua_tostring( L, -1 );
        lua_pop( L, 2 );

        // string arg, check for member
        if ( ! pobj->has_member( member_name.c_str() ) )
        {
            lua_pushnil( L );
            lua_pushstring( L, ( util::make_string() << "'" << pobj->long_name() << "' does not have a member '" << member_name << "'" ).c_str() );
            return 2;
        }

        auto const child = pobj->member( member_name );
        if ( child.valid() )
        {
            push_cppobj( L, member_name, child );
            return 1;
        }

        lua_pushnil( L );
        lua_pushstring( L, ( util::make_string() << "problem retrieving member '" << member_name << "' from '" << pobj->long_name() << "'" ).c_str() );
        return 2;
    }

    // pops the cppobj on the top of the stack, and:
    // for fundamental types, pushes the object's value as a Lua value
    // for other types, pushes nil
    int flatten( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );

        // auto const tag  = pobj->m_Typed.Tag;
        auto const kind = pobj->kind();
        auto const size = pobj->size();

        typedef decltype( pobj->raw() ) unsigned_integral_t;
        static_assert( std::is_unsigned< unsigned_integral_t >::value, "calculations below assume this type is unsigned" );

        bool is_integral = false;
        bool is_unsigned = false;
        unsigned_integral_t unsigned_integral_value = 0;

        // NOTE: this entire block is pretty hackish. we're trying to find the best
        // native Lua value that fits the C++ value.
        if ( cppobj::kind_t::base_ == kind )
        {
            util::basetype_t const type = util::object_basetype( *pobj );

            if ( util::basetype_t::bool_ == type )
            {
                lua_pushboolean( L, pobj->as_bool() );
                return 1;
            }
            else if ( util::basetype_t::float_ == type )
            {
                lua_pushnumber( L, pobj->as_float() );
                return 1;
            }
            else if ( util::basetype_t::double_ == type )
            {
                lua_pushnumber( L, pobj->as_double() );
                return 1;
            }
            else if ( util::is_integral_type( type ) )
            {
                is_integral = true;
                is_unsigned = util::is_unsigned_type( type );
                unsigned_integral_value = pobj->raw();
            }
            else
            {
                lua_pushnil( L );
                return 1;
            }
        }
        else if ( cppobj::kind_t::enum_ == kind )
        {
            is_integral = true;
            is_unsigned = false; // this is not really correct if the enum is backed by an unsigned type; that case should probably be handled
            unsigned_integral_value = pobj->raw();
        }
        else if ( cppobj::kind_t::pointer_ == kind )
        {
            is_integral = true;
            is_unsigned = true;
            unsigned_integral_value = pobj->raw();
        }
        else if ( cppobj::kind_t::array_ == kind )
        {
            is_integral = true;
            is_unsigned = true;
            unsigned_integral_value = pobj->offset();
        }
        else
        {
            // anything else, return nil
            lua_pushnil( L );
            return 1;
        }

        // should only get here for integral values
        if ( ! is_integral )
        {
            return luaL_error( L, "non-integral type fell through to integral processing when flattening '%s'", pobj->long_name().c_str() );
        }

        // okay, if we got here, the type was some kind of integral value so we're going to work it out as best we can

        if ( is_unsigned )
        {
            // see if the value is small enough to fit
            if ( unsigned_integral_value <= std::numeric_limits< lua_Unsigned >::max() )
            {
                lua_pushinteger( L, static_cast< lua_Integer >( unsigned_integral_value ) );
                return 1;
            }

            // okay, even though it may involve some loss of precision because lua_Number
            // is usually a floating-point type, see if it can fit in a lua_Number
            if ( sizeof( unsigned_integral_value ) <= sizeof( lua_Number ) )
            {
                lua_pushnumber( L, static_cast< lua_Number >( unsigned_integral_value ) );
                return 1;
            }

            // couldn't fit in a lua_Number, so return nil
            lua_pushnil( L );
            return 1;
        }

        // try to find a correctly sized signed type to fit it in
        int pushed = util::push_signed_integral( L, unsigned_integral_value, size );
        if ( pushed > 0 )
            return pushed;

        // couldn't fit in a lua_Number, so return nil
        lua_pushnil( L );
        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // pushes the dbgeng string interpretation of the object's value
    int tostring( lua_State * L )
    {
        auto const pobj = check_and_deref_cppobj( L, -1 );
        lua_pop( L, 1 );
        lua_pushstring( L, pobj->as_string().c_str() );
        return 1;
    }

    // for the cppobj at index -2 and argument at index -1:
    // performs one iteration of ipairs by
    //  popping the argument at index -1, and
    //  pushing the next index, and value
    // the cppobj on the stack stays on the stack
    int ipairs_iterate( lua_State * L )
    {
        auto const pobj    = check_and_deref_cppobj( L, -2 );
        bool const isarray = cppobj::kind_t::array_ == pobj->kind();

        if ( ! isarray )
        {
            return luaL_error( L, "cannot iterate non-array '%s'", pobj->long_name().c_str() );
        }

        if ( lua_isnil( L, -1 ) ) // first iteration
        {
            lua_pop( L, 1 );

            // shouldn't need to check array bounds. if it's an array, it'll have at least one member
            lua_pushinteger( L, 0 );
            push_cppobj( L, elem_name( 0 ), pobj->array_element( 0 ) );
            return 2;
        }

        // to iterate over each element, calculate the array upper bound and go til we hit it
        ULONG const arraysize = pobj->size();
        ULONG const elemsize  = pobj->array_element( 0 ).size();
        if ( elemsize == 0 || arraysize < elemsize || ( arraysize % elemsize ) != 0 )
        {
            // something's wrong, we can't correctly calculate the array count
            lua_pop( L, 1 );
            lua_pushnil( L );
            lua_pushliteral( L, "error calculating array size" );
            return 2;
        }

        ULONG const count = arraysize / elemsize;

        int isnum = 0;
        lua_Unsigned const nextidx = 1 + static_cast< lua_Unsigned >( lua_tointegerx( L, -1, &isnum ) );
        lua_pop( L, 1 );

        if ( ! isnum )
        {
            lua_pushnil( L );
            lua_pushliteral( L, "invalid array index" );
            return 2;
        }

        if ( nextidx < count )
        {
            lua_pushinteger( L, static_cast< lua_Integer >( nextidx ) );
            push_cppobj( L, elem_name( nextidx ), pobj->array_element( nextidx ) );
            return 2;
        }

        lua_pushnil( L );
        lua_pushliteral( L, "array index too large" );
        return 2;
    }

    // replaces a cppobj on the top of the stack with:
    //  index -1: nil
    //  index -2: the cppobj
    //  index -3: an iterator function
    // used to start iteration of an array
    int ipairs( lua_State * L )
    {
        check_cppobj( L, -1 );
        lua_pushcfunction( L, ipairs_iterate );
        lua_insert( L, -2 );
        lua_pushnil( L );
        return 3;
    }

    // for the cppobj at index -2 and argument at index -1:
    // performs one iteration of pairs by
    //  popping the argument at index -1, and
    //  pushing the next key index, key name, and value
    // the cppobj on the stack stays on the stack
    int pairs_iterate( lua_State * L )
    {
        // TODO: this function needs to be completely reworked for cppobj_impl
        lua_pushnil( L );
        return 1;
#if 0
        auto const pobj = check_and_deref_cppobj( L, -2 );
        auto const module = pobj->m_Typed.ModBase;
        auto const type   = pobj->m_Typed.TypeId;
        bool const isarray = SymTagArrayType == pobj->m_Typed.Tag;

        if ( lua_isnil( L, -1 ) ) // first iteration
        {
            lua_pop( L, 1 );

            if ( isarray )
            {
                // shouldn't need to check array bounds. if it's an array, it'll have at least one member
                lua_pushunsigned( L, 0 );
                lua_pushunsigned( L, 0 );
                push_cppobj( L, elem_name( 0 ), pobj->ArrayElement( 0 ) );
                return 3;
            }
            else
            {
                HRESULT hr = S_OK;
                auto member_name = Symbols::GetFieldName( module, type, 0, &hr );
                if ( FAILED( hr ) )
                {
                    // this object has no members to iterate
                    lua_pushnil( L );
                    return 1;
                }
    
                lua_pushunsigned( L, 0 );
                lua_pushstring( L, member_name.c_str() );
                push_cppobj( L, member_name, pobj->Field( member_name.c_str() ) );
                return 3;
            }
        }

        if ( isarray )
        {
            // for array types, we'd like to iterate over each element
            // to do that, calculate the array upper bound and go til we hit it
            ULONG const arraysize = pobj->m_Typed.Size;
            ULONG const elemsize  = pobj->ArrayElement( 0 ).m_Typed.Size;
            if ( elemsize == 0 || arraysize < elemsize || ( arraysize % elemsize ) != 0 )
            {
                // something's wrong, we can't correctly calculate the array count
                lua_pop( L, 1 );
                lua_pushnil( L );
                lua_pushliteral( L, "error calculating array size" );
                return 2;
            }

            ULONG const count = arraysize / elemsize;

            int isnum = 0;
            lua_Unsigned const nextidx = 1 + lua_tounsignedx( L, -1, &isnum );
            lua_pop( L, 1 );

            if ( ! isnum )
            {
                lua_pushnil( L );
                lua_pushliteral( L, "invalid array index" );
                return 2;
            }

            if ( nextidx < count )
            {
                lua_pushunsigned( L, nextidx );
                lua_pushunsigned( L, nextidx );
                push_cppobj( L, elem_name( nextidx ), ( *pobj )[ static_cast< ULONG64 >( nextidx ) ] );
                return 3;
            }

            lua_pushnil( L );
            lua_pushliteral( L, "array index too large" );
            return 2;
        }
        else
        {
            int isnum = 0;
            lua_Unsigned const nextidx = 1 + lua_tounsignedx( L, -1, &isnum );
            lua_pop( L, 1 );

            if ( ! isnum )
            {
                lua_pushnil( L );
                lua_pushliteral( L, "invalid member index" );
                return 2;
            }

            HRESULT hr = S_OK;
            auto member_name = Symbols::GetFieldName( module, type, nextidx, &hr );
            if ( FAILED( hr ) )
            {
                // no members left
                lua_pushnil( L );
                return 1;
            }

            lua_pushunsigned( L, nextidx );
            lua_pushstring( L, member_name.c_str() );
            push_cppobj( L, member_name, pobj->Field( member_name.c_str() ) );
            return 3;
        }

#endif
    }

    // replaces a cppobj on the top of the stack with:
    //  index -1: nil
    //  index -2: the cppobj
    //  index -3: an iterator function
    // used to start iteration of the object's members
    int pairs( lua_State * L )
    {
        check_cppobj( L, -1 );
        lua_pushcfunction( L, pairs_iterate );
        lua_insert( L, -2 );
        lua_pushnil( L );
        return 3;
    }

    // subtraction
    // index -2 is the first operand, index -1 is the second
    // pop the operands and push the result
    int sub( lua_State * L )
    {
        if ( lua_isnoneornil( L, -2 ) || lua_isnoneornil( L, -1 ) )
        {
            return luaL_error( L, "not enough arguments to cppobj.__sub" );
        }

        auto pop1 = test_and_deref_cppobj( L, -2 );
        auto pop2 = test_and_deref_cppobj( L, -1 );

        // could happen if called directly from C++ and the wrong operands were on the stack
        if ( ! pop1 && ! pop2 )
        {
            return luaL_error( L, "invalid operands to cppobj.__sub" );
        }

        // we want to treat arrays like pointers
        if ( pop1 && cppobj::kind_t::array_ == pop1->kind() )
            // pop1 = std::make_shared< ExtRemoteTyped >( pop1->Dereference().GetPointerTo() );
            pop1 = std::make_shared< cppobj_impl >( pop1->dereference().reference() );
        if ( pop2 && cppobj::kind_t::array_ == pop2->kind() )
            // pop2 = std::make_shared< ExtRemoteTyped >( pop2->Dereference().GetPointerTo() );
            pop2 = std::make_shared< cppobj_impl >( pop2->dereference().reference() );

        if ( pop1 && pop2 ) // both operands are cppobjs
        {
            switch ( pop1->kind() )
            {
            case cppobj::kind_t::pointer_:
                {
                    switch ( pop2->kind() )
                    {
                    case cppobj::kind_t::pointer_:
                        {
                            lua_pop( L, 2 );

                            // ptr - ptr
                            // if the types are equal, return a scaled distance.
                            // if types aren't equal, return nil.
                            // compare the type strings and not the TypeIds, because it's possible the objects could be from different modules.
                            std::string const type1 = pop1->type();
                            std::string const type2 = pop2->type();
                            if ( type1 == type2 )
                            {
                                // be careful doing subtraction on unsigned values
                                auto const op1addy    = pop1->as_ptr();
                                auto const op2addy    = pop2->as_ptr();
                                auto const negative   = op1addy < op2addy;
                                auto const difference = negative ? op2addy - op1addy : op1addy - op2addy;
                                auto const objsize    = pop1->dereference().size();
                                auto const scaled     = difference / objsize;
                                // I'm not sure if this will really affect anything, but it's possible this could be
                                // more precise as long as the scaled number can fit in a lua_Unsigned
                                if ( scaled < std::numeric_limits< lua_Unsigned >::max() )
                                    lua_pushinteger( L, static_cast< lua_Integer >( scaled ) );
                                else
                                    lua_pushnumber( L, static_cast< lua_Number >( scaled ) );
                                if ( negative )
                                    lua_arith( L, LUA_OPUNM );
                            }
                            else
                            {
                                lua_pushnil( L );
                            }

                            return 1;
                        }
                        break;

                    case cppobj::kind_t::base_:
                        {
                            lua_pop( L, 2 );

                            // ptr - numobj
                            // scaled arithmetic returning a pointer to the new location
                            util::basetype_t const op2type = util::object_basetype( *pop2 );
                            if ( ! util::is_integral_type( op2type ) )
                            {
                                return luaL_error( L, "only integral arithmetic is supported with pointers; attempting to subtract '%s' from '%s'", pop2->long_name().c_str(), pop1->long_name().c_str() );
                            }

                            auto const size = pop2->size();
                            ULONG64 const ul = pop2->raw();
                            if ( util::is_unsigned_type( op2type ) )
                            {
                                if ( ul > static_cast< ULONG64 >( std::numeric_limits< ptrdiff_t >::max() ) )
                                {
                                    return luaL_error( L, "array index '%d' too large", ul );
                                }

                                std::string const name = util::make_string() << pop1->long_name() << " - " << ul;
                                push_cppobj( L, name, pop1->array_element( - static_cast< ptrdiff_t >( ul ) ).reference() );
                            }
                            else
                            {
                                LONG64 const sl = util::sign_cast< LONG64 >( ul, size );
                                if ( sl > static_cast< LONG64 >( std::numeric_limits< ptrdiff_t >::max() ) )
                                {
                                    return luaL_error( L, "array index '%d' too large", ul );
                                }

                                std::string const name = util::make_string() << pop1->long_name() << " - " << sl;
                                push_cppobj( L, name, pop1->array_element( - static_cast< ptrdiff_t >( sl ) ).reference() );
                            }

                            return 1;
                        }
                        break;

                    default:
                        // ptr - anything else, return nil
                        lua_pop( L, 2 );
                        lua_pushnil( L );
                        return 1;
                    }
                }
                break;

            case cppobj::kind_t::base_:
            case cppobj::kind_t::enum_:
                {
                    // if the first operand is a basetype or enum, we only support subtracting
                    // another basetype or enum, and return a Lua number
                    switch ( pop2->kind() )
                    {
                    case cppobj::kind_t::base_:
                    case cppobj::kind_t::enum_:
                        {
                            // flatten the operands into Lua values and let Lua do the work
                            flatten( L );
                            lua_insert( L, -2 ); // swap operands so we can flatten op1
                            flatten( L );
                            lua_insert( L, -2 ); // swap operands back
                            lua_arith( L, LUA_OPSUB );
                            return 1;
                        }
                        break;
                    }

                    lua_pop( L, 2 );
                    lua_pushnil( L );
                    return 1;
                }
                break;

            default:
                // no support for other types in first operand
                lua_pop( L, 2 );
                lua_pushnil( L );
                return 1;
            }
        }
        else if ( pop1 ) // only op1 is a cppobj
        {
            switch ( pop1->kind() )
            {
            case cppobj::kind_t::pointer_:
                {
                    // ptr - luanum
                    // scaled arithmetic returning a pointer to the new location
                    int success = 0;
                    lua_Integer op2 = lua_tointegerx( L, -1, &success );
                    if ( ! success )
                    {
                        return luaL_error( L, "could not convert second operand to integer when attempting to subtract from '%s'", pop1->long_name().c_str() );
                    }

                    lua_pop( L, 2 );

                    if ( success )
                    {
                        std::string const name = util::make_string() << pop1->long_name() << " - " << op2;
                        push_cppobj( L, name, pop1->array_element( - op2 ).reference() );
                    }
                    else
                        lua_pushnil( L );

                    return 1;
                }
                break;

            case cppobj::kind_t::base_:
            case cppobj::kind_t::enum_:
                {
                    // numobj - luanum
                    // flatten op1 and let Lua do the work
                    lua_insert( L, -2 );
                    flatten( L );
                    lua_insert( L, -2 );
                    lua_arith( L, LUA_OPSUB );
                    return 1;
                }
                break;

            default:
                // no support for other types in first operand
                lua_pop( L, 2 );
                lua_pushnil( L );
                return 1;
            }
        }
        else if ( pop2 ) // only op2 is a cppobj
        {
            // op1 must be a Lua value, so we only attempt regular subtraction
            switch ( pop2->kind() )
            {
            case cppobj::kind_t::base_:
            case cppobj::kind_t::enum_:
                {
                    flatten( L );
                    lua_arith( L, LUA_OPSUB );
                    return 1;
                }
                break;

            default:
                lua_pop( L, 2 );
                lua_pushnil( L );
                return 1;
            }
        }

        // should never get here
        return 0;
    }

    // equality
    // index -2 is the first operand, index -1 is the second
    // pop the operands and push the result
    int eq( lua_State * L )
    {
        if ( lua_isnoneornil( L, -2 ) || lua_isnoneornil( L, -1 ) )
        {
            return luaL_error( L, "not enough arguments to cppobj.__eq" );
        }

        auto pop1 = check_and_deref_cppobj( L, -2 );
        auto pop2 = check_and_deref_cppobj( L, -1 );

        // we want to treat arrays like pointers
        if ( cppobj::kind_t::array_ == pop1->kind() )
            // pop1 = std::make_shared< ExtRemoteTyped >( pop1->Dereference().GetPointerTo() );
            pop1 = std::make_shared< cppobj_impl >( pop1->dereference().reference() );
        if ( cppobj::kind_t::array_ == pop2->kind() )
            // pop2 = std::make_shared< ExtRemoteTyped >( pop2->Dereference().GetPointerTo() );
            pop2 = std::make_shared< cppobj_impl >( pop2->dereference().reference() );

        auto const op1kind = pop1->kind();
        auto const op2kind = pop2->kind();

        // ptr == ptr
        if ( cppobj::kind_t::pointer_ == op1kind && cppobj::kind_t::pointer_ == op2kind )
        {
            // simply compare the pointer values
            // don't try comparing types, because it leads to all kinds of unnecessary messiness
            lua_pop( L, 2 );
            lua_pushboolean( L, pop1->as_ptr() == pop2->as_ptr() );
        }

        // object == object
        else if ( cppobj::kind_t::class_ == op1kind && cppobj::kind_t::class_ == op2kind )
        {
            // compare addresses and types
            int equal = 0;
            if ( pop1->offset() == pop2->offset() )
            {
                std::string const op1type = pop1->type();
                std::string const op2type = pop2->type();
                equal = op1type == op2type;
            }
            lua_pop( L, 2 );
            lua_pushboolean( L, equal );
        }

        // base type or enum == base type or enum
        else if ( ( cppobj::kind_t::base_ == op1kind || cppobj::kind_t::enum_ == op1kind ) && ( cppobj::kind_t::base_ == op2kind || cppobj::kind_t::enum_ == op2kind ) )
        {
            // flatten them and let Lua do it
            flatten( L );
            lua_insert( L, -2 ); // swap operands so we can flatten op1
            flatten( L );
            lua_insert( L, -2 ); // swap operands back, although it probably doesn't matter
            int equal = lua_compare( L, -2, -1, LUA_OPEQ );
            lua_pop( L, 2 );
            lua_pushboolean( L, equal );
        }

        // any other comparison is false
        else
        {
            lua_pop( L, 2 );
            lua_pushboolean( L, 0 );
        }

        return 1;
    }

    // pops the cppobj on the top of the stack, and
    // destructs the C++ side of the object
    int gc( lua_State * L )
    {
        auto const ppobj = check_cppobj( L, -1 );
        ppobj->~cppobj_t();
        lua_pop( L, 1 );
        return 0;
    }

    struct luaL_Reg const library_funcs[] =
    {
        { "new"       , new_cppobj  }, 
        { "validate"  , validate    },
        { "type"      , type        }, 
        { "kind"      , kind        },
        { "name"      , name        }, 
        { "long_name" , long_name   }, 
        { "sizeof"    , size_of     }, 
        { "addressof" , address_of  }, 
        { "deref"     , dereference }, 
        { "members"   , members     },
        { "bases"     , bases       },
        { nullptr     , nullptr     }, 
    };

    struct luaL_Reg const metamethods[] =
    {
        { "__index"    , index    }, 
        { "__len"      , flatten  }, 
        { "__tostring" , tostring }, 
        // { "__pairs"    , pairs    },
        { "__ipairs"   , ipairs   },
        { "__sub"      , sub      },
        { "__eq"       , eq       },
        { "__gc"       , gc       }, 
        { nullptr      , nullptr  }, 
    };

    // register the functions used by cppobj
    int register_funcs( lua_State * L )
    {
        // cppobj functions
        luaL_newlib( L, library_funcs );
        lua_setglobal( L, cppobj_typename );

        // metatable used for wrapped objects
        luaL_newmetatable( L, cppobj_typename );
        luaL_setfuncs( L, metamethods, 0 );
        lua_pop( L, 1 );

        return 0;
    }
}
