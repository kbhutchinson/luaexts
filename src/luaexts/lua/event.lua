--[[
-- Eventing subsystem
--]]

local luaexts = _ENV[ EXTNAME ]

-- registered event handlers
-- The layout looks like:
-- {
--   <event-name> =
--   {
--     [1] = <handler1>,
--     [2] = <handler2>,
--     ... -- more handlers
--     ;
--     <handler1> =
--     {
--       state = <state1>
--     },
--     <handler2> =
--     {
--       state = <state2>
--     },
--     ... -- more handler definitions
--   },
--   ... -- more events
-- }
--
-- The top level table maps an event name to an event-handlers table.
--
-- The array part of an event-handlers table is a simple list of each handler object in the order it was registered.
--
-- The hash part of an event-handlers table maps a handler object to a handler-details table. A handler object is some
-- callable value: a function, or a value with a __call metamethod.
--
-- The handler-details table contains the following keys:
--   state: the value passed in with the handler during registration, to be passed back to the handler when the event fires
--
local registered_event_handlers = {}


-- Registers a handler for a given event. The handler should be a callable value and will be passed a table with the following entries:
--   {
--     event     = <event-name>, -- the event name
--     eventargs = <event-args>, -- a table with event-specific information
--     state     = <state>, -- the state object passed in during registration
--     handler   = <handler-object>, -- the handler itself, which can be used to deregister
--   }
-- params:
--   event: name of the event
--   handler: callable value, to be called when the event fires
--   state: some value to be passed back to the handler when the event fires
-- returns:
--   nothing
local function register_event_handler( event, handler, state )
    luaexts.arg_type_check( EXTNAME .. '.event.register_event_handler', { { 'string', event } } )

    if nil == handler then
        error( 'attempted to register a nil handler for event \'' .. event .. '\'', 2 )
    end

    if 'table' ~= type( registered_event_handlers[ event ] ) then
        registered_event_handlers[ event ] = {}
    end

    local event_handlers = registered_event_handlers[ event ]
    if event_handlers[ handler ] then
        -- don't allow double registration
        return
    end

    event_handlers[ handler ] =
    {
        [ 'state' ] = state
    }
    table.insert( event_handlers, handler )
end


-- Deregisters a handler for a given event
-- params:
--   event: name of the event
--   handler: handler to deregister
-- returns:
--   nothing
local function deregister_event_handler( event, handler )
    luaexts.arg_type_check( EXTNAME .. '.event.deregister_event_handler', { { 'string', event } } )

    if nil == handler then
        error( 'attempted to deregister a nil handler for event \'' .. event .. '\'', 2 )
    end

    local event_handlers = registered_event_handlers[ event ]
    if 'table' == type( event_handlers ) then
        event_handlers[ handler ] = nil
    end
end


-- Run the handlers for a given event. In general, this function should only be called by the code that
-- implements a given event, and not by code that is interested in reacting to the event.
-- params:
--   event: name of the event
--   eventargs: table with event-specific information for the handler
--   reversed: boolean indicating whether the handlers should be run in the reverse order of registration
local function run_event_handlers( event, eventargs, reversed )
    luaexts.arg_type_check( EXTNAME .. '.event.run_event_handlers', { { 'string', event } } )

    local event_handlers = registered_event_handlers[ event ]
    if 'table' ~= type( event_handlers ) then
        return
    end

    local need_compacting = false
    local count = #event_handlers

    local start = 1
    local stop  = count
    local step  = 1
    if reversed then
        start = count
        stop  = 1
        step  = -1
    end
  
    for i = start, stop, step do
        local handler = event_handlers[ i ]
        local handler_details = event_handlers[ handler ]
        if handler_details then
            handler(
                {
                    event     = event,
                    eventargs = eventargs,
                    state     = handler_details.state,
                    handler   = handler
                }
            )

        else
            -- this handler was deregistered.
            -- avoid table.remove() and do compacting as a second pass, which avoids multiple table adjustments if multiple
            -- handlers were deregistered.
            event_handlers[ i ] = nil
            need_compacting = true
        end
    end
  
    -- compact empty array elements
    if need_compacting then
        local j = 0
        for i = 1, count do
            if event_handlers[ i ] ~= nil then
                j = j + 1
                event_handlers[ j ] = event_handlers[ i ]
            end
        end
        for i = j + 1, count do
            event_handlers[ i ] = nil
        end
    end
end


return
{
    register_event_handler = register_event_handler,
    deregister_event_handler = deregister_event_handler,
    run_event_handlers = run_event_handlers,
}

