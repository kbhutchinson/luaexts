#pragma once

struct lua_State;

// Lua projections for the IDebugSymbolGroup2 interface
namespace dbgeng { namespace symbolgroup
{
    // [-0,+0]
    // if the value at the given index is a symbolgroup userdata, returns the associated
    // IDebugSymbolGroup2 address. else reports error.
    IDebugSymbolGroup2 * xlua_check_symbolgroup( lua_State * L, int arg );

    // [-0,+1]
    // pushes:
    //   new userdata representing a symbolgroup, with associated metatable
    void xlua_push_symbolgroup( lua_State * L, IDebugSymbolGroup2 * sg );

    // add_symbol( sg, symexpr, index )
    // adds a symbol to a symbol group
    // params:
    //   sg: symbol group to add the symbol to
    //   symexpr: string expression evaluated to determine the symbol's type
    //   index: specifies the desired index of the new symbol. use 'dbgeng.ANY_ID' to append
    //     to the end of the group.
    // returns:
    //   index of the new symbol
    int l_add_symbol( lua_State * L );

    // expand_symbol( sg, index, expand )
    // adds or removes a symbol's children from the symbol group
    // params:
    //   sg: symbol group to expand or collapse
    //   index: index of the symbol to expand or collapse
    //   expand: boolean indicating desired operation; true for expand, false for collapse
    // returns:
    //   true if expansion succeeded.
    //   false if the symbol has no children.
    //   nil, 'E_INVALIDARG', and the code dbgeng.hr.E_INVALIDARG if the symbol is
    //   already at the maximum expansion depth, so it's children could not be added.
    int l_expand_symbol( lua_State * L );

    // get_number_symbols( sg )
    // returns the number of symbols in a symbol group
    // params:
    //   sg: symbol group to retrieve the number of symbols for
    // returns:
    //   number of symbols in the group
    int l_get_number_symbols( lua_State * L );

    // get_symbol_entry_information( sg, index )
    // returns information about a symbol in a symbol group
    // params:
    //   sg: symbol group to retrieve information from
    //   index: index of the symbol to retrieve information for
    // returns:
    //   symbol information, as a table of named fields:
    //     module: module base address
    //     offset: memory location of the symbol
    //     id: id of the symbol; if not known, will be equal to dbgeng.INVALID_OFFSET
    //     arg64: interpretation depends on the type of the symbol. if not known, will be 0.
    //     size: size, in bytes, of the symbol's value
    //     type_id: type id of the symbol
    //     name_size: size, in characters, of the symbol's name
    //     token: managed token of the symbol. if not known or symbol has no token, will be 0.
    //     tag: symbol tag of the symbol; will equal one of the values in the 'dbgeng.symtag' table
    //     arg32: interpretation of arg32 depends on the type of the symbol. currently, equals the
    //       register that holds the value or pointer to the value of the symbol. if the symbol is
    //       not held in a register or the register is not known, will be 0.
    int l_get_symbol_entry_information( lua_State * L );

    // get_symbol_name( sg, index )
    // returns the name of a symbol in a symbol group
    // params:
    //   sg: symbol group to retrieve the name from
    //   index: index of the symbol to retrieve the name for
    // returns:
    //   name of the symbol
    int l_get_symbol_name( lua_State * L );

    // get_symbol_offset( sg, index )
    // returns the location in the target's memory of a symbol in a symbol group, if the
    // symbol has an absolute address
    // params:
    //   sg: symbol group to retrieve the offset from
    //   index: index of the symbol to retrieve the offset for
    // returns:
    //   memory location of the symbol
    int l_get_symbol_offset( lua_State * L );

    // get_symbol_parameters( sg, start, count )
    // returns symbol parameters that describe the specified symbols from the symbol group
    // params:
    //   sg: symbol group to retrieve parameters from
    //   start: index in the symbol group of the first symbol to retrieve parameters for
    //   count: number of symbols to retrieve parameters for
    // returns:
    //   array of tables holding the requested symbols' parameters as named fields:
    //     module: module base address
    //     type_id: type id
    //     parent_symbol: index within the symbol group of the symbol's parent
    //     sub_elements: number of children of the symbol
    //     flags: bitfield combination of the values in the 'dbgeng.symbolgroup.symbol_flag' table
    //     expansion_depth: expansion depth of the symbol within the symbol group. the depth
    //       of a child symbol is always one more than the depth of its parent.
    int l_get_symbol_parameters( lua_State * L );

    // get_symbol_register( sg, index )
    // returns the register that contains the value or a pointer to the value of a symbol in a symbol group
    // params:
    //   sg: symbol group to retrieve the register data from
    //   index: index of the symbol to retrieve the register data for
    // returns:
    //   index of the register containing the value or pointer to the value of the symbol
    int l_get_symbol_register( lua_State * L );

    // get_symbol_size( sg, index )
    // returns the size of a symbol's value
    // params:
    //   sg: symbol group to retrieve the size from
    //   index: index of the symbol to retrieve the size for
    // returns:
    //   size, in bytes, of the symbol's value
    int l_get_symbol_size( lua_State * L );

    // get_symbol_type_name( sg, index )
    // return the name of the specified symbol's type
    // params:
    //   sg: symbol group to retrieve the type name from
    //   index: index of the symbol to retrieve the type name for
    // returns:
    //   name of the symbol's type
    int l_get_symbol_type_name( lua_State * L );

    // get_symbol_value_text( sg, index )
    // returns a string that represents the value of a symbol
    // params:
    //   sg: symbol group to retrieve the value text from
    //   index: index of the symbol to retrieve the value text for
    // returns:
    //   value of the symbol as a string
    int l_get_symbol_value_text( lua_State * L );

    // remove_symbol_by_index( sg, index )
    // removes specified symbol from the symbol group. child symbols cannot be removed, the
    // parent symbol must be removed.
    // params:
    //   sg: symbol group to remove the symbol from
    //   index: index of symbol to remove
    // returns:
    //   true if the removal succeeded
    int l_remove_symbol_by_index( lua_State * L );

    // remove_symbol_by_name( sg, name )
    // removes specified symbol from the symbol group. child symbols cannot be removed, the
    // parent symbol must be removed.
    // params:
    //   sg: symbol group to remove the symbol from
    //   name: name of symbol to remove
    // returns:
    //   true if the removal succeeded
    int l_remove_symbol_by_name( lua_State * L );

    // write_symbol( sg, index, value )
    // sets the value of the specified symbol
    // params:
    //   sg: symbol group containing the symbol whose value to set
    //   index: index of the symbol within the symbol group
    //   value: string containing C++ expression to evaluate to set the new value
    // returns:
    //   true if the operation was successful
    int l_write_symbol( lua_State * L );

    // [-0,+0]
    // dbgeng::symbolgroup api init
    void xlua_init( lua_State * L );

}}
