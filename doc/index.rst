.. include:: ../readme.rst

.. toctree::
    :maxdepth: 2
    :caption: User Guide

    user-guide/building
    user-guide/general-usage

API
===

The luaexts API is divided into two categories: 1) the exposed dbgeng COM API; and 2) the additional
API that luaexts provides, sometimes as higher level constructs on top of the dbgeng API like
:mod:`cppobj`, and sometimes as Lua-extension scaffolding like register_extension().

.. toctree::
    :maxdepth: 2
    :caption: API Reference

    api/luaexts
    api/dbgeng

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

