dbgeng.symbolgroup
===================

.. module:: dbgeng.symbolgroup
    :synopsis: functions that manipulate symbol groups

The symbol group functions are accessible from the global *dbgeng.symbolgroup* table, and represent
the functions from the IDebugSymbolGroup_ interfaces of the `dbgeng COM API`_.

Symbol group objects are created from the :func:`.create_symbol_group` function in :mod:`dbgeng.symbols`.

Symbol groups are disposed automatically by garbage collection when the object is no longer referenced
by any variables.

All of the *symbolgroup* methods are object methods, which means they can be called using Lua object
method syntax. Assuming a variable *sg* containing a symbol group object, this call would return the
number of symbols contained in the symbol group:

::

    local numsyms = sg:get_number_symbols()

This is really just syntatic sugar for the more verbose:

::

    local numsyms = dbgeng.symbolgroup.get_number_symbols( sg )

.. _IDebugSymbolGroup: https://msdn.microsoft.com/en-us/library/ff550838

Object methods
--------------

.. method:: add_symbol( symexpr, index ) -> integer

    Adds a symbol to the symbol group.
    See `AddSymbol <https://msdn.microsoft.com/en-us/library/ff537925>`_.

    :param string symexpr: C++ expression representing the symbol to add, which can include pointer,
        array, and structure dereferencing
    :param integre index: desired index within the symbol group for the added symbol; pass
        :data:`dbgeng.ANY_ID` to append the symbol to the end
    :returns: index of the newly added symbol

.. method:: expand_symbol( index, expand ) -> boolean

    Adds or removes the children of a symbol within the symbol group.
    See `ExpandSymbol <https://msdn.microsoft.com/en-us/library/ff543271>`_.

    :param integer index: index of the symbol to expand or collapse
    :param boolean expand: ``true`` to expand, ``false`` to collapse
    :returns: indication of success of the operation

        * ``true`` if the operation succeeded
        * ``false`` if the symbol had no children to add
        * if the symbol is already at the maximum expansion depth, so that its children could not be
          added, returns the three values: ``nil``, ``'E_INVALIDARG'``, and :data:`dbgeng.hr.E_INVALIDARG <dbgeng.hr>`

.. method:: get_number_symbols() -> integer

    Returns the number of symbols in the symbol group.
    See `GetNumberSymbols <https://msdn.microsoft.com/en-us/library/ff547975>`_.

.. method:: get_symbol_entry_information( index ) -> table of symbol information

    Returns information about a symbol in a symbol group.
    See `GetSymbolEntryInformation <https://msdn.microsoft.com/en-us/library/ff548487>`_.

    :param integer index: index of the symbol whose information to retrieve
    :returns: table containing the following fields:

        * ``module``    : module base address
        * ``offset``    : memory location of the symbol
        * ``id``        : id of the symbol; if not known, will be equal to
          :data:`dbgeng.INVALID_OFFSET`
        * ``arg64``     : interpretation depends on the type of the symbol; if not known, will be 0
        * ``size``      : size of the symbol's value, in bytes
        * ``type_id``   : type id of the symbol
        * ``name_size`` : size of the symbol's name, in characters
        * ``token``     : managed token of the symbol; if not known or has none, will be 0
        * ``tag``       : symbol tag of the symbol; will equal one of the values in the
          :data:`dbgeng.symtag` table
        * ``arg32``     : interpretation depends on the type of the symbol; currently, equals the
          register that holds the value or pointer to the value of the symbol; if the symbol is not
          held in a register or the register is not known, will be 0

.. method:: get_symbol_name( index ) -> string

    Returns the name of a symbol in the symbol group.
    See `GetSymbolName <https://msdn.microsoft.com/en-us/library/ff549121>`_.

    :param integer index: index of the symbol whose information to retrieve

.. method:: get_symbol_offset( index ) -> integer

    Returns the memory location in the target's memory of a symbol in the symbol group, if the
    symbol has an absolute address.
    See `GetSymbolOffset <https://msdn.microsoft.com/en-us/library/ff549135>`_.

    :param integer index: index of the symbol whose information to retrieve

.. method:: get_symbol_parameters( start, count ) -> array of symbol parameter information

    Returns symbol parameters that describe the specified symbols from the symbol group.
    See `GetSymbolParameters <https://msdn.microsoft.com/en-us/library/ff549146>`_.

    :param integer start: index of the first symbol for which to retrieve parameters
    :param integer count: number of symbols for which to retrieve parameters
    :returns: array of tables, each of which contains the following fields:

        * ``module``          : module base address
        * ``type_id``         : type id of the symbol
        * ``parent_symbol``   : index within the symbol group of the symbol's parent
        * ``sub_elements``    : number of children of the symbol
        * ``flags``           : bitfield combination of the values in the :data:`.symbol_flag` table
        * ``expansion_depth`` : expansion depth of the symbol within the symbol group; the depth of a
          child symbol is always one more than the depth of its parent

.. method:: get_symbol_register( index ) -> integer

    Returns the index of the register that contains the value or a pointer to the value of a symbol
    in the symbol group.
    See `GetSymbolRegister <https://msdn.microsoft.com/en-us/library/ff549165>`_.

    :param integer index: index of the symbol whose information to retrieve

.. method:: get_symbol_size( index ) -> integer

    Returns the size of a symbol's value, in bytes.
    See `GetSymbolSize <https://msdn.microsoft.com/en-us/library/ff549169>`_.

    :param integer index: index of the symbol whose information to retrieve

.. method:: get_symbol_type_name( index ) -> string

    Returns the name of a symbol's type.
    See `GetSymbolTypeName <https://msdn.microsoft.com/en-us/library/ff549188>`_.

    :param integer index: index of the symbol whose information to retrieve

.. method:: get_symbol_value_text( index ) -> string

    Returns a string representation of a symbol's value.
    See `GetSymbolValueText <https://msdn.microsoft.com/en-us/library/ff549201>`_.

    :param integer index: index of the symbol whose information to retrieve

.. method:: remove_symbol_by_index( index ) -> boolean

    Removes the specified symbol from the symbol group. Child symbols cannot be removed using this
    method; the parent symbol must be removed, which will remove the children as well.
    See `RemoveSymbolByIndex <https://msdn.microsoft.com/en-us/library/ff554510>`_.

    :param integer index: index of the symbol to remove
    :returns: ``true`` if the operation succeeded, otherwise ``nil``

.. method:: remove_symbol_by_name( name ) -> boolean

    Removes the specified symbol from the symbol group. Child symbols cannot be removed using this
    method; the parent symbol must be removed, which will remove the children as well.
    See `RemoveSymbolByName <https://msdn.microsoft.com/en-us/library/ff554518>`_.

    :param string name: name of the symbol to remove
    :returns: ``true`` if the operation succeeded, otherwise ``nil``

.. method:: write_symbol( index, value ) -> boolean

    Sets the value of a symbol in the symbol group.
    See `WriteSymbol <https://msdn.microsoft.com/en-us/library/ff561457>`_.

    :param integer index: index of the symbol to set
    :param string value: C++ expression that is evaluated to determine the symbol's new value
    :returns: ``true`` if the operation is successful, otherwise ``nil``

Other
-----

.. data:: symbol_flag

    * ``EXPANDED``    : The children of the symbol are part of the symbol group.
    * ``READ_ONLY``   : The symbol represents a read-only variable.
    * ``IS_ARRAY``    : The symbol represents an array variable.
    * ``IS_FLOAT``    : The symbol represents a floating-point variable.
    * ``IS_ARGUMENT`` : The symbol represents an argument passed to a function.
    * ``IS_LOCAL``    : The symbol represents a local variable in a scope.

    See `DEBUG_SYMBOL_XXX <https://msdn.microsoft.com/en-us/library/ff541692>`_.
