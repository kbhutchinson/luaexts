#include "stdafx.h"

#include "dbgeng-symbols.h"
#include "dbgeng-symbolgroup.h"
#include "dbgeng-common.h"
#include "util.h"
#include <lua.hpp>

// Lua projections for the IDebugSymbols interface
namespace dbgeng { namespace symbols
{
    // create_symbol_group()
    // creates a new symbol group
    // returns:
    //   new userdata representing a symbol group
    int l_create_symbol_group( lua_State * L )
    {
        IDebugSymbolGroup2 * sg = nullptr;

        HRESULT hr = g_Ext->m_Symbols3->CreateSymbolGroup2( &sg );
        if ( SUCCEEDED( hr ) )
        {
            symbolgroup::xlua_push_symbolgroup( L, sg );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_module_name_string( which, index, base )
    // returns the name of the specified module
    // params:
    //   which: which of the module's names to return; can be a value
    //     from the 'dbgeng.control.modname' table:
    //     IMAGE: name of the executable file, including the extension
    //     MODULE: usually just the file name without the extension
    //     LOADED_IMAGE: same as the image name
    //     SYMBOL_FILE: symbol file name; if no symbols have been loaded, this is the name of the executable file instead
    //     MAPPED_IMAGE: mapped image name
    //  index: index of the module; if set to 'dbgeng.ANY_ID', the 'base' parameter is used instead
    //  base: if 'index' parameter is 'dbgeng.ANY_ID', specifies the module base address; otherwise, it's ignored
    int l_get_module_name_string( lua_State * L )
    {
        auto which = static_cast< ULONG >( luaL_checkinteger( L, 1 ) );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        auto base  = static_cast< ULONG64 >( luaL_checkinteger( L, 3 ) );

        HRESULT hr = S_OK;
        ULONG namesize = 0;

        if ( SUCCEEDED( hr = g_Ext->m_Symbols2->GetModuleNameString( which, index, base, nullptr, 0, &namesize ) ) && namesize > 0 )
        {
            util::charbuf namebuf( namesize + 1 ); // for null terminator

            if ( SUCCEEDED( hr = g_Ext->m_Symbols2->GetModuleNameString( which, index, base, namebuf, namesize, nullptr ) ) )
            {
                lua_pushstring( L, namebuf );
                return 1;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_type_id( symbol )
    // params:
    //   symbol: symbol expression
    // returns:
    //   module base address
    //   type id
    int l_get_symbol_type_id( lua_State * L )
    {
        auto expr = luaL_checkstring( L, 1 );
        ULONG64 module = 0;
        ULONG typid = 0;

        HRESULT hr = g_Ext->m_Symbols->GetSymbolTypeId( expr, &typid, &module );

        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, module );
            lua_pushinteger( L, typid );
            return 2;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_type_id( module, typename )
    // returns the type id for a type given its module and name
    // params:
    //   module: module base address
    //   typename: type name or symbol expression
    // returns:
    //   type id
    int l_get_type_id( lua_State * L )
    {
        auto module = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );
        auto expr   = luaL_checkstring( L, 2 );
        ULONG typid = 0;

        HRESULT hr = g_Ext->m_Symbols->GetTypeId( module, expr, &typid );

        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, typid );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_type_name( module, typeid )
    // returns the name of a type specified by its module and type id
    // params:
    //   module: module base address
    //   typeid: type id
    // returns:
    //   type name
    int l_get_type_name( lua_State * L )
    {
        auto module = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );
        auto typid  = static_cast< ULONG   >( luaL_checkinteger( L, 2 ) );
        HRESULT hr = S_OK;
        ULONG namesize = 0;

        if ( SUCCEEDED( hr = g_Ext->m_Symbols->GetTypeName( module, typid, nullptr, 0, &namesize ) ) && namesize > 0 )
        {
            util::charbuf namebuf( namesize + 1 ); // for null terminator

            if ( SUCCEEDED( hr = g_Ext->m_Symbols->GetTypeName( module, typid, namebuf, namesize, nullptr ) ) )
            {
                lua_pushstring( L, namebuf );
                return 1;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_type_size( module, typeid )
    // returns the size of a type specified by its module and type id
    // params:
    //   module base address
    //   type id
    // returns:
    //   type size
    int l_get_type_size( lua_State * L )
    {
        auto module = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );
        auto typid  = static_cast< ULONG   >( luaL_checkinteger( L, 2 ) );
        ULONG size = 0;

        HRESULT hr = g_Ext->m_Symbols->GetTypeSize( module, typid, &size );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, size );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    struct enum_mapping const module_names[] =
    {
        { "IMAGE"        , DEBUG_MODNAME_IMAGE        },
        { "MODULE"       , DEBUG_MODNAME_MODULE       },
        { "LOADED_IMAGE" , DEBUG_MODNAME_LOADED_IMAGE },
        { "SYMBOL_FILE"  , DEBUG_MODNAME_SYMBOL_FILE  },
        { "MAPPED_IMAGE" , DEBUG_MODNAME_MAPPED_IMAGE },
        { nullptr, 0 }
    };

    struct luaL_Reg const library_funcs[] =
    {
        { "create_symbol_group", l_create_symbol_group },
        { "get_module_name_string", l_get_module_name_string },
        { "get_symbol_type_id", l_get_symbol_type_id },
        { "get_type_id", l_get_type_id },
        { "get_type_name", l_get_type_name },
        { "get_type_size", l_get_type_size },
        { nullptr, nullptr },
    };

    // [-0,+0]
    // dbgeng::symbols api init
    void xlua_init( lua_State * L )
    {
        // push dbgeng.symbols table
        xlua_get_dbgeng_subtable( L, "symbols" );
        luaL_setfuncs( L, library_funcs, 0 );

        // setup the table of modname types
        luaL_getsubtable( L, -1, "modname" );
        xlua_set_enums( L, module_names );
        lua_pop( L, 1 );

        // pop dbgeng.symbols table
        lua_pop( L, 1 );
    }


}}
