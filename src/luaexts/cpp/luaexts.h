// Definition of the main extension class and its methods

#pragma once

#include "bridge.h"
#include "cppobj_impl.h"

#include <stack>
#include <memory>

#define EXTNAME "luaexts"

namespace luaexts
{
    // current version of the extension
    extern unsigned short const major_version;
    extern unsigned short const minor_version;
    extern unsigned short const patch_version;

    // event names
    extern char const * const event_command_begin;
    extern char const * const event_command_end;
    extern char const * const event_registered_command_begin;
    extern char const * const event_registered_command_end;
    extern char const * const event_help_command_begin;
    extern char const * const event_help_command_end;
}

class EXT_CLASS : public ExtExtension
{
public:
    static EXT_CLASS & inst();

protected:
    virtual HRESULT Initialize() override;
    virtual void Uninitialize() override;

public:
    // Exposed extension commands

    EXT_COMMAND_METHOD( reinitialize ); // reset the extension

    EXT_COMMAND_METHOD( lua );     // run a lua chunk
    EXT_COMMAND_METHOD( luacmd );  // translate a debugger extension command into a Lua function call
    EXT_COMMAND_METHOD( luahelp ); // show help for registered commands


public:
    typedef std::weak_ptr< cppobj_impl > registered_object_t;
    typedef void * cmd_token_t;

    // create and register a new command description object, returning a token to it
    cmd_token_t register_command_description( std::string const & extname, std::string const & cmdname, std::string const & description, std::string const & arguments );

    // retrieve the command description associated with this token
    // if it doesn't exist, the shared_ptr will be empty
    std::shared_ptr< ExtCommandDesc > get_command_description( cmd_token_t token );

private:
    // simply checks for initialization failure, and reports it
    bool initialization_failure();

private:
    bridge::lua_inited m_inited;

    std::list< std::string const > m_string_library;

public:
    std::map< cmd_token_t, std::shared_ptr< ExtCommandDesc > > m_command_descriptions;
};

