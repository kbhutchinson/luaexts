#include "stdafx.h"

#include "dbgeng-extremotedata.h"
#include "dbgeng-common.h"
#include "luaexts.h"
#include "util.h"
#include <lua.hpp>

namespace
{
    char const * const extremotedata_typename = "dbgeng.extremotedata";
    char const * const extremotedata_cmdscope = "dbgeng.extremotedata.cmdscope";
}

// Lua projections for the ExtRemoteData class
namespace dbgeng { namespace extremotedata
{
    // used to keep track of ExtRemoteData lifetimes, since they should destruct when the extension
    // command that created them ends, according to https://msdn.microsoft.com/en-us/library/ff558927
    typedef std::stack< std::list< std::shared_ptr< ExtRemoteData > > > cmdscope_t;

    // [-0,+0]
    // If the value at the given index is an extremotedata userdata and is still valid, returns the associated
    // wrapped pointer. Else reports error.
    extremotedata_userdatum xlua_check_extremotedata( lua_State * L, int arg )
    {
        auto perd = reinterpret_cast< extremotedata_userdatum * >( luaL_checkudata( L, arg, extremotedata_typename ) );

        if ( perd->expired() )
            luaL_error( L, "%s has expired", extremotedata_typename );

        return * perd;
    }

    // [-0,+0]
    // If the value at the given index is an extremotedata userdata and is still valid, returns the associated
    // wrapped pointer. Else returns an expired pointer.
    extremotedata_userdatum xlua_test_extremotedata( lua_State * L, int arg )
    {
        auto perd = reinterpret_cast< extremotedata_userdatum * >( luaL_testudata( L, arg, extremotedata_typename ) );

        if ( nullptr != perd )
            return * perd;

        return {};
    }

    // [-0,+0]
    // Check that a given index holds a valid extremotedata_userdatum and, if so, return the underlying object it points to.
    auto xlua_check_and_deref_cppobj( lua_State * L, int index ) -> decltype( std::declval< extremotedata_userdatum >().lock() )
    {
        auto p = xlua_check_extremotedata( L, index );
        return p.lock();
    }

    // [-0,+1]
    // pushes:
    //   new userdata representing an ExtRemoteData, with associated metatable
    void xlua_push_extremotedata( lua_State * L, std::shared_ptr< ExtRemoteData > sperd )
    {
        // reconstitute the scope structure from the registry
        if ( LUA_TLIGHTUSERDATA != lua_getfield( L, LUA_REGISTRYINDEX, extremotedata_cmdscope ) )
            luaL_error( L, "%s cmdscope structure not found", extremotedata_typename );

        auto cmdscope = reinterpret_cast< cmdscope_t * >( lua_touserdata( L, -1 ) );
        lua_pop( L, 1 );

        // put a copy of 'sperd' into it in the topmost scope, so we can manage its lifetime
        cmdscope->top().push_back( sperd );

        // create a userdata and instantiate a weak_ptr copy of 'sperd' into it
        auto const addr = lua_newuserdata( L, sizeof( extremotedata_userdatum ) );
        auto const erd_datum = new( addr ) extremotedata_userdatum( sperd );

        // associate the class metatable with the userdata
        if ( luaL_getmetatable( L, extremotedata_typename ) )
            lua_setmetatable( L, -2 );
        else
            luaL_error( L, "no metatable registered for %s", extremotedata_typename );
    }

    // event_command_begin_handler( event_info )
    //
    // Event handler for the "luaexts.event.command_begin" event, which signals a new Lua extension command beginning.
    // When this happens, a new scope needs to be set so that any ExtRemoteData objects created during the command can
    // be cleaned up as soon as the command ends.
    // params:
    //   event_info: table of info to be used by event handlers
    // returns:
    //   nothing
    int l_event_command_begin_handler( lua_State * L )
    {
        // reconstitute the scope structure from the registry
        if ( LUA_TLIGHTUSERDATA != lua_getfield( L, LUA_REGISTRYINDEX, extremotedata_cmdscope ) )
            luaL_error( L, "%s cmdscope structure not found", extremotedata_typename );

        auto cmdscope = reinterpret_cast< cmdscope_t * >( lua_touserdata( L, -1 ) );
        lua_pop( L, 1 );

        // create a new scope
        cmdscope->emplace();

        return 0;
    }

    // event_command_end_handler( event_info )
    //
    // Event handler for the "luaexts.event.command_end" event, which signals a Lua extension command ending.
    // When this happens, the current command scope needs to be destroyed, thus destroying all ExtRemoteData objects
    // created while the command was running.
    // params:
    //   event_info: table of info to be used by event handlers
    // returns:
    //   nothing
    int l_event_command_end_handler( lua_State * L )
    {
        // reconstitute the scope structure from the registry
        if ( LUA_TLIGHTUSERDATA != lua_getfield( L, LUA_REGISTRYINDEX, extremotedata_cmdscope ) )
            luaL_error( L, "%s cmdscope structure not found", extremotedata_typename );

        auto cmdscope = reinterpret_cast< cmdscope_t * >( lua_touserdata( L, -1 ) );
        lua_pop( L, 1 );

        // destroy the top scope
        cmdscope->pop();

        return 0;
    }

    // get_boolean( erd )
    //
    // Returns a BOOLEAN version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   BOOLEAN version of the extremotedata object, as a number
    int l_get_boolean( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_boolean = perd->GetBoolean();
        lua_pushinteger( L, as_boolean );
        return 1;
    }

    // get_data( erd, size )
    //
    // Returns contents of the target's memory, as represented by the extremotedata object.
    // The size of the requested data must match the size specified with 'new()' or 'set()' methods.
    // params:
    //   erd: extremotedata to retrieve the value from
    //   size: size of requested data in bytes
    // returns:
    //   Contents of the target's memory, as a number
    int l_get_data( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto size = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        auto as_data = perd->GetData( size );
        lua_pushinteger( L, as_data );
        return 1;
    }

    // get_double( erd )
    //
    // Returns a double version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   double version of the extremotedata object, as a number
    int l_get_double( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_double = perd->GetDouble();
        lua_pushnumber( L, as_double );
        return 1;
    }

    // get_float( erd )
    //
    // Returns a float version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   float version of the extremotedata object, as a number
    int l_get_float( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_float = perd->GetFloat();
        lua_pushnumber( L, as_float );
        return 1;
    }

    // get_long( erd )
    //
    // Returns a LONG version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   LONG version of the extremotedata object, as a number
    int l_get_long( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_long = perd->GetLong();
        lua_pushinteger( L, as_long );
        return 1;
    }

    // get_long_ptr( erd )
    //
    // Returns a signed integer version (extended to LONG64) of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   Signed integer version of the extremotedata object, as a number
    int l_get_long_ptr( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_long_ptr = perd->GetLongPtr();
        lua_pushinteger( L, as_long_ptr );
        return 1;
    }

    // get_long64( erd )
    //
    // Returns a LONG64 version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   LONG64 version of the extremotedata object, as a number
    int l_get_long64( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_long64 = perd->GetLong64();
        lua_pushinteger( L, as_long64 );
        return 1;
    }

    // get_ptr( erd )
    //
    // Returns a ULONG64 pointer version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   Pointer version of the extremotedata object, as a number
    int l_get_ptr( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_ptr = perd->GetPtr();
        lua_pushinteger( L, as_ptr );
        return 1;
    }

    // get_short( erd )
    //
    // Returns a SHORT version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   SHORT version of the extremotedata object, as a number
    int l_get_short( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_short = perd->GetShort();
        lua_pushinteger( L, as_short );
        return 1;
    }

    // get_string( erd, num_chars )
    //
    // Reads a null-terminated string from the target's memory and returns it.
    // params:
    //   erd: extremotedata to retrieve the value from
    //   num_chars: maximum number of characters to read
    // returns:
    //   String located at the memory region represented by the extremotedata object
    int l_get_string( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto size = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );

        util::charbuf buffer( size + 1 ); // for null-terminator
        perd->GetString( buffer, size, size, false );
        lua_pushstring( L, buffer );
        return 1;
    }

    // get_std_bool( erd )
    //
    // Returns a bool version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   bool version of the extremotedata object, as a Lua boolean
    int l_get_std_bool( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_boolean = perd->GetStdBool();
        lua_pushboolean( L, as_boolean );
        return 1;
    }

    // get_char( erd )
    //
    // Returns a CHAR version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   CHAR version of the extremotedata object
    int l_get_char( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_char = perd->GetChar();
        lua_pushinteger( L, as_char );
        return 1;
    }

    // get_uchar( erd )
    //
    // Returns a UCHAR version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   UCHAR version of the extremotedata object, as a number
    int l_get_uchar( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_uchar = perd->GetUchar();
        lua_pushinteger( L, as_uchar );
        return 1;
    }

    // get_ulong( erd )
    //
    // Returns a ULONG version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   ULONG version of the extremotedata object, as a number
    int l_get_ulong( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_ulong = perd->GetUlong();
        lua_pushinteger( L, as_ulong );
        return 1;
    }

    // get_ulong_ptr( erd )
    //
    // Returns an unsigned integer version (extended to ULONG64) of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   Unsigned integer version of the extremotedata object, as a number
    int l_get_ulong_ptr( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_ulong_ptr = perd->GetUlongPtr();
        lua_pushinteger( L, as_ulong_ptr );
        return 1;
    }

    // get_ulong64( erd )
    //
    // Returns a ULONG64 version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   ULONG64 version of the extremotedata object, as a number
    int l_get_ulong64( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_ulong64 = perd->GetUlong64();
        lua_pushinteger( L, as_ulong64 );
        return 1;
    }

    // get_ushort( erd )
    //
    // Returns a USHORT version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   USHORT version of the extremotedata object, as a number
    int l_get_ushort( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_ushort = perd->GetUshort();
        lua_pushinteger( L, as_ushort );
        return 1;
    }

    // get_w32_bool( erd )
    //
    // Returns a BOOL version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   BOOL version of the extremotedata object, as a number
    int l_get_w32_bool( lua_State * L )
    {
        auto perd = xlua_check_and_deref_cppobj( L, 1 );
        auto as_w32_bool = perd->GetW32Bool();
        lua_pushinteger( L, as_w32_bool );
        return 1;
    }

    // new()
    // new( offset, size )
    // new( name, offset, size )
    //
    // Creates a new userdatum representing an ExtRemoteData object.
    // params:
    //   offset: memory offset of the target region
    //   size: size in bytes of the target region
    //   name: name of the target region. can be used to help identify the region and will inserted
    //     into error messages generated by the object
    // returns:
    //   new ExtRemoteData userdatum
    int l_new( lua_State * L )
    {
        int const num_args = lua_gettop( L );
        std::shared_ptr< ExtRemoteData > erd;
        if ( 0 == num_args )
        {
            erd = std::make_shared< ExtRemoteData >();
        }
        else if ( 2 == num_args )
        {
            auto offset = static_cast< ULONG64 >( luaL_checkinteger( L, 1 ) );
            auto size   = static_cast< ULONG   >( luaL_checkinteger( L, 2 ) );
            erd = std::make_shared< ExtRemoteData >( offset, size );
        }
        else if ( 3 == num_args )
        {
            auto name   = luaL_checkstring( L, 1 );
            auto offset = static_cast< ULONG64 >( luaL_checkinteger( L, 2 ) );
            auto size   = static_cast< ULONG   >( luaL_checkinteger( L, 3 ) );
            erd = std::make_shared< ExtRemoteData >( name, offset, size );
        }
        else
        {
            return luaL_error( L, "%s.new: wrong number of arguments", extremotedata_typename );
        }

        xlua_push_extremotedata( L, erd );
        return 1;
    }

    // __gc( erd )
    //
    // __gc metamethod: cleans up after an extremotedata userdatum
    // params:
    //   erd: extremotedata userdatum being cleaned up
    // returns:
    //   nothing
    int l_gc( lua_State * L )
    {
        auto perd = reinterpret_cast< extremotedata_userdatum * >( luaL_testudata( L, 1, extremotedata_typename ) );
        if ( perd )
            perd->~extremotedata_userdatum();

        return 0;
    }

    struct luaL_Reg const library_funcs[] =
    {
        { "get_boolean", l_get_boolean },
        { "get_char", l_get_char },
        { "get_data", l_get_data },
        { "get_double", l_get_double },
        { "get_float", l_get_float },
        { "get_long", l_get_long },
        { "get_long_ptr", l_get_long_ptr },
        { "get_long64", l_get_long64 },
        { "get_ptr", l_get_ptr },
        { "get_short", l_get_short },
        { "get_string", l_get_string },
        { "get_std_bool", l_get_std_bool },
        { "get_uchar", l_get_uchar },
        { "get_ulong", l_get_ulong },
        { "get_ulong_ptr", l_get_ulong_ptr },
        { "get_ulong64", l_get_ulong64 },
        { "get_ushort", l_get_ushort },
        { "get_w32_bool", l_get_w32_bool },
        { "new", l_new },
        { nullptr , nullptr },
    };

    struct luaL_Reg const metamethods[] =
    {
        { "__gc"       , l_gc     }, 
        { nullptr      , nullptr  }, 
    };

    // [-0,+0]
    // dbgeng::extremotedata api init
    void xlua_init( lua_State * L )
    {
        xlua_get_dbgeng_subtable( L, "extremotedata" );
        lua_pushvalue( L, -1 );
        luaL_setfuncs( L, library_funcs, 0 );

        // metatable used for wrapped objects
        if ( luaL_newmetatable( L, extremotedata_typename ) )
        {
            lua_insert( L, -2 );
            lua_setfield( L, -2, "__index" );
            lua_pushcfunction( L, l_gc );
            lua_setfield( L, -2, "__gc" );
            lua_pop( L, 1 );
        }

        auto cmdscope = std::make_unique< cmdscope_t >();
        lua_pushlightuserdata( L, cmdscope.release() );
        lua_setfield( L, LUA_REGISTRYINDEX, extremotedata_cmdscope );

        {
            // setup event handlers to create and destroy scopes
            lua_getglobal( L, EXTNAME );
            if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
                luaL_error( L, "'" EXTNAME "' table not found" );

            lua_getfield( L, -1, "event" );
            if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
                luaL_error( L, "'" EXTNAME ".event' table not found" );

            lua_getfield( L, -1, "register_event_handler" );
            if ( lua_isnil( L, -1 ) || ! lua_isfunction( L, -1 ) )
                luaL_error( L, "'" EXTNAME ".event.register_event_handler' function not found" );

            // remove parent tables
            lua_remove( L, -2 );
            lua_remove( L, -2 );

            // copy the function, we're going to call it twice
            lua_pushvalue( L, -1 );

            // command_begin handler
            lua_pushstring( L, luaexts::event_command_begin );
            lua_pushcfunction( L, l_event_command_begin_handler );
            lua_call( L, 2, 0 );

            // command_end handler
            lua_pushstring( L, luaexts::event_command_end );
            lua_pushcfunction( L, l_event_command_end_handler );
            lua_call( L, 2, 0 );
        }

        // pop dbgeng.extremotedata table
        lua_pop( L, 1 );
    }

}}
