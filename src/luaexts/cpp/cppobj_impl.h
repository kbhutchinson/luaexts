#pragma once

#include "comwrappers.h"

#include <string>
#include <memory>

namespace cppobj
{
    enum class kind_t;
}

////////////////////////////////////////////////////////////////////////////////
// cppobj_impl declaration
// This class is used to hide the Lua cppobj operations from the implementation
// details of how the debugging target's C++ objects are actually referenced and used.
//
class cppobj_impl
{
public:
    explicit cppobj_impl( std::string const & expression );

    // is this a valid object
    bool valid() const;

    // short name of this object, usually just the field name
    std::string const & short_name() const;
    // long name of this object, which should be a fully qualified expression valid for the current scope
    std::string const & long_name() const;
    // long name surrounded by parentheses if needed
    std::string safe_long_name() const;

    // type of the object
    std::string const & type() const;

    // size of this object
    ULONG size() const;

    // offset of this object in the target's memory
    ULONG64 offset() const;

    // type id of the object
    ULONG type_id() const;

    // kind of this object
    cppobj::kind_t kind() const;

    // does this object have a data member of a given name
    bool has_member( std::string const & name );

    // return an object representing a named data member from this object
    cppobj_impl member( std::string const & name );

    // return an object representing a data member from this object based on its index within this object
    cppobj_impl member( size_t index );

    // return all data members, in index order
    std::vector< cppobj_impl > const & members();

    // return just the object's direct data members, excluding inherited members
    std::vector< cppobj_impl > const & direct_members();

    // if this object is an array or pointer, return an object with the given element offset
    cppobj_impl array_element( ptrdiff_t index );

    // return an object representing a pointer to this object
    cppobj_impl reference() const;

    // if this object is a pointer, return the object it points to
    // if this object is an array, return the first element
    cppobj_impl dereference() const;

    // return list of base class representations of this object
    std::vector< cppobj_impl > const & base_classes();

    // return list of vtables for this object
    std::vector< cppobj_impl > const & vtables();

    // data retrieval
    bool    as_bool    () const;
    BOOLEAN as_boolean () const;
    BOOL    as_w32bool () const;
    CHAR    as_char    () const;
    UCHAR   as_uchar   () const;
    SHORT   as_short   () const;
    USHORT  as_ushort  () const;
    LONG    as_long    () const;
    ULONG   as_ulong   () const;
    LONG64  as_int64   () const;
    ULONG64 as_uint64  () const;
    float   as_float   () const;
    double  as_double  () const;
    ULONG64 as_ptr     () const;
    ULONG64 raw        () const;

    // dbgeng string interpretation of the object's value
    std::string as_string() const;

private:
    cppobj_impl( std::string const & expression, std::string const & short_name );
    cppobj_impl( std::shared_ptr< SymbolGroup > group, ULONG64 offset, ULONG type_id, cppobj_impl const & parent );
    cppobj_impl();
    void set_name_and_type( ULONG const index, cppobj_impl const * const parent = nullptr );
    void expand();
    std::string child_display_name( ULONG const index );
    std::string child_expression( ULONG const index );

private:
    bool m_valid; // is this object valid
    std::shared_ptr< SymbolGroup > const m_group; // symbol group holding this symbol
    ULONG64 const m_offset; // offset in memory; along with typeid, this is used to identify the object in the group
    ULONG   const m_type_id; // id of the type; along with offset, this is used to identify the object in the group

    std::string m_short_name;
    std::string m_long_name;
    std::string m_type;
    ULONG       m_size;
    enum class cppobj::kind_t m_kind;
    
    // does the long name need parens to be safely embedded in an expression
    bool m_needs_parens;

    bool m_expanded; // has this symbol been expanded in the symbol group
    std::vector< cppobj_impl > m_direct_data_members;   // data members not inherited
    std::vector< cppobj_impl > m_data_members;          // data members including inherited
    std::vector< cppobj_impl > m_base_classes;          // base class representations of this object
    std::vector< cppobj_impl > m_vtables;               // vtables
};
