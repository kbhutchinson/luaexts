This directory is searched for x86 link-time dependencies. The libraries
themselves are not version controlled, but should be placed here after retrieval
from their external sources.
