dbgeng
======

The `dbgeng COM API`_ is divided into several categories of COM objects, each relating to a specific
set of functionality. The provided Lua functions are intended to be a thin facade over the actual
COM functions, and so are patterned very closely after them.

.. toctree::
    :maxdepth: 1

    dbgeng/global-data
    dbgeng/control
    dbgeng/symbols
    dbgeng/symbolgroup
    dbgeng/breakpoint
