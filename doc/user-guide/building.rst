Building
========

The dbgeng headers and libs, required to build luaexts, are not included in this repository because I'm unsure of their redistribution license. So to build luaexts, you'll need to do the following:

* Clone the `luaexts repository`_.
* Retrieve the dbgeng headers and libs.

    * Once you have `Windbg installed`_, the headers can be found in ``<install-dir>\Debuggers\inc`` and the libs in ``<install-dir>\Debuggers\lib``. Copy the headers to ``luaexts\inc`` and the libs to the appropriate platform directory under ``luaexts\lib``.

* Build ``luaexts.sln`` with Visual Studio.
* Binaries will be placed in directories under ``luaexts\bin``.

Remember that after building locally, in addition to **luaexts.dll**, you also need the contents of |luaexts lua source directory|_ copied to your Windbg extension path, next to the DLL or in a ``luaexts`` directory next to the DLL.

.. _luaexts repository: https://bitbucket.org/kbhutchinson/luaexts
.. _Windbg installed: https://msdn.microsoft.com/en-us/library/ff551063
.. |luaexts lua source directory| replace:: **luaexts\\src\\luaexts\\lua**
.. _luaexts lua source directory: https://bitbucket.org/kbhutchinson/luaexts/src/tip/src/luaexts/lua

