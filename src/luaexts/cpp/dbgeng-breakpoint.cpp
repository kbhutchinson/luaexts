#include "stdafx.h"

#include "dbgeng-breakpoint.h"
#include "dbgeng-common.h"
#include "util.h"
#include <lua.hpp>

namespace
{
    char const * const breakpoint_typename = "dbgeng.breakpoint";
}

// Lua projections for the IDebugBreakpoint interface
namespace dbgeng { namespace breakpoint
{
    // [-0,+0]
    // if the value at the given index is a breakpoint userdata, returns the associated
    // IDebugBreakpoint address. else reports error.
    IDebugBreakpoint * xlua_check_breakpoint( lua_State * L, int arg )
    {
        return * reinterpret_cast< IDebugBreakpoint ** >( luaL_checkudata( L, arg, breakpoint_typename ) );
    }

    // [-0,+1]
    // pushes:
    //   new userdata representing a breakpoint, with associated metatable
    void xlua_push_breakpoint( lua_State * L, IDebugBreakpoint * bp )
    {
        auto paddr = reinterpret_cast< IDebugBreakpoint ** >( lua_newuserdata( L, sizeof( IDebugBreakpoint * ) ) );
        if ( ! paddr )
            luaL_error( L, "%s allocation failed", breakpoint_typename );

        *paddr = bp;

        if ( luaL_getmetatable( L, breakpoint_typename ) )
            lua_setmetatable( L, -2 );
        else
            luaL_error( L, "no metatable registered for %s", breakpoint_typename );
    }

    // add_flags( bp, flags )
    // adds flags to a breakpoint by combining the given bitfield with existing flags using
    // bitwise OR. possible values can be found in the table 'dbgeng.breakpoint.flag'.
    // params:
    //   bp: breakpoint to add flags to
    //   flags: bitfield value that will be added to existing flags
    // returns:
    //   nothing
    int l_add_flags( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto flags = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );

        HRESULT hr = bp->AddFlags( flags );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // get_command( bp )
    // returns the command string that is executed when a breakpoint is triggered
    // params:
    //   bp: breakpoint to retrieve command string for
    // returns:
    //   command string
    int l_get_command( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG bufsize = 0;

        HRESULT hr = bp->GetCommand( nullptr, 0, &bufsize );
        if ( SUCCEEDED( hr ) )
        {
            util::charbuf buffer( bufsize + 1 ); // for null-terminator
            hr = bp->GetCommand( buffer, bufsize, nullptr );
            if ( SUCCEEDED( hr ) )
            {
                lua_pushstring( L, buffer );
                return 1;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_current_pass_count( bp )
    // returns the remaining number of times that the target must reach the breakpoint
    // location before the breakpoint is triggered.
    // params:
    //   bp: breakpoint to retrieve pass count for
    // returns:
    //   remaining pass count
    int l_get_current_pass_count( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG passes = 0;

        HRESULT hr = bp->GetCurrentPassCount( &passes );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, passes );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_data_parameters( bp )
    // returns breakpoint parameters for a processor breakpoint. access type is a bitfield
    // combining the following possible values:
    //   - dbgeng.breakpoint.access.READ
    //   - dbgeng.breakpoint.access.WRITE
    //   - dbgeng.breakpoint.access.EXECUTE
    //   - dbgeng.breakpoint.access.IO
    // params:
    //   bp: breakpoint to retrieve the data parameters for
    // returns:
    //   size of memory block whose access triggers the breakpoint
    //   bitfield of access types
    int l_get_data_parameters( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG size = 0;
        ULONG access = 0;

        HRESULT hr = bp->GetDataParameters( &size, &access );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, size );
            lua_pushinteger( L, access );
            return 2;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_flags( bp )
    // returns the flags for a breakpoint, as a bitfield
    // params:
    //   bp: breakpoint to retrieve the flags for
    // returns:
    //   bitfield of breakpoint flags
    int l_get_flags( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG flags = 0;

        HRESULT hr = bp->GetFlags( &flags );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, flags );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_id( bp )
    // returns the breakpoint's id
    // params:
    //   bp: breakpoint to retrieve the id for
    // returns:
    //   breakpoint id
    int l_get_id( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG id = 0;

        HRESULT hr = bp->GetId( &id );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, id );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_match_thread_id( bp )
    // returns the engine thread id that can trigger the breakpoint
    // params:
    //   bp: breakpoint to retrieve matching thread id for
    // returns:
    //   matching thread id, if one has been set; otherwise nil
    int l_get_match_thread_id( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG id = 0;

        HRESULT hr = bp->GetMatchThreadId( &id );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, id );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_offset( bp )
    // returns the location that triggers the breakpoint
    // params:
    //   bp: breakpoint to retrieve command string for
    // returns:
    //   location
    int l_get_offset( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG64 offset = 0;

        HRESULT hr = bp->GetOffset( &offset );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, offset );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_offset_expression( bp )
    // returns the expression string that evaluates to the location that triggers the breakpoint
    // params:
    //   bp: breakpoint to retrieve offset expression for
    // returns:
    //   offset expression
    int l_get_offset_expression( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG bufsize = 0;

        HRESULT hr = bp->GetOffsetExpression( nullptr, 0, &bufsize );
        if ( SUCCEEDED( hr ) )
        {
            util::charbuf buffer( bufsize + 1 ); // for null-terminator
            hr = bp->GetOffsetExpression( buffer, bufsize, nullptr );
            if ( SUCCEEDED( hr ) )
            {
                lua_pushstring( L, buffer );
                return 1;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_parameters( bp )
    // returns most parameter information for a breakpoint in one call, rather than using separate
    // calls for each piece.
    // params:
    //   bp: breakpoint to retrieve parameters for
    // returns:
    //   breakpoint parameters, as a table with named fields
    int l_get_parameters( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        DEBUG_BREAKPOINT_PARAMETERS bpp = {};

        HRESULT hr = bp->GetParameters( &bpp );
        if ( SUCCEEDED( hr ) )
        {
            lua_createtable( L, 0, 10 );

            lua_pushinteger( L, bpp.Offset );
            lua_setfield( L, -2, "offset" );

            lua_pushinteger( L, bpp.Id );
            lua_setfield( L, -2, "id" );

            lua_pushinteger( L, bpp.BreakType );
            lua_setfield( L, -2, "break_type" );

            lua_pushinteger( L, bpp.ProcType );
            lua_setfield( L, -2, "proc_type" );

            lua_pushinteger( L, bpp.Flags );
            lua_setfield( L, -2, "flags" );

            lua_pushinteger( L, bpp.DataSize );
            lua_setfield( L, -2, "data_size" );

            lua_pushinteger( L, bpp.DataAccessType );
            lua_setfield( L, -2, "data_access_type" );

            lua_pushinteger( L, bpp.PassCount );
            lua_setfield( L, -2, "pass_count" );

            lua_pushinteger( L, bpp.CurrentPassCount );
            lua_setfield( L, -2, "current_pass_count" );

            lua_pushinteger( L, bpp.MatchThread );
            lua_setfield( L, -2, "match_thread" );

            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_pass_count( bp )
    // returns the number of times that the target was originally required to reach the breakpoint
    // location before the breakpoint is triggered.
    // params:
    //   bp: breakpoint to retrieve pass count for
    // returns:
    //   original pass count
    int l_get_pass_count( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG passes = 0;

        HRESULT hr = bp->GetPassCount( &passes );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, passes );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_type( bp )
    // returns the type of breakpoint and the type of the processor the breakpoint is set for.
    // the returned breakpoint type can be one of:
    //   - dbgeng.breakpoint.type.CODE: software breakpoint
    //   - dbgeng.breakpoint.type.DATA: processor breakpoint
    // params:
    //   bp: breakpoint to retrieve type for
    // returns:
    //   type of the breakpoint
    //   type of the processor
    int l_get_type( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        ULONG bptype = 0;
        ULONG proctype = 0;

        HRESULT hr = bp->GetType( &bptype, &proctype );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, bptype );
            lua_pushinteger( L, proctype );
            return 2;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // remove_flags( bp, flags )
    // removes flags from a breakpoint by combining the NOT of the given bitfield with
    // existing flags using bitwise AND. possible values can be found in the table
    // 'dbgeng.breakpoint.flag'.
    // params:
    //   bp: breakpoint to add flags to
    //   flags: bitfield value that will be removed from existing flags
    // returns:
    //   nothing
    int l_remove_flags( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto flags = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );

        HRESULT hr = bp->RemoveFlags( flags );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // set_command( bp, cmdstr )
    // sets the command string that is executed when a breakpoint is triggered
    // params:
    //   bp: breakpoint to set command string for
    //   cmdstr: command string to execute
    // returns:
    //   nothing
    int l_set_command( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto cmd = luaL_checkstring( L, 2 );

        HRESULT hr = bp->SetCommand( cmd );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // set_data_parameters( bp, size, access_type )
    // sets breakpoint parameters for a processor breakpoint. access type can be one of:
    //   - dbgeng.breakpoint.access.READ
    //   - dbgeng.breakpoint.access.WRITE
    //   - dbgeng.breakpoint.access.EXECUTE
    //   - dbgeng.breakpoint.access.IO
    // params:
    //   bp: breakpoint to retrieve the data parameters for
    //   size: size of the memory location whose access will trigger the breakpoint
    //   access_type: value from dbgeng.breakpoint.access
    // returns:
    //   nothing
    int l_set_data_parameters( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto size = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        auto access = static_cast< ULONG >( luaL_checkinteger( L, 3 ) );

        HRESULT hr = bp->SetDataParameters( size, access );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // set_flags( bp, flags )
    // sets the flags for a breakpoint, as a bitfield. possible values can be found in the
    // table 'dbgeng.breakpoint.flag'.
    // params:
    //   bp: breakpoint to set flags for
    //   flags: bitfield of breakpoint flags
    // returns:
    //   nothing
    int l_set_flags( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto flags = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );

        HRESULT hr = bp->SetFlags( flags );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // set_match_thread_id( bp, id )
    // sets the engine thread id that can trigger the breakpoint. if a thread has been set,
    // the setting can be removed by passing 'dbgeng.ANY_ID' for the id.
    // params:
    //   bp: breakpoint to set the matching thread id for
    //   id: id of the thread that can trigger the breakpoint
    // returns:
    //   nothing
    int l_set_match_thread_id( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto id = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );

        HRESULT hr = bp->SetMatchThreadId( id );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // set_offset( bp, offset )
    // sets the command string that is executed when a breakpoint is triggered
    // params:
    //   bp: breakpoint to set command string for
    //   offset: memory address that triggers the breakpoint
    // returns:
    //   nothing
    int l_set_offset( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto offset = static_cast< ULONG64 >( luaL_checkinteger( L, 2 ) );

        HRESULT hr = bp->SetOffset( offset );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // set_offset_expression( bp )
    // sets the expression string that evaluates to the location that triggers the breakpoint
    // params:
    //   bp: breakpoint to retrieve offset expression for
    //   expr: expression string that evaluates to the location that triggers the breakpoint
    // returns:
    //   nothing
    int l_set_offset_expression( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto expr = luaL_checkstring( L, 2 );

        HRESULT hr = bp->SetOffsetExpression( expr );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    // set_pass_count( bp, count )
    // sets the number of times that the target must reach the breakpoint location before the
    // breakpoint is triggered
    // params:
    //   bp: breakpoint to set the pass count for
    //   passes: number of times target must hit the breakpoint before it is triggered
    // returns:
    //   nothing
    int l_set_pass_count( lua_State * L )
    {
        auto bp = xlua_check_breakpoint( L, 1 );
        auto count = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );

        HRESULT hr = bp->SetPassCount( count );
        if ( SUCCEEDED( hr ) )
            return 0;

        return xlua_push_hresult_error( L, hr );
    }

    struct enum_mapping const breakpoint_types[] =
    {
        { "CODE", DEBUG_BREAKPOINT_CODE },
        { "DATA", DEBUG_BREAKPOINT_DATA },
        { nullptr , 0 }
    };

    struct enum_mapping const breakpoint_flags[] =
    {
        { "ENABLED"    , DEBUG_BREAKPOINT_ENABLED    },
        { "ADDER_ONLY" , DEBUG_BREAKPOINT_ADDER_ONLY },
        { "GO_ONLY"    , DEBUG_BREAKPOINT_GO_ONLY    },
        { "ONE_SHOT"   , DEBUG_BREAKPOINT_ONE_SHOT   },
        { "DEFERRED"   , DEBUG_BREAKPOINT_DEFERRED   },
        { nullptr , 0 }
    };

    struct enum_mapping const breakpoint_access_types[] =
    {
        { "READ"    , DEBUG_BREAK_READ    },
        { "WRITE"   , DEBUG_BREAK_WRITE   },
        { "EXECUTE" , DEBUG_BREAK_EXECUTE },
        { "IO"      , DEBUG_BREAK_IO      },
        { nullptr , 0 }
    };

    struct luaL_Reg const library_funcs[] =
    {
        { "add_flags"              , l_add_flags              },
        { "get_command"            , l_get_command            },
        { "get_current_pass_count" , l_get_current_pass_count },
        { "get_data_parameters"    , l_get_data_parameters    },
        { "get_flags"              , l_get_flags              },
        { "get_id"                 , l_get_id                 },
        { "get_match_thread_id"    , l_get_match_thread_id    },
        { "get_offset"             , l_get_offset             },
        { "get_offset_expression"  , l_get_offset_expression  },
        { "get_pass_count"         , l_get_pass_count         },
        { "get_parameters"         , l_get_parameters         },
        { "get_type"               , l_get_type               },
        { "remove_flags"           , l_remove_flags           },
        { "set_command"            , l_set_command            },
        { "set_data_parameters"    , l_set_data_parameters    },
        { "set_flags"              , l_set_flags              },
        { "set_match_thread_id"    , l_set_match_thread_id    },
        { "set_offset"             , l_set_offset             },
        { "set_offset_expression"  , l_set_offset_expression  },
        { "set_pass_count"         , l_set_pass_count         },
        { nullptr , nullptr },
    };

    // [-0,+0]
    // dbgeng::breakpoint api init
    void xlua_init( lua_State * L )
    {
        xlua_get_dbgeng_subtable( L, "breakpoint" );
        lua_pushvalue( L, -1 );
        luaL_setfuncs( L, library_funcs, 0 );

        // metatable used for wrapped objects
        if ( luaL_newmetatable( L, breakpoint_typename ) )
        {
            lua_insert( L, -2 );
            lua_setfield( L, -2, "__index" );
            lua_pop( L, 1 );
        }

        // setup the table of breakpoint types
        luaL_getsubtable( L, -1, "type" );
        xlua_set_enums( L, breakpoint_types );
        lua_pop( L, 1 );

        // setup the table of breakpoint flags
        luaL_getsubtable( L, -1, "flag" );
        xlua_set_enums( L, breakpoint_flags );
        lua_pop( L, 1 );

        // setup the table of breakpoint access types
        luaL_getsubtable( L, -1, "access" );
        xlua_set_enums( L, breakpoint_access_types );
        lua_pop( L, 1 );

        // pop dbgeng.breakpoint table
        lua_pop( L, 1 );
    }

}} // namespace dbgeng::breakpoint
