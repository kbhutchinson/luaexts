#pragma once

struct lua_State;

// Lua projections for the IDebugBreakpoint interface
namespace dbgeng { namespace breakpoint
{
    // [-0,+0]
    // if the value at the given index is a breakpoint userdata, returns the associated
    // IDebugBreakpoint address. else reports error.
    IDebugBreakpoint * xlua_check_breakpoint( lua_State * L, int arg );

    // [-0,+1]
    // pushes:
    //   new userdata representing a breakpoint, with associated metatable
    void xlua_push_breakpoint( lua_State * L, IDebugBreakpoint * bp );

    // add_flags( bp, flags )
    // adds flags to a breakpoint by combining the given bitfield with existing flags using
    // bitwise OR. possible values can be found in the table 'dbgeng.breakpoint.flag'.
    // params:
    //   bp: breakpoint to add flags to
    //   flags: bitfield value that will be added to existing flags
    // returns:
    //   nothing
    int l_add_flags( lua_State * L );

    // get_command( bp )
    // returns the command string that is executed when a breakpoint is triggered
    // params:
    //   bp: breakpoint to retrieve command string for
    // returns:
    //   command string
    int l_get_command( lua_State * L );

    // get_current_pass_count( bp )
    // returns the remaining number of times that the target must reach the breakpoint
    // location before the breakpoint is triggered.
    // params:
    //   bp: breakpoint to retrieve pass count for
    // returns:
    //   remaining pass count
    int l_get_current_pass_count( lua_State * L );

    // get_data_parameters( bp )
    // returns breakpoint parameters for a processor breakpoint. access type can be one of:
    //   - dbgeng.breakpoint.access.READ
    //   - dbgeng.breakpoint.access.WRITE
    //   - dbgeng.breakpoint.access.EXECUTE
    //   - dbgeng.breakpoint.access.IO
    // params:
    //   bp: breakpoint to retrieve the data parameters for
    // returns:
    //   size of memory block whose access triggers the breakpoint
    //   access type
    int l_get_data_parameters( lua_State * L );

    // get_flags( bp )
    // returns the flags for a breakpoint, as a bitfield. possible values can be
    // found in the table 'dbgeng.breakpoint.flag'.
    // params:
    //   bp: breakpoint to retrieve the flags for
    // returns:
    //   bitfield of breakpoint flags
    int l_get_flags( lua_State * L );

    // get_id( bp )
    // returns the breakpoint's id
    // params:
    //   bp: breakpoint to retrieve the id for
    // returns:
    //   breakpoint id
    int l_get_id( lua_State * L );

    // get_match_thread_id( bp )
    // returns the engine thread id that can trigger the breakpoint
    // params:
    //   bp: breakpoint to retrieve matching thread id for
    // returns:
    //   matching thread id, if one has been set; otherwise nil
    int l_get_match_thread_id( lua_State * L );

    // get_offset( bp )
    // returns the location that triggers the breakpoint
    // params:
    //   bp: breakpoint to retrieve command string for
    // returns:
    //   memory address
    int l_get_offset( lua_State * L );

    // get_offset_expression( bp )
    // returns the expression string that evaluates to the location that triggers the breakpoint
    // params:
    //   bp: breakpoint to retrieve offset expression for
    // returns:
    //   offset expression
    int l_get_offset_expression( lua_State * L );

    // get_parameters( bp )
    // returns most parameter information for a breakpoint in one call, rather than using separate
    // calls for each piece.
    // params:
    //   bp: breakpoint to retrieve parameters for
    // returns:
    //   breakpoint parameters, as a table with named fields
    int l_get_parameters( lua_State * L );

    // get_pass_count( bp )
    // returns the number of times that the target was originally required to reach the breakpoint
    // location before the breakpoint is triggered.
    // params:
    //   bp: breakpoint to retrieve pass count for
    // returns:
    //   original pass count
    int l_get_pass_count( lua_State * L );

    // get_type( bp )
    // returns the type of breakpoint and the type of the processor the breakpoint is set for.
    // the returned breakpoint type can be one of:
    //   - dbgeng.breakpoint.type.CODE: software breakpoint
    //   - dbgeng.breakpoint.type.DATA: processor breakpoint
    // params:
    //   bp: breakpoint to retrieve type for
    // returns:
    //   type of the breakpoint
    //   type of the processor
    int l_get_type( lua_State * L );

    // remove_flags( bp, flags )
    // removes flags from a breakpoint by combining the NOT of the given bitfield with
    // existing flags using bitwise AND. possible values can be found in the table
    // 'dbgeng.breakpoint.flag'.
    // params:
    //   bp: breakpoint to add flags to
    //   flags: bitfield value that will be removed from existing flags
    // returns:
    //   nothing
    int l_remove_flags( lua_State * L );

    // set_command( bp, cmdstr )
    // sets the command string that is executed when a breakpoint is triggered
    // params:
    //   bp: breakpoint to set command string for
    //   cmdstr: command string to execute
    // returns:
    //   nothing
    int l_set_command( lua_State * L );

    // set_data_parameters( bp, size, access_type )
    // sets breakpoint parameters for a processor breakpoint. access type can be one of:
    //   - dbgeng.breakpoint.access.READ
    //   - dbgeng.breakpoint.access.WRITE
    //   - dbgeng.breakpoint.access.EXECUTE
    //   - dbgeng.breakpoint.access.IO
    // params:
    //   bp: breakpoint to retrieve the data parameters for
    //   size: size of the memory location whose access will trigger the breakpoint
    //   access_type: value from dbgeng.breakpoint.access
    // returns:
    //   nothing
    int l_set_data_parameters( lua_State * L );

    // set_flags( bp, flags )
    // sets the flags for a breakpoint, as a bitfield. possible values can be found in the
    // table 'dbgeng.breakpoint.flag'.
    // params:
    //   bp: breakpoint to set flags for
    //   flags: bitfield of breakpoint flags
    // returns:
    //   nothing
    int l_set_flags( lua_State * L );

    // set_match_thread_id( bp, id )
    // sets the engine thread id that can trigger the breakpoint. if a thread has been set,
    // the setting can be removed by passing 'dbgeng.ANY_ID' for the id.
    // params:
    //   bp: breakpoint to set the matching thread id for
    //   id: id of the thread that can trigger the breakpoint
    // returns:
    //   nothing
    int l_set_match_thread_id( lua_State * L );

    // set_offset( bp, offset )
    // sets the memory location in the target that triggers the breakpoint
    // params:
    //   bp: breakpoint to set command string for
    //   offset: memory address that triggers the breakpoint
    // returns:
    //   nothing
    int l_set_offset( lua_State * L );

    // set_offset_expression( bp, expr )
    // sets the expression string that evaluates to the location that triggers the breakpoint
    // params:
    //   bp: breakpoint to retrieve offset expression for
    //   expr: expression string that evaluates to the location that triggers the breakpoint
    // returns:
    //   nothing
    int l_set_offset_expression( lua_State * L );

    // set_pass_count( bp, count )
    // sets the number of times that the target must reach the breakpoint location before the
    // breakpoint is triggered
    // params:
    //   bp: breakpoint to set the pass count for
    //   passes: number of times target must hit the breakpoint before it is triggered
    // returns:
    //   nothing
    int l_set_pass_count( lua_State * L );

    // [-0,+0]
    // dbgeng::breakpoint api init
    void xlua_init( lua_State * L );

}} // namespace dbgeng::breakpoint
