dbgeng.control
==============

.. module:: dbgeng.control
    :synopsis: functions that control the debuggee

The control functions are all accessible from the global *dbgeng.control* table, and represent the
functions from the IDebugControl_ interfaces of the `dbgeng COM API`_.

.. _IDebugControl: https://msdn.microsoft.com/en-us/library/ff550508

Functions
---------

.. function:: add_assembly_options( options ) -> boolean

    Turns on some of the assembly and disassembly options. The given options will be added to the
    existing options. See `AddAssemblyOptions <https://msdn.microsoft.com/en-us/library/ff537852>`_.

    :param integer options: bitfield, combining values from the :data:`.asmopt` table
    :returns: true if successful

.. function:: add_breakpoint( type, id ) -> dbgeng.breakpoint

    Adds a breakpoint. *type* can be one of the values from the :data:`dbgeng.breakpoint.type` table.
    See `AddBreakpoint <https://msdn.microsoft.com/en-us/library/ff537856>`_.

    :param dbgeng.breakpoint.type type: breakpoint type, from the :data:`dbgeng.breakpoint.type` table 
    :param integer id: breakpoint id to use; use :data:`dbgeng.ANY_ID` to have the engine assign the id
    :rtype: dbgeng.breakpoint

.. function:: add_engine_options( options ) -> boolean

    Turns on some of the debugger engine's options, by combining the given bitfield with the
    existing options using bitwise OR. Possible bitflag values can be found in the :data:`.engopt`
    table. See `AddEngineOptions <https://msdn.microsoft.com/en-us/library/ff537884>`_.

    :param integer options: bitfield, combining values from the :data:`.engopt` table
    :returns: true if successful

.. function:: add_extension( path ) -> integer

    Loads an extension library into the debugger engine. If the extension library is already loaded,
    simply returns the handle, the library will not be loaded again.
    See `AddExtension <https://msdn.microsoft.com/en-us/library/ff537892>`_.

    :param string path: fully qualified path to the extension library to load
    :returns: handle to the loaded library

.. function:: call_extension( handle, function, argstr ) -> boolean

    Calls an extension library function.
    See `CallExtension <https://msdn.microsoft.com/en-us/library/ff539023>`_.

    :param integer handle: handle to the extension library, returned by :func:`.add_extension`; if
        set to 0, the engine will walk the extension library chain search for the function
    :param string function: name of the extension function to call
    :param string argstr: parsed into arguments by the extension as if typed at the command prompt
    :returns: true if called successfully

.. function:: disassemble( offset, flags ) -> string, integer

    Disassembles a processor instruction in the target's memory.
    See `Disassemble <https://msdn.microsoft.com/en-us/library/ff541948>`_.

    :param integer offset: location in the target's memory of the instruction to disassemble
    :param integer flags: bitfield of flags that affect the behavior of the function; currently the
        only flag that can be set is ``dbgeng.control.disasm_flag.EFFECTIVE_ADDRESS``, which will
        instruct the engine to computer and display the effective address from the current register
        information
    :returns: two values, the first being the disassembled instruction as a string, and the second
        being the memory location of the following instruction

.. function:: execute( outctl, command, flags ) -> boolean

    Executes the specified debugger commands, as if typed at the command prompt.
    See `Execute <https://msdn.microsoft.com/en-us/library/ff543208>`_.

    :param integer outctl: bitfield, combining values from the :data:`dbgeng.outctl` table
    :param string command: command string to execute, may contain multiple commands
    :param integer flags: bitfield, combining values from the :data:`.execute_flag` table

.. function:: get_assembly_options() -> integer

    Returns the assembly and disassembly options that affect how the debugger engine assembles and
    disassembles processor instructions for the target.
    See `GetAssemblyOptions <https://msdn.microsoft.com/en-us/library/ff545605>`_.

    :returns: bitfield containing a combination of values from the :data:`.asmopt` table

.. function:: get_breakpoint_by_id( id ) -> breakpoint

    Returns the :class:`dbgeng.breakpoint` with the specified breakpoint id. See `GetBreakpointById
    <https://msdn.microsoft.com/en-us/library/ff545617>`_.

    If a breakpoint with the given id is not found, belongs to another process, or is private,
    returns ``nil``.

    :param integer id: id of the breakpoint to return
    :rtype: dbgeng.breakpoint

.. function:: get_breakpoint_by_index( index ) -> breakpoint

    Returns the breakpoint at the specified index. See `GetBreakpointByIndex
    <https://msdn.microsoft.com/en-us/library/ff545639>`_.

    If a breakpoint with the given index is not found or is private, returns ``nil``.

    :param integer index: zero-based index of the breakpoint to return, ranging from 0 to the number
        of defined breakpoints; this is specific to the current process
    :rtype: dbgeng.breakpoint

.. function:: get_code_level() -> integer

    Returns the current code level and is mainly used when stepping through code.
    See `GetCodeLevel <https://msdn.microsoft.com/en-us/library/ff545670>`_.

    :returns: one of the values from the :data:`.level` table

.. function:: get_engine_options() -> integer

    Returns the current engine options.
    See `GetEngineOptions <https://msdn.microsoft.com/en-us/library/ff546598>`_.

    :returns: bitfield containing the engine options, which is a combination of the values in the
        :data:`.engopt` table

.. function:: get_expression_syntax() -> integer

    Returns the current syntax that the engine is using for evaluating expressions.
    See `GetExpressionSyntax <https://msdn.microsoft.com/en-us/library/ff546701>`_.

    :returns: one of the values from the :data:`.expr` table

.. function:: get_expression_syntax_names( index ) -> string, string

    Returns the full and abbreviated names of an expression syntax.
    See `GetExpressionSyntaxNames <https://msdn.microsoft.com/en-us/library/ff546708>`_.

    :param integer index: index of the expression syntax, which should be between zero and the
        number returned by :func:`.get_number_expression_syntaxes` minus one.
    :returns: two values, the first being the abbreviated name of the expression syntax, and the
        second being the full name

.. function:: get_interrupt() -> boolean

    Checks whether a user interrupt was issued. If a user interrupt was issued, it's cleared when
    this function is called. Examples of user interrupts include pressing Ctrl+C or pressing the
    Stop button in a debugger. Calling :func:``.set_interrupt`` also causes a user interrupt.
    See `GetInterrupt <https://msdn.microsoft.com/en-us/library/ff546944>`_.

    :returns: true if an interrupt has been requested, otherwise false

.. function:: get_number_expression_syntaxes() -> integer

    Returns the number of expression syntaxes that are supported by the engine.
    See `GetNumberExpressionSyntaxes <https://msdn.microsoft.com/en-us/library/ff547913>`_.

    :returns: number of supported syntaxes

.. function:: get_stack_trace( frmoff, stkoff, instoff, numframes ) -> array of stack frame data

    Returns the frames at the top of the specified call stack.
    See `GetStackTrace <https://msdn.microsoft.com/en-us/library/ff548425>`_.

    :param integer frmoff: memory offset of the stack frame at the top of the stack; if zero, the
        current frame pointer is used
    :param integer stkoff: memory offset of the current stack; if zero, the current stack pointer is
        used
    :param integer instoff: memory offset of the instruction of interest for the function at the top
        of the stack; if zero, the current instruction pointer is used
    :param integer numframes: number of frames to retrieve

    :returns: array of tables representing the requested stack frames, each of which contains the
        following fields:

        * ``instruction_offset`` : memory offset of the related instruction for the stack frame; typically, the return address for the next stack frame, or the current instruction pointer if the frame is at the top of the stack
        * ``return_offset``      : memory offset of the return address for the stack frame; typically, the related instruction for the previous stack frame
        * ``frame_offset``       : memory offset of the stack frame, if known
        * ``stack_offset``       : memory offset of the processor stack
        * ``func_table_entry``   : memory offset of the function entry point for this frame, if available
        * ``params``             : array containing the first four stack slots passed to the function, if available
        * ``virtual``            : boolean, set to true if this stack frame was generated by the debugger during unwinding; set to false if it was formed from a thread's current context
        * ``frame_number``       : index of the frame from the top of the stack, which has index 0

.. function:: is_pointer_64bit() -> boolean

    See `IsPointer64Bit <https://msdn.microsoft.com/en-us/library/ff551092>`_.

    :returns: true if the effective processor uses 64-bit pointers, otherwise false

.. function:: output_disassembly_lines( outctl, previous_lines, total_lines, offset, flags ) -> table of memory location data

    Disassembles several processor instructions and sends the resulting assembly instructions to the
    output callbacks.
    See `OutputDisassemblyLines <https://msdn.microsoft.com/library/windows/hardware/ff553216>`_.

    :param integer outctl: combination of flags from :data:`dbgeng.outctl` that control output
    :param integer previous_lines: number of lines of instructions before *offset* to include in the output
    :param integer total_lines: number of lines of instructions to include in the output
    :param integer offset: memory location to disassemble, along with surrounding instructions depending on
      other arguments
    :param integer flags: bitfield, combining values from the :data:`.disasm_flag` table

    :returns: table containing the following fields:

      * ``offset_line``  : line number in the output that contains the instruction at *offset*
      * ``start_offset`` : memory location of the first instruction included in the output
      * ``end_offset``   : memory location of the instruction after the last disassembled instruction
      * ``line_offsets`` : array that contains the memory location of each instruction included in the
        output; if the output for an instruction spans multiple lines, the array element
        corresponding to the first line of output will contain the address of the instruction

    .. versionadded:: 1.1.0

.. function:: remove_assembly_options( options ) -> boolean

    Turns off some of the assembly and disassembly options. The given options will be removed from
    the existing options.
    See `RemoveAssemblyOptions <https://msdn.microsoft.com/en-us/library/ff554483>`_.

    :param integer options: bitfield, combining values from the :data:`.asmopt` table
    :returns: true if successful

.. function:: remove_breakpoint( bp ) -> boolean

    Removes the given breakpoint. See `RemoveBreakpoint
    <https://msdn.microsoft.com/en-us/library/ff554487>`_.

    :param breakpoint bp: breakpoint to remove
    :returns: true if breakpoint was successfully removed

.. function:: remove_engine_options( options ) -> boolean

    Turns off some of the debugger engine's options, by combining the complement of the given
    bitfield with the existing options using bitwise AND. Possible bitflag values can be found in
    the :data:`.engopt` table.
    See `RemoveEngineOptions <https://msdn.microsoft.com/en-us/library/ff554491>`_.

    :param integer options: bitfield, combining values from the :data:`.engopt` table
    :returns: true if successful

.. function:: remove_extension( handle ) -> boolean

    Unloads an extension library.
    See `RemoveExtension <https://msdn.microsoft.com/en-us/library/ff554497>`_.

    :param integer handle: handle of the extension to unload, returned from :func:`.add_extension`
    :returns: true if successful

.. function:: set_assembly_options( options ) -> boolean

    Changes the assembly and disassembly options. The given options will completely replace the
    existing options.
    See `SetAssemblyOptions <https://msdn.microsoft.com/en-us/library/ff556626>`_.

    :param integer options: bitfield, combining values from the :data:`.asmopt` table
    :returns: true if successful

.. function:: set_code_level( level ) -> boolean

    Sets the current code level and is mainly used when stepping through code.
    See `SetCodeLevel <https://msdn.microsoft.com/en-us/library/ff556629>`_.

    :param integer level: one of the values from the :data:`.level` table
    :returns: true if successful

.. function:: set_engine_options( options ) -> boolean

    Changes the debugger engine's options, completely replacing the existing options. Possible
    bitflag values can be found in the :data:`.engopt` table.
    See `SetEngineOptions <https://msdn.microsoft.com/en-us/library/ff556670>`_.

    :param integer options: bitfield, combining values from the :data:`.engopt` table
    :returns: true if successful

.. function:: set_expression_syntax( syntax) -> boolean

    Sets the syntax that the engine will use to evaluate expressions.
    See `SetExpressionSyntax <https://msdn.microsoft.com/en-us/library/ff556696>`_.

    :param integer syntax: one of the values from the :data:`.expr` table
    :returns: true if successful

.. function:: set_expression_syntax_by_name( syntax ) -> boolean

    Sets the syntax that the engine will use to evaluate expressions, by syntax name.
    See `SetExpressionSyntaxByName <https://msdn.microsoft.com/en-us/library/ff556697>`_.

    :param string syntax: one of the following values:

        * ``"MASM"``      : expressions will be evaluated according to MASM syntax
        * ``"CPLUSPLUS"`` : expressions will be evaluated according to C++ syntax

    :returns: true if successful

.. function:: set_interrupt( flags ) -> boolean

    Registers a user interrupt or breaks into the debugger.
    See `SetInterrupt <https://msdn.microsoft.com/en-us/library/ff556722>`_.

    :param integer flags: one of the values from the :data:`.interrupt` table
    :returns: true if successful

Other
-----

.. data:: asmopt

    Table containing assembly and disassembly options that affect how the debugger engine assembles
    and disassembles processor instructions for the target. Contains the following values, which are
    intended to be combined in a bitfield:

    * ``VERBOSE``             : When set, additional information is included in the disassembly
    * ``NO_CODE_BYTES``       : When set, the raw bytes for an instruction are not included in the disassembly
    * ``IGNORE_OUTPUT_WIDTH`` : When set, the debugger ignores the width of the output display when formatting instructions during dissamebly
    * ``SOURCE_LINE_NUMBER``  : When set, each line of dissembly output is prefixed with the line number of the source code provided by symbol information

    See `DEBUG_ASMOPT_XXX <https://msdn.microsoft.com/en-us/library/ff541443>`_.

.. data:: disasm_flag

    Table containing options controlled some disassembly functions. Contains the following values,
    which are intended to be combined in a bitfield:

    * ``EFFECTIVE_ADDRESS``  : Compute the effective address from the current register information.
    * ``MATCHING_SYMBOLS``   : If the address of the instruction has an exact symbol match, output the symbol.
    * ``SOURCE_LINE_NUMBER`` : Include the source line number of the instruction in the output.
    * ``SOURCE_FILE_NAME``   : Include the source file name in the output.

    See the table in the documentation for `OutputDisassembly <https://msdn.microsoft.com/en-us/library/windows/hardware/ff553211>`_.

.. data:: engopt

    Table containing global options that affect the behavior of the debugger engine. Contains the
    following values, which are intended to be combined in a bitfield:

    * ``IGNORE_DBGHELP_VERSION``      : The debugger engine generates a warning instead of an error if the version of the DbgHelp DLL does not match the version of the debugger engine.
    * ``IGNORE_EXTENSION_VERSIONS``   : Disable version checking for extensions.
    * ``ALLOW_NETWORK_PATHS``         : Network shares can be used for loading symbols and extensions. This option cannot be set if *DISALLOW_NETWORK_PATHS* is set.
    * ``DISALLOW_NETWORK_PATHS``      : Network shares cannot be used for loading symbols and extensions. This option cannot be set if *ALLOW_NETWORK_PATHS* is set.
    * ``NETWORK_PATHS``               : Bitwise OR of *ALLOW_NETWORK_PATHS* and *DISALLOW_NETWORK_PATHS*.
    * ``IGNORE_LOADER_EXCEPTIONS``    : Ignore expected first-chance exceptions that are generated by the loader in certain versions of Windows.
    * ``INITIAL_BREAK``               : Break into the debugger at the target's initial event.
    * ``INITIAL_MODULE_BREAK``        : Break into the debugger when the target loads its first module.
    * ``FINAL_BREAK``                 : Break into the debugger at the target's final event. In a live user-mode target, this is when the process exits. It has no effect in kernel mode.
    * ``NO_EXECUTE_REPEAT``           : When given an empty command, the debugger engine does not repeat the last command.
    * ``FAIL_INCOMPLETE_INFORMATION`` : Prevent the debugger from loading modules whose images cannot be mapped.
    * ``ALLOW_READ_ONLY_BREAKPOINTS`` : Allow the debugger engine to manipulate page protections on the target to allow for setting software breakpoints in a read-only section of memory.
    * ``SYNCHRONIZE_BREAKPOINTS``     : In live user-mode debugging, the engine performs extra work when inserting and removing breakpoints to ensure that all threads in the target have a consistent breakpoint state at all times.
    * ``DISALLOW_SHELL_COMMANDS``     : Disallow executing shell commands through the debugger. After this option has been set, it cannot be unset.
    * ``KD_QUIET_MODE``               : Turn on quiet mode. For more info see the `documentation <https://msdn.microsoft.com/en-us/library/ff558789>`_ for the ``sq`` command.
    * ``DISABLE_MANAGED_SUPPORT``     : Disables debugger engine support for managed code. If support for managed code is already in use, this option has no effect.
    * ``DISABLE_MODULE_SYMBOL_LOAD``  : The debugger does not load symbols for modules that are loaded while this flag is set.
    * ``DISABLE_EXECUTION_COMMANDS``  : Prevents any commands that would cause the target to begin executing.
    * ``DISALLOW_IMAGE_FILE_MAPPING`` : Disallows mapping of image files from disk.
    * ``PREFER_DML``                  : The debugger runs DML-enhanced versions of commands and operations by default.
    * ``DISABLESQM``                  : Disables upload of Software Quality Metrics (SQM) data.
    * ``DISABLE_STEPLINES_OPTIONS``

    See `DEBUG_ENGOPT_XXX <https://msdn.microsoft.com/en-us/library/ff541475>`_.

.. data:: execute_flag

    Table containing options that control the operation of :func:`.execute`. Contains the following
    values:

    * ``ECHO``       : The command string is sent to the output.
    * ``NOT_LOGGED`` : The command string is not logged. This is overridden by ``ECHO``.
    * ``NO_REPEAT``  : If command is an empty string, do not repeat the last command, and do not save the current command string for repeat execution later.

    See the table in the documentation for `Execute <https://msdn.microsoft.com/en-us/library/ff543208>`_.

.. data:: expr

    Table containing values that represent possible expression evaluation syntaxes. Contains the
    following values:

    * ``MASM``      : Expressions will be evaluated according to MASM syntax.
    * ``CPLUSPLUS`` : Expressions will be evaluated according to C++ syntax.

    See the table in the documentation for `GetExpressionSyntax <https://msdn.microsoft.com/en-us/library/ff546701>`_.

.. data:: interrupt

    Table containing values that control the operation of :func:`.set_interrupt`. Contains the
    following values:

    * ``ACTIVE``  : If the target is running, the engine will request a break into the debugger. This request might time out. Otherwise, when the target is suspended, the engine will register a user interrupt.
    * ``PASSIVE`` : The engine will register a user interrupt.
    * ``EXIT``    : See documentation on MSDN.

    See table in the documentation for `SetInterrupt <https://msdn.microsoft.com/en-us/library/ff556722>`_.

.. data:: level

    Table containing values that represent possible code stepping levels. Contains the following
    values:

    * ``SOURCE``   : When stepping through code on the target, the size of a single step will be a line of source code.
    * ``ASSEMBLY`` : When stepping through code on the target, the size of a single step will be a single processor instruction.

    See the table in the documentation for `GetCodeLevel <https://msdn.microsoft.com/en-us/library/ff545670>`_.
