// General C++ <--> Lua interop functions

#pragma once

struct lua_State;

// Types and functions for bridging C++ and Lua
namespace bridge
{

    // for each value on the stack:
    //  converts it, in place, to a string using tostring()
    //  prints the result to the debugger console
    //  pops the string
    // the stack is emptied by this function
    int my_print( lua_State * L );

    // just like my_print above, except outputs using the DML outputter
    int dml_print( lua_State * L );

    // Wrap a lua_State in RAII
    struct lua
    {
        std::shared_ptr< lua_State > L;

        lua();

        operator lua_State *() { return L.get(); }
    };

    // Convenient way to report initalization status
    struct lua_inited
    {
        bool success;
        lua L;
        std::string errmsg;

        lua_inited() : success( false ) {}
        lua_inited( lua L ) : success( true ), L( L ) {}
        lua_inited( char const * errmsg ) : success( false ), errmsg( errmsg ) {}
        lua_inited( std::string  errmsg ) : success( false ), errmsg( errmsg ) {}
    };

    // init a Lua!
    bridge::lua_inited init();

    // run some Lua code
    // on failure, returns the error message
    std::string run( lua_State * L, std::string const & code );

    // run a registered command
    // on failure, returns the error message
    std::string run_registered_command( lua_State * L, std::string const & command_line );

    // show help for a registered command, or all commands if 'command' is empty
    // on failure, returns the error message
    std::string help_registered_command( lua_State * L, std::string const & command_line );

} // namespace bridge
