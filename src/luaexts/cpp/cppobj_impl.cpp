#include "stdafx.h"

#include "cppobj_impl.h"
#include "cppobj.h"
#include "util.h"

#include <regex>

namespace
{
    // helper function used to initialize the 'm_offset' const data member
    ULONG64 add_symbol( SymbolGroup & symgroup, std::string const & expr, bool & valid )
    {
        valid = false;

        auto idx = symgroup.AddSymbol( expr, DEBUG_ANY_ID );

        if ( idx != DEBUG_ANY_ID )
        {
            valid = true;
            return symgroup.GetSymbolEntryInformation( idx ).Offset;
        }

        return 0;
    }

    // helper functions to find a symbol within the group
    ULONG find_symbol_index( SymbolGroup const & symgroup, ULONG64 const offset, ULONG const type_id )
    {
        auto const count = symgroup.GetNumberSymbols();

        for ( ULONG i = 0; i < count; ++i )
        {
            auto dse = symgroup.GetSymbolEntryInformation( i );
            if ( offset == dse.Offset && type_id == dse.TypeId )
                return i;
        }

        return DEBUG_ANY_ID;
    }
    ULONG find_symbol_index( SymbolGroup const & symgroup, cppobj_impl const & obj )
    {
        auto const offset  = obj.offset();
        auto const type_id = obj.type_id();
        return find_symbol_index( symgroup, offset, type_id );
    }

    // is the given symbol in the symbol group a baseclass?
    bool is_baseclass( SymbolGroup const & symgroup, ULONG idx )
    {
        // baseclasses show up as a symbol with the same name as its type
        auto symname = symgroup.GetSymbolName( idx );
        auto typname = symgroup.GetSymbolTypeName( idx );
        auto count = symgroup.GetNumberSymbols(); // for some reason, the above call to GetSymbolTypeName() will sometimes mess up the symbol group, but calling GetNumberSymbols() seems to fix it
        typname = std::regex_replace( std::regex_replace( typname, std::regex( "^class " ), "" ), std::regex( "^struct " ), "" );
        return symname == typname;
    }

    // is the given symbol in the symbol group a vtable?
    bool is_vtable( SymbolGroup const & symgroup, ULONG idx )
    {
        auto symname = symgroup.GetSymbolName( idx );
        return symname == "__vfptr";
    }

    // does this expression need parens around it to be safely embedded in another expression
    bool needs_parens( std::string const & expr )
    {
        bool needs = false;

        // doesn't need parens because it's already got them
        if ( '(' == expr.front() && ')' == expr.back() )
            needs = false;

        // anything else will need parens unless it's just a plain identifier
        else
            needs = ! std::regex_match( expr, std::regex{ "[_A-Za-z][_A-Za-z0-9]*" } );

        return needs;
    }
}

////////////////////////////////////////////////////////////////////////////////
// cppobj_impl definition
// This class is used to hide the Lua cppobj operations from the implementation
// details of how the debugging target's C++ objects are actually referenced and used.
//
// NOTE: Symbol indexes within the symbol group can change any time a symbol is added
// to the group, for example when a symbol is expanded. So the key characteristic used
// for identifying a symbol is its offset, which I think will never change.
cppobj_impl::cppobj_impl( std::string const & expression )
    : m_valid{ false }
    , m_group{ std::make_shared< SymbolGroup >( Symbols::CreateSymbolGroup() ) }
    , m_offset{ add_symbol( *m_group, expression, m_valid ) }
    , m_type_id{ m_group->GetSymbolEntryInformation( 0 ).TypeId }
    , m_short_name{}
    , m_long_name{}
    , m_type{}
    , m_size{ 0 }
    , m_kind{ cppobj::kind_t::unknown_ }
    , m_needs_parens{ needs_parens( expression ) }
    , m_expanded{ false }
    , m_direct_data_members{}
    , m_data_members{}
    , m_base_classes{}
    , m_vtables{}
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object expression: %s", expression.c_str() );

    auto idx = find_symbol_index( *m_group, *this );
    m_valid = idx != DEBUG_ANY_ID;
    if ( m_valid )
    {
        auto dse = m_group->GetSymbolEntryInformation( idx );
        m_size   = dse.Size;
        m_kind   = util::symtagenum::tokind( static_cast< enum SymTagEnum >( dse.Tag ) );
        set_name_and_type( idx );
    }
}

// private constructor for creating children
// this object is assumed to be valid
cppobj_impl::cppobj_impl( std::string const & expression, std::string const & short_name )
    : cppobj_impl{ expression }
{
    m_short_name = short_name;
    m_needs_parens = '*' == expression.front();
}

// private constructor for creating children
// this object is assumed to be valid
cppobj_impl::cppobj_impl( std::shared_ptr< SymbolGroup > group, ULONG64 offset, ULONG type_id, cppobj_impl const & parent )
    : m_valid{ true }
    , m_group{ group }
    , m_offset{ offset }
    , m_type_id{ type_id }
    , m_short_name{}
    , m_long_name{}
    , m_type{}
    , m_size{ 0 }
    , m_kind{ cppobj::kind_t::unknown_ }
    , m_needs_parens{ false }
    , m_expanded{ false }
    , m_direct_data_members{}
    , m_data_members{}
    , m_base_classes{}
    , m_vtables{}
{
    auto idx = find_symbol_index( *m_group, *this );
    m_valid = idx != DEBUG_ANY_ID;
    if ( m_valid )
    {
        auto dse = m_group->GetSymbolEntryInformation( idx );
        m_size   = dse.Size;
        m_kind   = util::symtagenum::tokind( static_cast< enum SymTagEnum >( dse.Tag ) );
        set_name_and_type( idx, &parent );
    }
}

// private constructor for creating an empty, invalid object
cppobj_impl::cppobj_impl()
    : m_valid{ false }
    , m_group{}
    , m_offset{ 0 }
    , m_type_id{ 0 }
    , m_short_name{}
    , m_long_name{}
    , m_type{}
    , m_size{ 0 }
    , m_kind{ cppobj::kind_t::unknown_ }
    , m_needs_parens{ false }
    , m_expanded{ false }
    , m_direct_data_members{}
    , m_data_members{}
    , m_base_classes{}
    , m_vtables{}
{
}

void cppobj_impl::set_name_and_type( ULONG const index, cppobj_impl const * const parent /* = nullptr */ )
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    bool const baseclass = is_baseclass( *m_group, index );
    bool deref = false;

    if ( m_short_name.empty() )
    {
        if ( baseclass )
        {
            m_short_name = parent->m_short_name;
        }
        else
        {
            m_short_name = m_group->GetSymbolName( index );
            if ( "*" == m_short_name )
            {
                deref = true;
                m_short_name = util::make_string() << "*" << parent->m_short_name;
            }
        }
    }

    // m_long_name = "kurt";

    if ( m_long_name.empty() )
    {
        if ( ! parent )
        {
            m_long_name = m_short_name;
        }
        else
        {
            std::string expression = parent->m_long_name;
            if ( baseclass )
            {
                expression = util::make_string() << "*static_cast<" <<  m_group->GetSymbolName( index ) << "*>(&" << m_short_name << ")";
            }
            else
            {
                auto parent_kind = parent->kind();
                // check kind of parent symbol to know whether to use arrow or dot or subscript
                if ( cppobj::kind_t::pointer_ == parent_kind )
                {
                    if ( deref )
                    {
                        expression = util::make_string() << "*(" << expression << ")";
                    }
                    else
                    {
                        expression = util::make_string() << "(" << expression << ")->" << m_short_name;
                    }
                }
                else if ( cppobj::kind_t::class_ == parent_kind )
                {
                    expression = util::make_string() << "(" << expression << ")." << m_short_name;
                }
                else if ( cppobj::kind_t::array_ == parent_kind )
                {
                    // the short name for array element should include the brackets
                    expression = util::make_string() << "(" << expression << ")" << m_short_name;
                }
                else
                {
                    // no other kind should have a child?
                    g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: unsupported kind processing long_name" );
                }
    
            }

            m_long_name = expression;
        }
    }

#if 0 // this was the old, slow way
    if ( m_long_name.empty() )
    {
        // we need to establish the chain of symbols leading from this symbol back
        // to its greatest ancestor that appears in the group
        std::list< ULONG > indexes;
        auto dsps = m_group->GetSymbolParameters( 0, m_group->GetNumberSymbols() );
        for ( auto symidx = index; symidx != DEBUG_ANY_ID; symidx = dsps[ symidx ].ParentSymbol )
        {
            if ( ! is_baseclass( *m_group, symidx ) )
                indexes.push_front( symidx );
        }

        // now, walk the chain and construct an expression
        std::string expression;
        cppobj::kind_t kind = cppobj::kind_t::unknown_;
        for ( const auto & symidx : indexes )
        {
            auto const dse = m_group->GetSymbolEntryInformation( symidx );
            std::string const name = m_group->GetSymbolName( symidx );
            if ( expression.empty() )
            {
                expression = name;
            }
            else
            {
                // check kind of previous symbol to know whether to use arrow or dot
                if ( cppobj::kind_t::pointer_ == kind )
                {
                    expression = util::make_string() << "(" << expression << ")->" << name;
                }
                else if ( cppobj::kind_t::class_ == kind )
                {
                    expression = util::make_string() << "(" << expression << ")." << name;
                }
                else
                {
                    // no other kind should have a child?
                    g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: unsupported kind processing long_name" );
                }
            }

            kind = util::symtagenum::tokind( static_cast< enum SymTagEnum >( dse.Tag ) );
        }

        m_long_name = expression;
    }
#endif

    if ( m_type.empty() )
    {
        auto const dse = m_group->GetSymbolEntryInformation( index );
        // Symbols::GetTypeName returns a better type string for display and
        // comparison than SymbolGroup::GetSymbolTypeName.
        m_type = Symbols::GetTypeName( dse.ModuleBase, dse.TypeId );
    }
}

// create a display name for this child
std::string cppobj_impl::child_display_name( ULONG const index )
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    auto child_name = m_group->GetSymbolName( index );

    if ( "*" == child_name )
        child_name += m_short_name;

    return child_name;
}

// with this object as the parent, create a child expression for the child at the given index
std::string cppobj_impl::child_expression( ULONG const index )
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    auto child_name = m_group->GetSymbolName( index );

    if ( is_baseclass( *m_group, index ) )
    {
        std::string long_name;
        if ( cppobj::kind_t::pointer_ != m_kind )
            long_name = util::make_string() << "&" << safe_long_name();
        else
            long_name = m_long_name;

        if ( std::string::npos != child_name.find( ' ' ) )
            child_name = "@!\"" + child_name + '"';

        return
            util::make_string()
            << "*static_cast<" << child_name << "*>(" // if it's a baseclass, child_name will actually be the type name
            << long_name << ")";
    }

    std::string expression{ safe_long_name() };

    auto dsp = m_group->GetSymbolParameters( index );
    auto this_index = find_symbol_index( *m_group, *this );
    if ( dsp.ParentSymbol != this_index )
    {
        // if this child shares a name with any other child, we'll need a baseclass cast to get the right symbol
        auto count = m_group->GetNumberSymbols();
        for ( ULONG i = 0; i < count; ++i )
        {
            if ( i == this_index || i == index )
                continue;

            auto nm = m_group->GetSymbolName( i );
            if ( nm == child_name )
            {
                expression = util::make_string() << "(" << child_expression( dsp.ParentSymbol ) << ")";
                break;
            }
        }
    }

    if ( "*" == child_name )
    {
        // a child name of "*" is a deref'd pointer
        expression = "*" + expression;
    }
    else if ( cppobj::kind_t::pointer_ == m_kind )
    {
        expression = expression + "->" + child_name;
    }
    else if ( cppobj::kind_t::class_ == m_kind )
    {
        expression = expression + "." + child_name;
    }
    else if ( cppobj::kind_t::array_ == m_kind )
    {
        expression = expression + child_name;
    }
    else
    {
        // no other kind should have a child?
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: unsupported kind processing long_name" );
    }

    return expression;
}

// is this a valid object
bool cppobj_impl::valid() const
{
    return m_valid;
}

// short name of this object, usually just the field name
std::string const & cppobj_impl::short_name() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    return m_short_name;
}

// long name of this object, which should be a fully qualified expression valid for the current scope
std::string const & cppobj_impl::long_name() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    return m_long_name;
}

// long name surrounded by parentheses if needed
std::string cppobj_impl::safe_long_name() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    if ( ! m_needs_parens )
        return m_long_name;

    return util::make_string() << "(" << m_long_name << ")";
}

// type of the object
std::string const & cppobj_impl::type() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    return m_type;
}

// size of this object
ULONG cppobj_impl::size() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    return m_size;
}

// offset of this object in the target's memory
ULONG64 cppobj_impl::offset() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    return m_offset;
}

// type id of the object
ULONG cppobj_impl::type_id() const
{
    return m_type_id;
}

// kind of this object
cppobj::kind_t cppobj_impl::kind() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    return m_kind;
}

// expands this symbol and tracks its kids
void cppobj_impl::expand()
{
    if ( m_expanded ) return;

    auto index = find_symbol_index( *m_group, *this );
    m_valid = index != DEBUG_ANY_ID;

    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    ULONG const prevcount = m_group->GetNumberSymbols();

    HRESULT hr = S_OK;
    m_group->ExpandSymbol( index, true, &hr );
    m_expanded = true;

    if ( S_FALSE == hr ) // no kids
        return;

    if ( E_INVALIDARG == hr )
    {
        // this symbol's associated group has reach its expansion limit and can't expand further.
        // for now, we'll just throw when this happens, but it should be possible to accomodate
        // this situation by replacing this symbol's group with a newly created group using the
        // symbol's long name as the base symbol.
        g_Ext->ThrowStatus( hr, "cppobj_impl: symbol group related to '%s' at its expansion limit", m_long_name.c_str() );
    }

    // expansion could change the symbol's index
    index = find_symbol_index( *m_group, *this );

    // STEP ONE: find all of this object's direct data members, and keep track of direct base classes
    typedef std::pair< ULONG64, ULONG > member_t;
    auto count = m_group->GetNumberSymbols();
    auto dsps = m_group->GetSymbolParameters( 0, count );
    std::list< member_t > bases_to_expand;
    for ( ULONG i = 0; i < count; ++i )
    {
        if ( dsps[ i ].ParentSymbol != index )
            continue;

        auto dse = m_group->GetSymbolEntryInformation( i );

        if ( is_baseclass( *m_group, i ) )
        {
            bases_to_expand.emplace_back( dse.Offset, dse.TypeId );
            continue;
        }
        else if ( is_vtable( *m_group, i ) )
        {
            // TODO: do something with vtables
            continue;
        }

        // if we got here, this should be a data member
        m_direct_data_members.push_back( cppobj_impl( m_group, dse.Offset, dse.TypeId, *this ) );
        // m_direct_data_members.push_back( cppobj_impl{ child_expression( i ), child_display_name( i ) } );
    }

    // STEP TWO: expand all baseclasses so we can get at every inherited member
    while ( ! bases_to_expand.empty() )
    {
        member_t base = bases_to_expand.front();
        bases_to_expand.pop_front();
        auto inspos = std::begin( bases_to_expand );

        index = find_symbol_index( *m_group, std::get< 0 >( base ), std::get< 1 >( base ) );
        m_group->ExpandSymbol( index, true, &hr );

        if ( E_INVALIDARG == hr )
        {
            // this symbol's associated group has reach its expansion limit and can't expand further.
            // for now, we'll just throw when this happens, but it should be possible to accomodate
            // this situation by replacing this symbol's group with a newly created group using the
            // symbol's long name as the base symbol.
            g_Ext->ThrowStatus( hr, "cppobj_impl: symbol group related to '%s' at its expansion limit", m_long_name.c_str() );
        }

        // find it again so we can find all of its bases
        index = find_symbol_index( *m_group, std::get< 0 >( base ), std::get< 1 >( base ) );
        count = m_group->GetNumberSymbols();
        dsps = m_group->GetSymbolParameters( 0, count );
        for ( ULONG i = 0; i < count; ++i )
        {
            if ( dsps[ i ].ParentSymbol != index )
                continue;

            auto dse = m_group->GetSymbolEntryInformation( i );

            if ( is_baseclass( *m_group, i ) )
            {
                bases_to_expand.emplace( inspos, dse.Offset, dse.TypeId );
            }
        }
    }

    // STEP THREE: walk the list cataloging anything related to this object
    index = find_symbol_index( *m_group, *this );
    count = m_group->GetNumberSymbols();
    dsps = m_group->GetSymbolParameters( 0, count );
    for ( ULONG i = 0; i < count; ++i )
    {
        if ( i == index )
            continue;

        for ( auto j = i; j != DEBUG_ANY_ID; j = dsps[ j ].ParentSymbol )
        {
            if ( j == index ) // i is a descendent of this object
            {
                auto dse = m_group->GetSymbolEntryInformation( i );
                cppobj_impl member( m_group, dse.Offset, dse.TypeId, *this );
                if ( is_baseclass( *m_group, i ) )
                {
                    m_base_classes.push_back( member );
                    // m_base_classes.push_back( cppobj_impl{ child_expression( i ), m_short_name } );
                }
                else if ( is_vtable( *m_group, i ) )
                {
                    // m_vtables.push_back( member );
                    // m_vtables.push_back( cppobj_impl{ child_expression( i ), child_display_name( i ) } );
                }
                else
                {
                    m_data_members.push_back( member );
                    // m_data_members.push_back( cppobj_impl{ child_expression( i ), child_display_name( i ) } );
                }

                break;
            }
        }
    }
}

// does this object have a data member of a given name
bool cppobj_impl::has_member( std::string const & name )
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    expand();

    for ( const auto & member : m_data_members )
    {
        if ( name == member.m_short_name )
            return true;
    }

    return false;
}

// return an object representing a named data member from this object
cppobj_impl cppobj_impl::member( std::string const & name )
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    expand();

    if ( m_data_members.empty() )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: '%s' has no data members" );

    // when looking for a member by name, we need to search backwards, so that members from
    // derived classes are found before members from base classes with the same name
    for ( auto it = m_data_members.crbegin(); it != m_data_members.crend(); ++it )
    {
        if ( name == it->m_short_name )
            return *it;
    }

    return cppobj_impl();
}

// return an object representing a data member from this object based on its index within this object
cppobj_impl cppobj_impl::member( size_t index )
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    expand();

    if ( m_data_members.size() <= index )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: member index '%d' out of range for '%s'", index, m_long_name.c_str() );

    return m_data_members[ index ];
}

// return all data members, in index order
std::vector< cppobj_impl > const & cppobj_impl::members()
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    expand();

    return m_data_members;
}

// return just the object's direct data members, excluding inherited members
std::vector< cppobj_impl > const & cppobj_impl::direct_members()
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    expand();

    return m_direct_data_members;
}

// if this object is an array or pointer, return an object with the given element offset
cppobj_impl cppobj_impl::array_element( ptrdiff_t index )
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    if ( cppobj::kind_t::array_ != m_kind && cppobj::kind_t::pointer_ != m_kind )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: kind of '%s' not array or pointer for array indexing", m_long_name.c_str() );

    // arrays can actually be expanded in a symbol group, but if the index is negative, that wouldn't help.
    // so just create a new cppobj_impl with its own indexed expression and symbol group.
    // std::string const expr = util::make_string() << "(" << m_long_name << ")[" << index << "]";
    std::string const expr = util::make_string() << safe_long_name() << "[" << index << "]";
    cppobj_impl elem( expr );
    return elem;
}

// return an object representing a pointer to this object
cppobj_impl cppobj_impl::reference() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    // std::string const expr = util::make_string() << "&(" << m_long_name << ")";
    std::string const expr = util::make_string() << "&" << safe_long_name();
    cppobj_impl ptr( expr );
    return ptr;
}

// if this object is a pointer, return the object it points to
// if this object is an array, return the first element
cppobj_impl cppobj_impl::dereference() const
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    if ( cppobj::kind_t::array_ != m_kind && cppobj::kind_t::pointer_ != m_kind )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: kind of '%s' not pointer or array for dereferencing", m_long_name.c_str() );

    // std::string const expr = util::make_string() << "*(" << m_long_name << ")";
    std::string const expr = util::make_string() << "*" << safe_long_name();
    cppobj_impl deref( expr );
    return deref;
}

// return list of base class representations of this object
std::vector< cppobj_impl > const & cppobj_impl::base_classes()
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    expand();

    return m_base_classes;
}

// return list of vtables for this object
std::vector< cppobj_impl > const & cppobj_impl::vtables()
{
    if ( ! m_valid )
        g_Ext->ThrowStatus( E_FAIL, "cppobj_impl: invalid object" );

    expand();

    return m_vtables;
}

// data retrieval

bool    cppobj_impl::as_bool() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetStdBool();
}

BOOLEAN cppobj_impl::as_boolean() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetBoolean();
}

BOOL    cppobj_impl::as_w32bool() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetW32Bool();
}

CHAR    cppobj_impl::as_char() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetChar();
}

UCHAR   cppobj_impl::as_uchar() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetUchar();
}

SHORT   cppobj_impl::as_short() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetShort();
}

USHORT  cppobj_impl::as_ushort() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetUshort();
}

LONG    cppobj_impl::as_long() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetLong();
}

ULONG   cppobj_impl::as_ulong() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetUlong();
}

LONG64  cppobj_impl::as_int64() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetLong64();
}

ULONG64 cppobj_impl::as_uint64() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetUlong64();
}

float   cppobj_impl::as_float() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetFloat();
}

double  cppobj_impl::as_double() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetDouble();
}

ULONG64 cppobj_impl::as_ptr() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetPtr();
}

ULONG64 cppobj_impl::raw() const
{
    ExtRemoteData data( m_long_name.c_str(), m_offset, m_size );
    return data.GetData( m_size );
}

// dbgeng string interpretation of the object's value
std::string cppobj_impl::as_string() const
{
    auto idx = find_symbol_index( *m_group, *this );
    return m_group->GetSymbolValueText( idx );
}
