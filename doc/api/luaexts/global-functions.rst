Global functions
================

Most of the API provided by luaexts is scoped within an appropriately named global table. However,
there are a number of global functions that are accessible without prefixing.

.. function:: print( anything ) -> nil

    Takes any number of arguments of any type, converts them to strings, and outputs those strings
    to the debugger's output window with no other formatting.

.. function:: say( anything ) -> nil

    Exactly like :func:`print`, but appends a newline to whatever is output.

.. function:: dml_print( anything ) -> nil

    Similar to :func:`print`, but outputs using `debugger markup language`_ (DML).

    .. _`debugger markup language`: https://msdn.microsoft.com/en-us/library/mt613235
