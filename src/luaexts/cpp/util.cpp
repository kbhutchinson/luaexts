////////////////////////////////////////////////////////////////////////////////
// Utility classes and functions
//

#include "stdafx.h"

#include "util.h"
#include "cppobj.h"
#include "cppobj_impl.h"

#include <regex>

namespace util
{

// returns the full path, including DLL filename, that this DLL was loaded from
filesystem::path get_dll_path()
{
    filesystem::path fullpath;

    char buf[ MAX_PATH ] = { 0 };
    auto addr = reinterpret_cast< char * >( &g_Ext );
    HMODULE hmod = 0;
    if ( ::GetModuleHandleExA( GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, addr, &hmod ) )
    {
        ::GetModuleFileNameA( hmod, buf, ARRAYSIZE( buf ) );
        fullpath = buf;
    }

    return fullpath;
}

// returns a path to the user's home directory
filesystem::path get_home_path()
{
    // try $HOME, then, if not available, try $HOMEDRIVE$HOMEPATH
    size_t pathsize = 0;
    char buf[ MAX_PATH ] = {};

    if ( 0 == getenv_s( &pathsize, buf, "HOME" ) && 0 < pathsize )
        return filesystem::path( buf );

    if ( 0 != getenv_s( &pathsize, buf, "HOMEDRIVE" ) || 0 == pathsize )
        return nullptr;

    filesystem::path homepath = buf;

    if ( 0 != getenv_s( &pathsize, buf, "HOMEPATH" ) || 0 == pathsize )
        return nullptr;

    homepath /= buf;
    return homepath;
}

basetype_t type_basetype( std::string const & typname )
{
    using std::regex;
    using std::regex_search;

    if ( regex_search( typname, regex( "\\bbool\\b" ) ) )
        return basetype_t::bool_;
    else if ( regex_search( typname, regex( "\\bwchar_t\\b" ) ) )
        return basetype_t::wchar_t_;
    else if ( regex_search( typname, regex( "\\bchar16_t\\b" ) ) )
        return basetype_t::char16_t_;
    else if ( regex_search( typname, regex( "\\bchar32_t\\b" ) ) )
        return basetype_t::char32_t_;
    else if ( regex_search( typname, regex( "\\bfloat\\b" ) ) )
        return basetype_t::float_;
    else if ( regex_search( typname, regex( "\\bdouble\\b" ) ) )
    {
        if ( regex_search( typname, regex( "\\blong\\b" ) ) )
            return basetype_t::ldouble_;
        else
            return basetype_t::double_;
    }
    else if ( regex_search( typname, regex( "\\bchar\\b" ) ) )
    {
        if ( regex_search( typname, regex( "\\bunsigned\\b" ) ) )
            return basetype_t::uchar_;
        else
            return basetype_t::char_;
    }
    else if ( regex_search( typname, regex( "\\bshort\\b" ) ) )
    {
        if ( regex_search( typname, regex( "\\bunsigned\\b" ) ) )
            return basetype_t::ushort_;
        else
            return basetype_t::short_;
    }
    else if ( regex_search( typname, regex( "\\blong long\\b" ) ) )
    {
        if ( regex_search( typname, regex( "\\bunsigned\\b" ) ) )
            return basetype_t::ulong64_;
        else
            return basetype_t::long64_;
    }
    else if ( regex_search( typname, regex( "\\bint64\\b" ) ) )
    {
        if ( regex_search( typname, regex( "\\bunsigned\\b" ) ) )
            return basetype_t::ulong64_;
        else
            return basetype_t::long64_;
    }
    else if ( regex_search( typname, regex( "\\blong\\b" ) ) )
    {
        if ( regex_search( typname, regex( "\\bunsigned\\b" ) ) )
            return basetype_t::ulong_;
        else
            return basetype_t::long_;
    }
    else if ( regex_search( typname, regex( "\\bint\\b" ) ) )
    {
        if ( regex_search( typname, regex( "\\bunsigned\\b" ) ) )
            return basetype_t::uint_;
        else
            return basetype_t::int_;
    }

    return basetype_t::none_;
}

basetype_t object_basetype( cppobj_impl const & object )
{
    std::string const typname = object.type();
    return type_basetype( typname );
}

bool is_integral_type( basetype_t t )
{
    switch ( t )
    {
    case basetype_t::char_:
    case basetype_t::uchar_:
    case basetype_t::wchar_t_:
    case basetype_t::char16_t_:
    case basetype_t::char32_t_:
    case basetype_t::short_:
    case basetype_t::ushort_:
    case basetype_t::int_:
    case basetype_t::uint_:
    case basetype_t::long_:
    case basetype_t::ulong_:
    case basetype_t::long64_:
    case basetype_t::ulong64_:
        return true;
    }

    return false;
}

bool is_unsigned_type( basetype_t t )
{
    switch ( t )
    {
    case basetype_t::uchar_:
    case basetype_t::wchar_t_:
    case basetype_t::char16_t_:
    case basetype_t::char32_t_:
    case basetype_t::ushort_:
    case basetype_t::uint_:
    case basetype_t::ulong_:
    case basetype_t::ulong64_:
        return true;
    }

    return false;
}

namespace
{
    ULONG basetype_ids[ static_cast< int >( basetype_t::last_ ) ] = {};
    void init_basetype_id_list()
    {
        if ( basetype_ids[ 1 ] > 0 )
            return;

        std::vector< std::string > types;
        types.push_back( "bool" );
        types.push_back( "char" );
        types.push_back( "unsigned char" );
        types.push_back( "wchar_t" );
        types.push_back( "char16_t" );
        types.push_back( "char32_t" );
        types.push_back( "short" );
        types.push_back( "unsigned short" );
        types.push_back( "int" );
        types.push_back( "unsigned int" );
        types.push_back( "long" );
        types.push_back( "unsigned long" );
        types.push_back( "int64" );
        types.push_back( "unsigned int64" );
        types.push_back( "float" );
        types.push_back( "double" );
        types.push_back( "double" );

        ULONG idx = 1;
        for ( const auto & type : types )
        {
            ExtRemoteTyped ert( ( util::make_string() << "static_cast<" << type << ">(0)" ).c_str() );
            basetype_ids[ idx++ ] = ert.m_Typed.BaseTypeId;
        }
    }
}

ULONG basetype_id( basetype_t t )
{
    init_basetype_id_list();
    return basetype_ids[ static_cast< ULONG >( t ) ];
}

std::string symtagenum::stringify( enum SymTagEnum val )
{
    switch ( val )
    {
    case SymTagNull             : return "Null";
    case SymTagExe              : return "Exe";
    case SymTagCompiland        : return "Compiland";
    case SymTagCompilandDetails : return "CompilandDetails";
    case SymTagCompilandEnv     : return "CompilandEnv";
    case SymTagFunction         : return "Function";
    case SymTagBlock            : return "Block";
    case SymTagData             : return "Data";
    case SymTagAnnotation       : return "Annotation";
    case SymTagLabel            : return "Label";
    case SymTagPublicSymbol     : return "PublicSymbol";
    case SymTagUDT              : return "UserDefinedType";
    case SymTagEnum             : return "Enum";
    case SymTagFunctionType     : return "FunctionType";
    case SymTagPointerType      : return "PointerType";
    case SymTagArrayType        : return "ArrayType";
    case SymTagBaseType         : return "BaseType";
    case SymTagTypedef          : return "Typedef";
    case SymTagBaseClass        : return "BaseClass";
    case SymTagFriend           : return "Friend";
    case SymTagFunctionArgType  : return "FunctionArgType";
    case SymTagFuncDebugStart   : return "FuncDebugStart";
    case SymTagFuncDebugEnd     : return "FuncDebugEnd";
    case SymTagUsingNamespace   : return "UsingNamespace";
    case SymTagVTableShape      : return "VTableShape";
    case SymTagVTable           : return "VTable";
    case SymTagCustom           : return "Custom";
    case SymTagThunk            : return "Thunk";
    case SymTagCustomType       : return "CustomType";
    case SymTagManagedType      : return "ManagedType";
    case SymTagDimension        : return "Dimension";
    case SymTagCallSite         : return "CallSite";
    case 32 /*SymTagInlineSite   */ : return "InlineSite";
    case 33 /*SymTagBaseInterface*/ : return "BaseInterface";
    case 34 /*SymTagVectorType   */ : return "VectorType";
    case 35 /*SymTagMatrixType   */ : return "MatrixType";
    case 36 /*SymTagHLSLType     */ : return "HLSLType";
    }

    return "SymTag Unknown";
}

// similar to the above, but the string returned more closely matches Lua's type() function
std::string symtagenum::tostring( enum SymTagEnum val )
{
    switch ( val )
    {
    case SymTagFunction         : return "Function";
    case SymTagUDT              : return "UserDefinedType";
    case SymTagEnum             : return "Enum";
    case SymTagFunctionType     : return "FunctionType";
    case SymTagPointerType      : return "PointerType";
    case SymTagArrayType        : return "ArrayType";
    case SymTagBaseType         : return "BaseType";
    }

    return std::string();
}

// translated to cppobj's kind concept
enum class cppobj::kind_t symtagenum::tokind( enum SymTagEnum val )
{
    switch ( val )
    {
    case SymTagBaseType         : return cppobj::kind_t::base_;
    case SymTagEnum             : return cppobj::kind_t::enum_;
    case SymTagUDT              : return cppobj::kind_t::class_;
    case SymTagFunctionType     : return cppobj::kind_t::function_;
    case SymTagPointerType      : return cppobj::kind_t::pointer_;
    case SymTagArrayType        : return cppobj::kind_t::array_;
    }

    return cppobj::kind_t::unknown_;
}

} // namespace util
