// General C++ <--> Lua interop functions

#include "stdafx.h"

#include "bridge.h"

#include "luaexts.h"
#include "dbgeng-common.h"
#include "dbgeng-control.h"
#include "dbgeng-symbols.h"
#include "dbgeng-breakpoint.h"
#include "dbgeng-symbolgroup.h"
#include "dbgeng-extremotedata.h"
#include "cppobj.h"
#include "cppobj_impl.h"
#include "comwrappers.h"
#include "util.h"

#include <iostream>
#include <sstream>
#include <memory>
#include <filesystem>
#include <regex>

#include <lua.hpp>

#define CPPOBJ_EXPIRED "cppobj has expired"

namespace {
    char const * const main_library = EXTNAME ".lua";
    char const * const luaexts_conf = EXTNAME ".conf";
    char const * const name_space   = EXTNAME;

    inline std::string init_error( std::string scope, std::string msg )
    {
        return scope + " error: " + msg;
    }

    // for the registered_commands cmdspec on top of the stack, finds and
    // returns the corresponding ExtCommandDesc object, creating one if necessary
    std::shared_ptr< ExtCommandDesc > get_cmdspec_command_description( lua_State * L )
    {
        auto token = EXT_CLASS::cmd_token_t();
        std::shared_ptr< ExtCommandDesc > pecd;

        lua_getfield( L, -1, "command_token" );
        if ( lua_islightuserdata( L, -1 ) )
        {
            token = lua_touserdata( L, -1 );
            pecd = EXT_CLASS::inst().get_command_description( token );
        }

        lua_pop( L, 1 );

        if ( ! pecd ) // ExtCommandDesc not yet registered or it became unregistered
        {
            lua_getfield( L, -1, "extension" ); // get extension specification, so we can get its name
            lua_getfield( L, -1, "extension_name" );
            std::string const extname = lua_tostring( L, -1 );
            lua_pop( L, 2 );

            lua_getfield( L, -1, "command_name" );
            std::string const cmdname = lua_tostring( L, -1 );
            lua_pop( L, 1 );

            lua_getfield( L, -1, "command_description" );
            std::string const cmddesc = lua_tostring( L, -1 );
            lua_pop( L, 1 );

            lua_getfield( L, -1, "command_line_arguments" );
            std::string const cmdargs = lua_tostring( L, -1 );
            lua_pop( L, 1 );

            token = EXT_CLASS::inst().register_command_description( extname, cmdname, cmddesc, cmdargs );
            lua_pushlightuserdata( L, token );
            lua_setfield( L, -2, "command_token" );

            pecd = EXT_CLASS::inst().get_command_description( token );
            if ( ! pecd )
                throw std::runtime_error( "command description creation failed" );
        }

        return pecd;
    }
}

// Types and functions for bridging C++ and Lua
namespace bridge
{

    // for each value on the stack:
    //  converts it, in place, to a string using tostring()
    //  prints the result to the debugger console
    //  pops the string
    // the stack is emptied by this function
    int my_print( lua_State * L )
    {
        int const count = lua_gettop( L );

        if ( 0 == count )
            return 0;

        lua_getglobal( L, "tostring" );

        for ( int i = 1; i <= count; ++i )
        {
            lua_pushvalue( L, -1 ); // copy of 'tostring'
            lua_pushvalue( L, i );
            if ( lua_pcall( L, 1, 1, 0 ) )
                return luaL_error( L, "error calling 'tostring': %s", lua_tostring( L, -1 ) );

            EXT_CLASS::inst().Out( "%s", lua_tostring( L, -1 ) );
            lua_pop( L, 1 );
        }

        return 0;
    }

    // just like my_print above, except outputs using the DML outputter
    int dml_print( lua_State * L )
    {
        int const count = lua_gettop( L );

        if ( 0 == count )
            return 0;

        lua_getglobal( L, "tostring" );

        for ( int i = 1; i <= count; ++i )
        {
            lua_pushvalue( L, -1 ); // copy of 'tostring'
            lua_pushvalue( L, i );
            if ( lua_pcall( L, 1, 1, 0 ) )
                return luaL_error( L, "error calling 'tostring': %s", lua_tostring( L, -1 ) );

            EXT_CLASS::inst().Dml( "%s", lua_tostring( L, -1 ) );
            lua_pop( L, 1 );
        }

        return 0;
    }

    // Wrap a lua_State in RAII
    lua::lua()
        : L( luaL_newstate(), lua_close )
    {
        luaL_openlibs( L.get() );
    }

    // init a Lua!
    lua_inited init()
    {
        namespace filesystem = util::filesystem;

        lua L;

        filesystem::path const ext_subdir( EXTNAME );
        filesystem::path const home_path = util::get_home_path();
        filesystem::path const current_dir( "." );

        // look for the main library in this order:
        //   1. dll load path/luaexts
        //   2. dll load path
        std::deque< filesystem::path > search_paths;
        search_paths.push_back( util::get_dll_path().parent_path() / ext_subdir );
        search_paths.push_back( util::get_dll_path().parent_path() );

        filesystem::path library_path;
        filesystem::path main = main_library;
        for ( auto const & path : search_paths )
        {
            if ( path.empty() || ! filesystem::exists( path ) )
                continue;

            if ( filesystem::exists( path / main ) )
            {
                library_path = path / main;
                break;
            }
        }
        if ( library_path.empty() )
            return init_error( main_library, "couldn't find file" );

        if ( luaL_loadfile( L, library_path.string().c_str() ) )
            return init_error( main_library, util::make_string() << "loading failed: " << lua_tostring( L, -1 ) );

        // pass extname to the main library
        lua_pushliteral( L, EXTNAME );

        // pass the extension version as an array
        lua_createtable( L, 2, 0 );
        lua_pushinteger( L, luaexts::major_version );
        lua_seti( L, -2, 1 );
        lua_pushinteger( L, luaexts::minor_version );
        lua_seti( L, -2, 2 );
        lua_pushinteger( L, luaexts::patch_version );
        lua_seti( L, -2, 3 );

        // call the main library with an array of the search paths:
        //   1. dll load path
        //   2. user's home
        //   3. current dir
        // the initialization routine will also append luaexts to the end of each
        search_paths.pop_front();
        search_paths.push_back( home_path );
        search_paths.push_back( current_dir );
        lua_newtable( L );
        size_t idx = 1;
        for ( auto const & path : search_paths )
        {
            lua_pushinteger( L, idx++ );
            lua_pushstring( L, path.string().c_str() );
            lua_settable( L, -3 );
        }

        if ( lua_pcall( L, 3, 1, 0 ) )
            return init_error( main_library, util::make_string() << "initializing failed: " << lua_tostring( L, -1 ) );

        // main library should return a table, like a proper module
        if ( ! lua_istable( L, -1 ) )
            return init_error( main_library, "return value should be a table" );

        // copy the table value so we can use it after setglobal
        lua_pushvalue( L, -1 );

        // name our library's table
        lua_setglobal( L, name_space );

        // now call the initialize function to set everything up
        lua_getfield( L, -1, "initialize" );
        if ( lua_pcall( L, 0, 0, 0 ) )
            return init_error( EXTNAME ".initialize()", util::make_string() << lua_tostring( L, -1 ) );

        // register functions and metamethods
        dbgeng::xlua_init( L );
        dbgeng::control::xlua_init( L );
        dbgeng::symbols::xlua_init( L );
        dbgeng::breakpoint::xlua_init( L );
        dbgeng::symbolgroup::xlua_init( L );
        dbgeng::extremotedata::xlua_init( L );

        // redirect print for easy output
        // don't do this any earlier because we don't want any Lua init code to try printing and having
        // that sent to ExtExtension::Out() until EXT_CLASS::Initialize() returns, or bad things happen
        lua_pushcfunction( L, my_print );
        lua_setglobal( L, "print" );
        lua_pushcfunction( L, dml_print );
        lua_setglobal( L, "dml_print" );

        lua_settop( L, 0 );

        return L;
    }

    struct save_stack_depth
    {
        lua_State * const L;
        int const depth;
        explicit save_stack_depth( lua_State * L ) : L( L ), depth( lua_gettop( L ) ) {}
        ~save_stack_depth() { lua_settop( L, depth ); }
    };

    // run some Lua code
    // on failure, returns the error message
    std::string run( lua_State * L, std::string const & code )
    {
        save_stack_depth ssd( L );
        std::string err;

        bool const okay = LUA_OK == luaL_dostring( L, code.c_str() );

        if ( ! okay )
            err = lua_tostring( L, -1 );

        return err;
    }

    // run a registered command
    // on failure, returns the error message
    std::string run_registered_command( lua_State * L, std::string const & command_line )
    {
        save_stack_depth ssd( L );

        // pull out the first whitespace-delimited word as the command name
        std::string command, remainder;
        {
            std::smatch matches;
            if ( std::regex_search( command_line, matches, std::regex( "^\\s*(\\S+)\\s*(.*)" ) ) )
            {
                command = matches.str( 1 );
                remainder = matches.str( 2 );
            }
        }

        // commands are registered with entries in the 'registered_commands' table, so we can look them up directly

        lua_getglobal( L, EXTNAME );
        if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
            return "'" EXTNAME "' table not found";

        lua_getfield( L, -1, "registered_commands" );
        if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
            return "'" EXTNAME ".registered_commands' table not found";

        // find the command
        lua_getfield( L, -1, command.c_str() );
        if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
            return util::make_string() << "registered command not found: '" << command << "'";

        // now we've got the cmdspec on top of the stack. get the command's ExtCommandDesc object to parse the command line
        auto pecd = get_cmdspec_command_description( L );

        // parse the command line into arguments
        g_Ext->ParseArgs( pecd.get(), remainder.c_str() );

        // translate the arguments from the command line into an argument table to pass to the handler
        // arguments can be optional, so every argument in the ExtCommandDesc won't necessarily be present

        lua_createtable( L, pecd->m_NumUnnamedArgs, ( pecd->m_NumArgs - pecd->m_NumUnnamedArgs ) );

        typedef decltype( pecd->m_NumArgs ) index_t;
        index_t const count = pecd->m_NumArgs;
        index_t unnamedidx = -1;
        auto arg = pecd->m_Args;
        for ( index_t idx = 0; idx < count; ++idx, ++arg )
        {
            bool const unnamed  = nullptr == arg->Name;
            if ( unnamed )
                unnamedidx++;

            if ( ! arg->Present )
                continue;

            auto argerr = [ arg, unnamed, unnamedidx ]( std::string const & msg ) -> std::string
            {
                return util::make_string()
                    << msg << " for " << ( unnamed ? "unnamed" : "named" ) << " argument "
                    << ( unnamed ? ( util::make_string() << unnamedidx ) : ( util::make_string() << '\'' << arg->Name << '\'' ) ).str();
            };

            // push the arg's key
            if ( unnamed )
                lua_pushinteger( L, static_cast< lua_Integer >( unnamedidx + 1 ) ); // because Lua arrays are 1-based
            else
                lua_pushstring( L, arg->Name );

            // push the value according to its type
            if ( arg->Boolean )
            {
                lua_pushboolean( L, true );
            }
            else
            {
                // find the arg's value
                decltype( & g_Ext->m_Args[ 0 ] ) argval = nullptr;
                if ( unnamed )
                    argval = g_Ext->m_Args + unnamedidx;
                else
                    argval = g_Ext->FindArg( arg->Name, false );

                if ( arg->String )
                {
                    lua_pushstring( L, argval->StrVal );
                }
                else if ( arg->Expression )
                {
                    bool failed = true;
                    if ( arg->ExpressionSigned )
                    {
                        typedef std::make_signed< decltype( argval->NumVal ) >::type signed_t;
                        signed_t const sval = util::sign_cast< signed_t >( argval->NumVal, ( arg->ExpressionBits / 8 ) );
                        if ( std::numeric_limits< lua_Integer >::min() <= sval && sval <= std::numeric_limits< lua_Integer >::max() )
                        {
                            failed = false;
                            lua_pushinteger( L, static_cast< lua_Integer >( sval ) );
                        }
                    }
                    else if ( argval->NumVal <= std::numeric_limits< lua_Unsigned >::max() )
                    {
                        failed = false;
                        lua_pushinteger( L, static_cast< lua_Integer >( argval->NumVal ) );
                    }

                    if ( failed )
                        argerr( "number too large for safe conversion to Lua" );
                }
                else
                {
                    // new argument type?
                    return argerr( "unsupported argument type" );
                }
            }

            // add this argument to the argument table
            lua_settable( L, -3 );
        }

        // get cmdspec handler, move it to proper position on the stack, and call it
        lua_getfield( L, -2, "handler" );
        lua_insert( L, -3 );

        if ( lua_pcall( L, 2, 0, 0 ) )
            return lua_tostring( L, -1 );

        return std::string();
    }

    // show help for a registered command, or all commands if 'command' is empty
    // on failure, returns the error message
    std::string help_registered_command( lua_State * L, std::string const & command )
    {
        save_stack_depth ssd( L );

        if ( command.empty() )
        {
            lua_getglobal( L, EXTNAME );
            if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
                return "'" EXTNAME "' table not found";

            lua_getfield( L, -1, "help_listing" );
            if ( lua_isnil( L, -1 ) || ! lua_isfunction( L, -1 ) )
                return "'" EXTNAME ".help_listing' function not found";

            if ( lua_pcall( L, 0, 0, 0 ) )
                return lua_tostring( L, -1 );
        }
        else
        {
            // show help for just the requested command
            // commands are registered with entries in the 'registered_commands' table, so we can look them up directly
            lua_getglobal( L, EXTNAME );
            if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
                return "'" EXTNAME "' table not found";

            lua_getfield( L, -1, "registered_commands" );
            if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
                return "'" EXTNAME ".registered_commands' table not found";

            // find the command
            lua_getfield( L, -1, command.c_str() );
            if ( lua_isnil( L, -1 ) || ! lua_istable( L, -1 ) )
                return util::make_string() << "registered command not found: '" << command << "'";

            // now we've got the cmdspec on top of the stack. get the command's ExtCommandDesc object to send to the help routine
            auto pecd = get_cmdspec_command_description( L );

            g_Ext->HelpCommand( pecd.get() );
        }

        return std::string();
    }

} // namespace bridge
