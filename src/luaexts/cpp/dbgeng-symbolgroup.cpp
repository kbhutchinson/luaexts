#include "stdafx.h"

#include "dbgeng-symbolgroup.h"
#include "dbgeng-common.h"
#include "util.h"
#include <lua.hpp>

namespace
{
    char const * const symbolgroup_typename = "dbgeng.symbolgroup";
}

// Lua projections for the IDebugSymbolGroup2 interface
namespace dbgeng { namespace symbolgroup
{
    // [-0,+0]
    // if the value at the given index is a symbolgroup userdata, returns the associated
    // IDebugSymbolGroup2 address. else reports error.
    IDebugSymbolGroup2 * xlua_check_symbolgroup( lua_State * L, int arg )
    {
        auto sgptr = reinterpret_cast< IDebugSymbolGroup2 ** >( luaL_checkudata( L, arg, symbolgroup_typename ) );

        if ( nullptr == ( * sgptr ) )
            luaL_error( L, "%s has expired", symbolgroup_typename );

        return * sgptr;
    }

    // [-0,+1]
    // pushes:
    //   new userdata representing a symbolgroup, with associated metatable
    void xlua_push_symbolgroup( lua_State * L, IDebugSymbolGroup2 * sg )
    {
        auto paddr = reinterpret_cast< IDebugSymbolGroup2 ** >( lua_newuserdata( L, sizeof( IDebugSymbolGroup2 * ) ) );
        if ( ! paddr )
            luaL_error( L, "%s allocation failed", symbolgroup_typename );

        *paddr = sg;

        if ( luaL_getmetatable( L, symbolgroup_typename ) )
            lua_setmetatable( L, -2 );
        else
            luaL_error( L, "no metatable registered for %s", symbolgroup_typename );
    }

    // add_symbol( sg, symexpr, index )
    // adds a symbol to a symbol group
    // params:
    //   sg: symbol group to add the symbol to
    //   symexpr: string expression evaluated to determine the symbol's type
    //   index: specifies the desired index of the new symbol. use 'dbgeng.ANY_ID' to append
    //     to the end of the group.
    // returns:
    //   index of the new symbol
    int l_add_symbol( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto symexpr = luaL_checkstring( L, 2 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 3 ) );

        HRESULT hr = sg->AddSymbol( symexpr, &index );
        if ( S_OK == hr ) // S_FALSE does not mean partial success in this case, so we need to be specific
        {
            // GetSymbolEntryInformation() won't always work correctly right after a call to AddSymbol() unless
            // GetNumberSymbols() is called first, so let's do it pre-emptively
            {
                ULONG count = 0;
                sg->GetNumberSymbols( &count );
            }

            lua_pushinteger( L, index );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // expand_symbol( sg, index, expand )
    // adds or removes a symbol's children from the symbol group
    // params:
    //   sg: symbol group to expand or collapse
    //   index: index of the symbol to expand or collapse
    //   expand: boolean indicating desired operation; true for expand, false for collapse
    // returns:
    //   true if expansion succeeded.
    //   false if the symbol has no children.
    //   nil, 'E_INVALIDARG', and the code dbgeng.hr.E_INVALIDARG if the symbol is
    //   already at the maximum expansion depth, so it's children could not be added.
    int l_expand_symbol( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        BOOL expand = lua_toboolean( L, 3 );

        HRESULT hr = sg->ExpandSymbol( index, expand );
        if ( S_OK == hr)
        {
            lua_pushboolean( L, 1 );
            return 1;
        }
        // although the docs say S_FALSE is the response for "no children to add",
        // experience shows that it's actually E_FAIL. so we'll accept either.
        if ( S_FALSE == hr || E_FAIL == hr)
        {
            lua_pushboolean( L, 0 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_number_symbols( sg )
    // returns the number of symbols in a symbol group
    // params:
    //   sg: symbol group to retrieve the number of symbols for
    // returns:
    //   number of symbols in the group
    int l_get_number_symbols( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        ULONG count = 0;

        HRESULT hr = sg->GetNumberSymbols( &count );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, count );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_entry_information( sg, index )
    // returns information about a symbol in a symbol group
    // params:
    //   sg: symbol group to retrieve information from
    //   index: index of the symbol to retrieve information for
    // returns:
    //   symbol information, as a table of named fields:
    //     module: module base address
    //     offset: memory location of the symbol
    //     id: id of the symbol; if not known, will be equal to dbgeng.INVALID_OFFSET
    //     arg64: interpretation depends on the type of the symbol. if not known, will be 0.
    //     size: size, in bytes, of the symbol's value
    //     type_id: type id of the symbol
    //     name_size: size, in characters, of the symbol's name
    //     token: managed token of the symbol. if not known or symbol has no token, will be 0.
    //     tag: symbol tag of the symbol; will equal one of the values in the 'dbgeng.symtag' table
    //     arg32: interpretation of arg32 depends on the type of the symbol. currently, equals the
    //       register that holds the value or pointer to the value of the symbol. if the symbol is
    //       not held in a register or the register is not known, will be 0.
    int l_get_symbol_entry_information( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        DEBUG_SYMBOL_ENTRY dse = {};

        // symbol group seems to have some bugs. sometimes it will get wedged in a way that will prevent
        // the GetSymbolEntryInformation() call below to work properly or at all. calling GetNumberSymbols()
        // first seems to fix it, however, calling GetNumberSymbols() before every call to
        // GetSymbolEntryInformation() is really slow, so the GetNumberSymbols() calls have been placed in
        // the identified problem areas. unfortunately, it means that GetSymbolEntryInformation() may not
        // always be correct, since there may be other problem areas not yet identified.
        // so far the following broken scenarios are known:
        //   - calling GetSymbolTypeName() for index 0
        //   - calling GetSymbolEntryInformation() directly after AddSymbol()

        HRESULT hr = sg->GetSymbolEntryInformation( index, &dse );
        if ( SUCCEEDED( hr ) )
        {
            lua_createtable( L, 0, 10 );
            lua_pushinteger( L, dse.ModuleBase );
            lua_setfield( L, -2, "module" );
            lua_pushinteger( L, dse.Offset );
            lua_setfield( L, -2, "offset" );
            lua_pushinteger( L, dse.Id );
            lua_setfield( L, -2, "id" );
            lua_pushinteger( L, dse.Arg64 );
            lua_setfield( L, -2, "arg64" );
            lua_pushinteger( L, dse.Size );
            lua_setfield( L, -2, "size" );
            lua_pushinteger( L, dse.TypeId );
            lua_setfield( L, -2, "type_id" );
            lua_pushinteger( L, dse.NameSize );
            lua_setfield( L, -2, "name_size" );
            lua_pushinteger( L, dse.Token );
            lua_setfield( L, -2, "token" );
            lua_pushinteger( L, dse.Tag );
            lua_setfield( L, -2, "tag" );
            lua_pushinteger( L, dse.Arg32 );
            lua_setfield( L, -2, "arg32" );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_name( sg, index )
    // returns the name of a symbol in a symbol group
    // params:
    //   sg: symbol group to retrieve the name from
    //   index: index of the symbol to retrieve the name for
    // returns:
    //   name of the symbol
    int l_get_symbol_name( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        ULONG bufsize = 0;

        HRESULT hr = sg->GetSymbolName( index, nullptr, 0, &bufsize );
        if ( SUCCEEDED( hr ) )
        {
            util::charbuf buffer( bufsize + 1 ); // for null-terminator
            hr = sg->GetSymbolName( index, buffer, bufsize, nullptr );
            if ( SUCCEEDED( hr ) )
            {
                lua_pushstring( L, buffer );
                return 1;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_offset( sg, index )
    // returns the location in the target's memory of a symbol in a symbol group, if the
    // symbol has an absolute address
    // params:
    //   sg: symbol group to retrieve the name from
    //   index: index of the symbol to retrieve the offset for
    // returns:
    //   memory location of the symbol
    int l_get_symbol_offset( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        ULONG64 offset = 0;

        HRESULT hr = sg->GetSymbolOffset( index, &offset );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, offset );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_parameters( sg, start, count )
    // returns symbol parameters that describe the specified symbols from the symbol group
    // params:
    //   sg: symbol group to retrieve parameters from
    //   start: index in the symbol group of the first symbol to retrieve parameters for
    //   count: number of symbols to retrieve parameters for
    // returns:
    //   array of tables holding the requested symbols' parameters as named fields:
    //     module: module base address
    //     type_id: type id
    //     parent_symbol: index within the symbol group of the symbol's parent. equal to
    //       dbgeng.ANY_ID if symbol has no parent in the group.
    //     sub_elements: number of children of the symbol
    //     flags: bitfield combination of the values in the 'dbgeng.symbolgroup.symbol_flag' table
    //     expansion_depth: expansion depth of the symbol within the symbol group. the depth
    //       of a child symbol is always one more than the depth of its parent.
    int l_get_symbol_parameters( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        auto count = static_cast< ULONG >( luaL_checkinteger( L, 3 ) );

        util::buffer< DEBUG_SYMBOL_PARAMETERS > buffer{ count };

        HRESULT hr = sg->GetSymbolParameters( index, count, buffer );
        if ( SUCCEEDED( hr ) )
        {
            // return array
            lua_createtable( L, count, 0 );

            for ( ULONG i = 0; i < count; ++i )
            {
                auto & p = buffer[ i ];

                lua_pushinteger( L, i + 1 );
                lua_createtable( L, 0, 5 );

                lua_pushinteger( L, p.Module );
                lua_setfield( L, -2, "module" );
                lua_pushinteger( L, p.TypeId );
                lua_setfield( L, -2, "type_id" );
                lua_pushinteger( L, p.ParentSymbol );
                lua_setfield( L, -2, "parent_symbol" );
                lua_pushinteger( L, p.SubElements );
                lua_setfield( L, -2, "sub_elements" );
                lua_pushinteger( L, p.Flags );
                lua_setfield( L, -2, "flags" );
                lua_pushinteger( L, p.Flags & DEBUG_SYMBOL_EXPANSION_LEVEL_MASK );
                lua_setfield( L, -2, "expansion_depth" );

                lua_settable( L, -3 );
            }
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_register( sg, index )
    // returns the register that contains the value or a pointer to the value of a symbol in a symbol group
    // params:
    //   sg: symbol group to retrieve the register data from
    //   index: index of the symbol to retrieve the register data for
    // returns:
    //   index of the register containing the value or pointer to the value of the symbol
    int l_get_symbol_register( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        ULONG reg = 0;

        HRESULT hr = sg->GetSymbolRegister( index, &reg );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, reg );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_size( sg, index )
    // returns the size of a symbol's value
    // params:
    //   sg: symbol group to retrieve the size from
    //   index: index of the symbol to retrieve the size for
    // returns:
    //   size, in bytes, of the symbol's value
    int l_get_symbol_size( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        ULONG size = 0;

        HRESULT hr = sg->GetSymbolSize( index, &size );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushinteger( L, size );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_type_name( sg, index )
    // return the name of the specified symbol's type
    // params:
    //   sg: symbol group to retrieve the type name from
    //   index: index of the symbol to retrieve the type name for
    // returns:
    //   name of the symbol's type
    int l_get_symbol_type_name( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        ULONG bufsize = 0;

        HRESULT hr = sg->GetSymbolTypeName( index, nullptr, 0, &bufsize );
        if ( SUCCEEDED( hr ) )
        {
            util::charbuf buffer( bufsize + 1 ); // for null-terminator
            hr = sg->GetSymbolTypeName( index, buffer, bufsize, nullptr );
            if ( SUCCEEDED( hr ) )
            {
                // GetSymbolEntryInformation() won't always work correctly right after a call to
                // GetSymbolTypeName() for index 0 unless GetNumberSymbols() is called first,
                // so let's do it pre-emptively
                if ( 0 == index )
                {
                    ULONG count = 0;
                    sg->GetNumberSymbols( &count );
                }

                lua_pushstring( L, buffer );
                return 1;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // get_symbol_value_text( sg, index )
    // returns a string that represents the value of a symbol
    // params:
    //   sg: symbol group to retrieve the value text from
    //   index: index of the symbol to retrieve the value text for
    // returns:
    //   value of the symbol as a string
    int l_get_symbol_value_text( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        ULONG bufsize = 0;

        HRESULT hr = sg->GetSymbolValueText( index, nullptr, 0, &bufsize );
        if ( SUCCEEDED( hr ) )
        {
            util::charbuf buffer( bufsize + 1 ); // for null-terminator
            hr = sg->GetSymbolValueText( index, buffer, bufsize, nullptr );
            if ( SUCCEEDED( hr ) )
            {
                lua_pushstring( L, buffer );
                return 1;
            }
        }

        return xlua_push_hresult_error( L, hr );
    }

    // remove_symbol_by_index( sg, index )
    // removes specified symbol from the symbol group. child symbols cannot be removed, the
    // parent symbol must be removed.
    // params:
    //   sg: symbol group to remove the symbol from
    //   index: index of symbol to remove
    // returns:
    //   true if the removal succeeded
    int l_remove_symbol_by_index( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );

        HRESULT hr = sg->RemoveSymbolByIndex( index );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // remove_symbol_by_name( sg, name )
    // removes specified symbol from the symbol group. child symbols cannot be removed, the
    // parent symbol must be removed.
    // params:
    //   sg: symbol group to remove the symbol from
    //   name: name of symbol to remove
    // returns:
    //   true if the removal succeeded
    int l_remove_symbol_by_name( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto name = luaL_checkstring( L, 2 );

        HRESULT hr = sg->RemoveSymbolByName( name );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // write_symbol( sg, index, value )
    // sets the value of the specified symbol
    // params:
    //   sg: symbol group containing the symbol whose value to set
    //   index: index of the symbol within the symbol group
    //   value: string containing C++ expression to evaluate to set the new value
    // returns:
    //   true if the operation was successful
    int l_write_symbol( lua_State * L )
    {
        auto sg = xlua_check_symbolgroup( L, 1 );
        auto index = static_cast< ULONG >( luaL_checkinteger( L, 2 ) );
        auto expr  = luaL_checkstring( L, 3 );

        HRESULT hr = sg->WriteSymbol( index, expr );
        if ( SUCCEEDED( hr ) )
        {
            lua_pushboolean( L, 1 );
            return 1;
        }

        return xlua_push_hresult_error( L, hr );
    }

    // gc( sg )
    // __gc metamethod: cleans up after a symbolgroup
    // params:
    //   sg: symbolgroup to release
    // returns:
    //   nothing
    int gc( lua_State * L )
    {
        auto sgptr = reinterpret_cast< IDebugSymbolGroup2 ** >( luaL_checkudata( L, 1, symbolgroup_typename ) );

        if ( nullptr != ( * sgptr ) )
        {
            ( *sgptr )->Release();

            // reset the address the userdata points to, due to this:
            // http://blog.reverberate.org/2014/06/beware-of-lua-finalizers-in-c-modules.html
            *sgptr = nullptr;
        }

        return 0;
    }

    struct enum_mapping const symbol_flags[] =
    {
        { "EXPANDED"    , DEBUG_SYMBOL_EXPANDED    },
        { "READ_ONLY"   , DEBUG_SYMBOL_READ_ONLY   },
        { "IS_ARRAY"    , DEBUG_SYMBOL_IS_ARRAY    },
        { "IS_FLOAT"    , DEBUG_SYMBOL_IS_FLOAT    },
        { "IS_ARGUMENT" , DEBUG_SYMBOL_IS_ARGUMENT },
        { "IS_LOCAL"    , DEBUG_SYMBOL_IS_LOCAL    },
        { nullptr , 0 }
    };

    struct luaL_Reg const library_funcs[] =
    {
        { "add_symbol", l_add_symbol },
        { "expand_symbol", l_expand_symbol },
        { "get_number_symbols", l_get_number_symbols },
        { "get_symbol_entry_information", l_get_symbol_entry_information },
        { "get_symbol_name", l_get_symbol_name },
        { "get_symbol_offset", l_get_symbol_offset },
        { "get_symbol_parameters", l_get_symbol_parameters },
        { "get_symbol_register", l_get_symbol_register },
        { "get_symbol_size", l_get_symbol_size },
        { "get_symbol_type_name", l_get_symbol_type_name },
        { "get_symbol_value_text", l_get_symbol_value_text },
        { "remove_symbol_by_index", l_remove_symbol_by_index },
        { "remove_symbol_by_name", l_remove_symbol_by_name },
        { "write_symbol", l_write_symbol },
        { nullptr , nullptr },
    };

    // [-0,+0]
    // dbgeng::symbolgroup api init
    void xlua_init( lua_State * L )
    {
        xlua_get_dbgeng_subtable( L, "symbolgroup" );
        lua_pushvalue( L, -1 );
        luaL_setfuncs( L, library_funcs, 0 );

        // metatable used for wrapped objects
        if ( luaL_newmetatable( L, symbolgroup_typename ) )
        {
            lua_insert( L, -2 );
            lua_setfield( L, -2, "__index" );
            lua_pushcfunction( L, gc );
            lua_setfield( L, -2, "__gc" );
            lua_pop( L, 1 );
        }

        // setup the table of symbol flags
        luaL_getsubtable( L, -1, "symbol_flag" );
        xlua_set_enums( L, symbol_flags );
        lua_pop( L, 1 );

        // pop dbgeng.symbolgroup table
        lua_pop( L, 1 );
    }

}}
