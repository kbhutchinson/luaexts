
-- Stuff that gets passed in from the C++ side during initialization
local extname, VERSION, search_paths = ...

-- make it easy to avoid hard-coding the extension name
EXTNAME = extname

-- print with a newline; NB: this is global
function say( ... )
    print( ... )
    print( '\n' )
end


--[[ Version metamethods ]]--
local version_meta = {}
setmetatable( VERSION, version_meta )

function version_meta.__tostring( v )
    local s = v[ 1 ]
    for i = 2, #v, 1 do
        s = s .. '.' .. v[ i ]
    end

    return s
end

function version_meta.__eq( v1, v2 )
    local n = math.max( #v1, #v2 )

    -- nils pad out to insignificant zeroes
    for i = 1, n do
        local p1 = v1[ i ]
        local p2 = v2[ i ]

        p1 = ( 'nil' == type( p1 ) ) and 0 or p1
        p2 = ( 'nil' == type( p2 ) ) and 0 or p2

        if p1 ~= p2 then
            return false
        end
    end

    return true
end

function version_meta.__lt( v1, v2 )
    local n = math.max( #v1, #v2 )

    -- nils pad out to insignificant zeroes
    for i = 1, n do
        local p1 = v1[ i ]
        local p2 = v2[ i ]

        p1 = ( 'nil' == type( p1 ) ) and 0 or p1
        p2 = ( 'nil' == type( p2 ) ) and 0 or p2

        if p1 < p2 then
            return true
        elseif p1 > p2 then
            return false
        end
    end

    return false
end


-- path config values for easy reference
local path_config = {}
do
    for v in string.gmatch( package.config, '(.)\n' ) do
        table.insert( path_config, v )
    end

    path_config.dirsep  = path_config[ 1 ]
    path_config.pathsep = path_config[ 2 ]
    path_config.subchar = path_config[ 3 ]
    path_config.exechar = path_config[ 4 ]
    path_config.ignchar = path_config[ 5 ]

    if '\\' == path_config.dirsep then
        -- the path class being used on the C++ side uses forward slashes,
        -- so things look weird if we use back slashes in here
        path_config.dirsep = '/'
    end

    for i, _ in ipairs( path_config ) do
        path_config[ i ] = nil
    end
end


-- Simple file existence check
local function file_exists( filepath )
    local exists = false
    local fh = io.open( filepath )
    if fh then
        exists = true
        fh:close()
    end
    return exists
end


-- Convenient argument type checking
-- Params:
--   funcname - name of the function being checked
--   argspec - table with the argument specification
--     provide a number key for each argument to check,
--     the key's value should be a list with the desired
--     typestring and the argument itself.
--     to skip validating an argument, provide a value for
--     its key that is not a table.
--
-- Returns:
--   Never returns if arguments don't validate. Otherwise, returns true.
--
-- Example:
-- local function somefunc( filename, optional_arg, line_number )
--     arg_type_check( 'somefunc', {
--         { 'string', filename },
--         true, -- argument is optional, so don't check it
--         { 'string', line_number },
--     })
-- end
local function arg_type_check( funcname, argspec )
    for i, v in ipairs( argspec ) do
        if 'table' == type( v ) then
            local typestr = type( v[ 2 ] )
            if v[ 1 ] ~= typestr then
                error( string.format( 'bad argument #%d to \'%s\' (%s expected, got %s)', i, funcname, v[ 1 ], typestr ), 3 )
            end
        end
    end
    return true
end


-- Validate luaexts configuration settings
-- Params:
--   config - table holding the current configuration settings
--   filepath - path of the last config file loaded; if the current config doesn't
--     validate, this file was responsible for messing it up
local function validate_config( config, filepath )
    -- 'extensions' should be an array of strings: extension names
    for i, v in ipairs( config.extensions ) do
        local valtype = type( v )
        if 'string' ~= valtype then
            error( string.format( '%s: invalid value found in \'extensions\' list; all values should be strings, found a %s at index %d', filepath, valtype, i ), 0 )
        end
    end
end


-- Load configuration settings
-- Params:
--   extname - the extension name; each path in the search paths will be tried with this name
--     appended and without this name appended
--   filename - optional; configuration base file name that will be searched for in the search paths.
--     if not provided, the value passed in extname is appended with '.conf'
--   validator - optional; function to validate the current configuration after each file is executed.
--     validator will be passed the current config table and the most recent filename executed, and
--     should error if the configuration doesn't validate
--   search_paths: optional; list of paths to search in. if not provided, uses luaexts.config.search_paths
-- Returns:
--   The table resulting from executing each found config file.
local function load_configuration( extname, filename, validator, search_paths )
    arg_type_check( 'load_configuration', { { 'string', extname } } )

    local luaexts = _ENV[ EXTNAME ]

    if 'string' ~= type( filename ) then
        filename = extname .. '.conf'
    end
    if 'table' ~= type( search_paths ) then
        search_paths = nil
    end
    if not search_paths and luaexts and luaexts.config and luaexts.config.search_paths then
        search_paths = luaexts.config.search_paths
    end
    if not search_paths or 0 == #search_paths then
        error( 'no search paths defined' )
    end

    -- make a version of each search path with the extname appended for those who want to throw
    -- everything into a specific sub-directory
    do
        local fixed_up = {}
        for _, path in ipairs( search_paths ) do
            table.insert( fixed_up, path .. path_config.dirsep .. extname )
            table.insert( fixed_up, path )
        end
        search_paths = fixed_up
    end

    -- the order of search_paths should be from most general path to most specific,
    -- so we just run through each, and if there's a config file there we execute it.
    -- in this way, specific config settings will override general ones.

    -- allow access to the environment, but save new values set by the config chunk in a table
    -- this makes it easy for us to capture the values being set in the config files
    local config = setmetatable( {}, { __index = _ENV } )
    for _, path in ipairs( search_paths ) do
        local filepath = path .. path_config.dirsep .. filename
        say( 'checking ' .. filepath )
        if file_exists( filepath ) then
            say( 'loading ' .. filepath )
            local chunk = assert( loadfile( filepath, 'bt', config ) )
            -- pass the environment to the config chunk in case someone wants to inject things into it
            -- instead of the config table
            chunk( _ENV )
            -- if we validate after each config is executed, we'll know which one
            -- broke things when it doesn't validate
            if validator then
                validator( config, filepath )
            end
        end
    end -- for search_paths

    setmetatable( config, nil ) -- no longer needed

    -- and let's put these in, too, if something's not already there
    if nil == config.search_paths then
        config.search_paths = search_paths
    end

    -- extensions that build paths might want these
    if nil == config.path_config then
        config.path_config = path_config
    end

    return config
end


local registered_extensions = {}
local registered_commands   = {}
local last_extension_registered = nil


-- Allows an extension to supply extra information about itself
-- Params:
--   extspec - an extension specification record
-- Returns:
--   Nothing
local function register_extension( extspec )
    -- validate the 'extension_name' field
    local extname = extspec.extension_name
    if not extname then
        error( "'extension_name' field required", 2 )
    end
    if 'string' ~= type( extname ) or 0 == #extname then
        error( "'extension_name' field invalid", 2 )
    end
    if extname:find( '.', 1, true ) then
        error( "'extension_name' field cannot contain a dot (.)", 2 )
    end

    -- validate the 'version' field
    local extension_version = extspec.version
    local extension_version_type = type( extension_version )
    if 'nil' ~= extension_version_type then
        if 'table' ~= extension_version_type then
            error( "'extension_version' field must be an array of integers, if specified", 2 )
        end

        setmetatable( extension_version, version_meta )
    end

    -- validate the 'required_framework_version' field
    local required_framework_version = extspec.required_framework_version
    local required_version_type = type( required_framework_version )
    if 'nil' ~= required_version_type then
        if 'table' ~= required_version_type then
            error( "'required_framework_version' field must be an array of integers, if specified", 2 )
        end

        setmetatable( required_framework_version, version_meta )
        if VERSION < required_framework_version then
            error( string.format( "%s version %s is less than required version %s", EXTNAME, VERSION, required_framework_version ), 2 )
        end
    end

    -- validate the 'commands' field
    if extspec.commands then
        local cmdnames = {}
        for i, cmdspec in ipairs( extspec.commands ) do
            local cmdname = cmdspec.command_name
            if not cmdname then
                error( string.format( "'command_name' field required in command entry #%d", i ), 2 )
            end
            if 'string' ~= type( cmdname ) or #cmdname == 0 then
                error( string.format( "'command_name' field invalid in command entry #%d", i ), 2 )
            end
            if cmdname:find( '.', 1, true ) then
                error( string.format( "'command_name' field cannot contain a dot (.), in command entry #%d", i ), 2 )
            end

            local descrip = cmdspec.command_description
            if not descrip then
                error( string.format( "'command_description' field required in command entry #%d", i ), 2 )
            end
            if 'string' ~= type( descrip ) then
                error( string.format( "'command_description' field invalid in command entry #%d", i ), 2 )
            end

            local args = cmdspec.command_line_arguments
            if not args then
                error( string.format( "'command_line_arguments' field required in command entry #%d", i ), 2 )
            end
            if 'string' ~= type( args ) then
                error( string.format( "'command_line_arguments' field invalid in command entry #%d", i ), 2 )
            end

            local handler = cmdspec.handler
            if not handler then
                error( string.format( "'handler' field required in command entry #%d", i ), 2 )
            end

            -- prevent two commands with the same name in one extension
            if cmdnames[ cmdname ] then
                error( string.format( "two commands cannot use the same name; collision found with command entries #%d and #%d", cmdnames[ cmdname ], i ), 2 )
            end

            cmdnames[ cmdname ] = i
        end
    end

    -- extspec itself validated, but we need to make sure there are no collisions with other extensions
    if registered_extensions[ extname ] then
        error( string.format( "an extension using the name '%s' has already been registered, loaded from %s", extname, registered_extensions[ extname ].file ), 2 )
    end

    -- validation succeeded
    extspec.file = debug.getinfo( 2, 'S' ).short_src
    registered_extensions[ extname ] = extspec
    table.insert( registered_extensions, 1, extspec ) -- keep track of the order of registration, too
    last_extension_registered = extspec

    -- commands get registered as both a fully qualified name (extname.cmdname) and as a short name (cmdname), with extensions loaded later overriding
    -- the short names of those loaded earlier, similar to the way windbg's extension search chain works
    -- this allows convenient access to the last command registered with a given name, while still allowing access to other commands with the same
    -- name by using the fully qualified name
    if extspec.commands then
        for _, cmdspec in ipairs( extspec.commands ) do
            local cmdname = cmdspec.command_name
            local fqcmdname = extname .. '.' .. cmdname
            registered_commands[ fqcmdname ] = cmdspec
            registered_commands[ cmdname   ] = cmdspec

            -- while we're at it, make sure we can tell which extension a command is part of
            cmdspec.extension = extspec
        end
    end

end


-- Outputs help for all registered commands
local function help_listing()
    local commands = {}
    local lengths = { name = 0, description = 0 }
    for _, extension in ipairs( registered_extensions ) do
        local extname = extension.extension_name
        for _, command in ipairs( extension.commands ) do
            -- if this command can be called as its shortname, show that the extension name is optional
            local shortname = command.command_name
            local longname  = extname .. '.' .. shortname
            local has_shortname = registered_commands[ shortname ] == registered_commands[ longname ]
            local displayname
            if has_shortname then
                --displayname = '[' .. extname .. '.' .. ']' .. shortname
                displayname = '* ' .. extname .. '.' .. shortname
            else
                displayname = '  ' .. extname .. '.' .. shortname
            end

            local description = command.command_description

            -- just note them for now, so we can process them below
            commands[ #commands + 1 ] =
            {
                name = displayname,
                description = description,
            }
            if lengths.name < #displayname then
                lengths.name = #displayname
            end
            if description and lengths.description < #description then
                lengths.description = #description
            end
        end
    end

    local helpmsg =
[[
The following commands can be run with the !luacmd extension command. The syntax is:
  !luacmd <command> <arguments>
In the listing below, <command> is shown as <extname>.<cmdname>, where <extname> is the
name that the extension used to register itself with %s. Entries with a leading star
can be run using just <cmdname>, ie without <extname> and dot prefix.
]]

    say( string.format( helpmsg, EXTNAME ) )

    for _, command in ipairs( commands ) do
        local name, description = command.name, command.description
        say( string.format( '  %s%s - %s', name, string.rep( ' ', lengths.name - #name ), description ) )
    end

    say
[[

To get more detailed help for a command, including its supported arguments, type:
  !luahelp <command>
]]

end


-- Calls the handler for a registered command
-- Params:
--   cmdname - name of the command, either short or fully qualified
-- Returns:
--   Whatever the handler returns, if found. Otherwise, errors.
local function call_command_handler( cmdname, ... )
    arg_type_check( 'call_command_handler', { { 'string', cmdname } } )

    if not registered_commands[ cmdname ] then
        error( string.format( "command '%s' not found", cmdname ), 0 )
    end

    return registered_commands[ cmdname ].handler( ... )
end


-- Load submodules belonging to luaexts. Submodules will be loaded into tables in the global luaexts table.
-- Additionally, luaexts is generally configured out of the box to load submodules into global tables, as well.
-- Params:
--   none
-- Returns:
--   nothing
local function load_submodules()
    local luaexts = _ENV[ EXTNAME ]

    local submods = {
        'event',  -- eventing subsystem
        'cppobj', -- project C++ objects into Lua
    }

    for i, modname in ipairs( submods ) do
        local submod = require( modname )

        -- be careful about name clashes
        if luaexts[ modname ] then
            error( string.format( '%s.%s is already filled', EXTNAME, modname ) )
        end
        luaexts[ modname ] = submod

        if luaexts.config.load_submodule_globally[ modname ] then
            if _ENV[ modname ] then
                error( string.format( '_ENV.%s is already filled', modname ) )
            end
            _ENV[ modname ] = submod
        end
    end
end


-- this is the main initialization routine
local function initialize()
    local luaexts = _ENV[ EXTNAME ]

    -- replace package.path and package.cpath with our own search paths so require will work
    package.path = ''
    for _, path in ipairs( luaexts.config.search_paths ) do
        if #package.path > 0 then
            package.path = package.path .. path_config.pathsep
        end
        local template = path .. path_config.dirsep .. path_config.subchar
        template = ( template .. path_config.dirsep .. path_config.subchar .. '.lua' ) .. path_config.pathsep .. ( template .. '.lua' )
        package.path = package.path .. template
    end
    package.cpath = string.gsub( package.path, ( '%.lua%f[' .. path_config.pathsep .. '\0]' ), '.dll' )

    -- load luaexts-owned submodules
    load_submodules()

    -- these extensions get 'require'd and saved in a global variable with the same name
    for _, extname in ipairs( luaexts.config.extensions ) do
        last_extension_registered = nil
        local extlib = require( extname )

        -- if the extension registered itself, place its library table in a global using the registered name
        if last_extension_registered then
            extname = last_extension_registered.extension_name
        end

        if _ENV[ extname ] then
            error( string.format( "an extension has already been stored in the global '%s', proceeding would overwrite it", extname ) )
        end

        _ENV[ extname ] = extlib

        last_extension_registered = nil
    end
end


return
{
    version = VERSION,

    load_configuration = load_configuration,
    file_exists = file_exists,
    arg_type_check = arg_type_check,
    registered_extensions = registered_extensions,
    registered_commands = registered_commands,
    register_extension = register_extension,
    call_command_handler = call_command_handler,
    help_listing = help_listing,
    initialize = initialize,

    -- config settings
    config = load_configuration( extname, extname .. '.conf', validate_config, search_paths ),
}
