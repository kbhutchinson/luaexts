#pragma once

struct lua_State;

// Lua projections for the IDebugControl interface
namespace dbgeng { namespace control
{
    // add_assembly_options( options )
    // turns on some of the assembly and disassembly options. the given options will be added to the
    // existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.asmopt' table
    // returns:
    //   true if successful
    int l_add_assembly_options( lua_State * L );

    // add_breakpoint( type, id )
    // adds a breakpoint. type can be one of:
    //   - dbgeng.breakpoint.type.CODE: software breakpoint
    //   - dbgeng.breakpoint.type.DATA: processor breakpoint
    // params:
    //   type: breakpoint type
    //   id  : breakpoint id to use. use 'dbgeng.ANY_ID' to have the engine assign the id.
    // returns:
    //   new userdata representing a breakpoint
    int l_add_breakpoint( lua_State * L );

    // add_engine_options( options )
    // turns on some of the debugger engine's options. the given options will be added to the existing
    // options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.engopt' table.
    // returns:
    //   true if successful
    int l_add_engine_options( lua_State * L );

    // add_extension( path )
    // loads an extension library into the debugger engine. if the extension library is already
    // loaded, simply returns the handle. the library will not be loaded again.
    // params:
    //   path: fully qualified path to the extension library to load
    // returns:
    //   handle to the loaded extension library
    int l_add_extension( lua_State * L );

    // call_extension( handle, function, argstr )
    // calls a debugger extension
    // params:
    //   handle: handle to the extension library containing the extension to call,
    //     returned by add_extension. if set to 0, the engine will walk the extension
    //     library chain searching for the extension.
    //   function: string name of the extension to call
    //   argstr: string that will be parsed into arguments by the extension as if
    //     from the command prompt
    // returns:
    //   true if successful
    int l_call_extension( lua_State * L );

    // disassemble( offset, flags )
    // disassembles a processor instruction in the target's memory
    // params:
    //   offset: location in the target's memory of the instruction to disassemble
    //   flags: bitfield of flags that affect the behavior of this function. currently, the
    //     only flag that can be set is dbgeng.control.disasm_flag.EFFECTIVE_ADDRESS. when set,
    //     the engine will compute and display the effective address from the current registers
    //     information.
    // returns:
    //   disassembled instruction as a string
    //   location in the target's memory of the instruction following the disassembled instruction
    int l_disassemble( lua_State * L );

    // execute( outctl, command, flags )
    // executes the specified debugger commands, as if typed at the command prompt
    // params:
    //   outctl: a combination of values from the 'dbgeng.outctl' table. for more
    //     information, see: https://msdn.microsoft.com/en-us/library/ff541517.aspx
    //   command: command string to execute. may contain multiple commands.
    //   flags: bitfield combination of values from the 'dbgeng.control.execute_flag' table:
    //     ECHO: command string is send to output
    //     NOT_LOGGED: command string is not logged; this is overridden by ECHO
    //     NO_REPEAT: if command string is empty, do not repeat the last command, and do not
    //       save the current command string for repeat execution later
    // returns:
    //   true if successful
    int l_execute( lua_State * L );

    // get_assembly_options()
    // returns the assembly and disassembly options that affect how the debugger engine assembles
    // and disassembles processor instructions for the target
    // returns:
    //   bitfield containing the assembly options, which is a combination of the values in the
    //   'dbgeng.control.asmopt' table. for more information on their meanings, see:
    //   https://msdn.microsoft.com/en-us/library/ff541443.aspx
    int l_get_assembly_options( lua_State * L );

    // get_breakpoint_by_id( id )
    // returns the breakpoint with the specified breakpoint id
    // params:
    //   id: breakpoint id of the breakpoint to return
    // returns
    //   breakpoint with the given id
    int l_get_breakpoint_by_id( lua_State * L );

    // get_breakpoint_by_index( index )
    // returns the breakpoint located at the specified index
    // params:
    //   index: zero-based index of the breakpoint to return. this is specific to the current
    //     process. the value should be between zero and the total number of breakpoints minus
    //     one. the total number of breakpoints can be retrieved by calling get_number_breakpoints().
    // returns:
    //   breakpoint with the given index
    int l_get_breakpoint_by_index( lua_State * L );

    // get_code_level()
    // returns the current code level and is mainly used when stepping through code
    // returns:
    //   current code level, which can be one of the values from the 'dbgeng.control.level' table:
    //     SOURCE: when single stepping, the size of a step will be one source line
    //     ASSEMBLY: when single stepping, the size of a step will be a single processor instruction
    int l_get_code_level( lua_State * L );

    // get_engine_options()
    // returns the engine's options
    // returns:
    //   bitfield containing the engine options, which is a combination of the values in the
    //   'dbgeng.control.engopt' table. for more information on their meanings, see:
    //   https://msdn.microsoft.com/en-us/library/ff541475.aspx
    int l_get_engine_options( lua_State * L );

    // get_expression_syntax()
    // returns the current syntax that the engine is using for evaluating expressions
    // returns:
    //   one of the following values from the 'dbgeng.control.expr' table:
    //     MASM: expressions will be evaluated according to MASM syntax
    //     CPLUSPLUS: expressions will be evaluated according to C++ syntax
    int l_get_expression_syntax( lua_State * L );

    // get_expression_syntax_names( index )
    // returns the full and abbreviated names of an expression syntax
    // params:
    //   index: index of the expression syntax. this should be between zero and the number returned by
    //     get_number_expression_syntaxes() minus one.
    // returns:
    //   abbreviated name of the expression syntax
    //   full name of the expression syntax
    int l_get_expression_syntax_names( lua_State * L );

    // get_interrupt()
    // checks whether a user interrupt was issued. if a user interrupt was issued, it's cleared
    // when this function is called. examples of user interrupts include pressing Ctrl+C or pressing
    // the Stop button in a debugger. calling set_interrupt() also causes a user interrupt.
    // returns:
    //   true if an interrupt has been requested.
    //   false if an interrupt has not been requested.
    int l_get_interrupt( lua_State * L );

    // get_number_expression_syntaxes()
    // returns the number of expression syntaxes that are supported by the engine
    // returns:
    //   number of supported syntaxes
    int l_get_number_expression_syntaxes( lua_State * L );

    // get_stack_trace( frmoff, stkoff, instoff, numframes )
    // returns the frames at the top of the specified call stack
    // params:
    //   frmoff: memory offset of the stack frame at the top of the stack. if 0, the current frame
    //     pointer is used.
    //   stkoff: memory offset of the current stack. if 0, current stack pointer is used.
    //   instoff: memory offset of instruction of interest for the function at the top of the stack.
    //     if 0, current instruction pointer is used.
    //   numframes: number of frames to retrieve
    // returns:
    //   array containing the requested stack frames, as tables with named fields:
    //     instruction_offset: memory offset of the related instruction for the stack frame.
    //       typically, the return address for the next stack frame, or the current instruction 
    //       pointer if the frame is at the top of the stack.
    //     return_offset: memory offset of the return address for the stack frame. typically, the
    //       related instruction for the previous stack frame.
    //     frame_offset: memory offset of the stack frame, if known
    //     stack_offset: memory offset of the processor stack
    //     func_table_entry: memory offset of the function entry point for this frame, if available.
    //     params: array containing the first four stack slots passed to the function, if available.
    //     virtual: boolean, set to true if this stack frame was generated by the debugger during
    //       unwinding. set to false if it was formed from a thread's current context.
    //     frame_number: index of the frame from the top of the stack, which has index 0.
    int l_get_stack_trace( lua_State * L );

    // is_pointer_64bit()
    // returns:
    //   bool indicating whether the effective processor uses 64-bit pointers
    int l_is_pointer_64bit( lua_State * L );

    // output_disassembly_lines( outctl, previous_lines, total_lines, offset, flags )
    // disassembles several processor instructions and sends the resulting assembly instructions to
    // the output callbacks
    // params:
    //   outctl: combination of flags from DEBUG_OUTCTL_XXX that control output
    //   previous_lines: number of lines of instructions before *offset* to include in the output
    //   total_lines: number of lines of instructions to include in the output
    //   offset: memory location to disassemble, along with surrounding instructions depending on
    //      other arguments
    //   flags: bitfield that determines format of the output
    // returns:
    //   table containing the following fields:
    //     offset_line: line number in the output that contains the instruction at *offset*
    //     start_offset: memory location of the first instruction included in the output
    //     end_offset: memory location of the instruction after the last disassembled instruction
    //     line_offsets: array that contains the memory location of each instruction included in the
    //       output; if the output for an instruction spans multiple lines, the array element
    //       corresponding to the first line of output will contain the address of the instruction
    int l_output_disassembly_lines( lua_State * L );

    // remove_assembly_options( options )
    // turns off some of the assembly and disassembly options. the given options will be removed from
    // the existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.asmopt' table.
    // returns:
    //   true if successful
    int l_remove_assembly_options( lua_State * L );

    // remove_breakpoint( breakpoint )
    // removes a breakpoint
    // params:
    //   breakpoint: a breakpoint userdata
    // returns:
    //   true if successful
    int l_remove_breakpoint( lua_State * L );

    // remove_engine_options( options )
    // turns off some of the engine's options. the given options will be removed from the existing
    // options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.engopt' table.
    // returns:
    //   true if successful
    int l_remove_engine_options( lua_State * L );

    // remove_extension( handle )
    // unloads an extension library
    // params:
    //   handle: handle of the extension library to unload, returned from add_extension
    // returns
    //   true if successful
    int l_remove_extension( lua_State * L );

    // set_assembly_options( options )
    // changes the assembly and disassembly options. the given options will completely replace the
    // existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.asmopt' table.
    // returns:
    //   true if successful
    int l_set_assembly_options( lua_State * L );

    // set_code_level( level )
    // sets the current code level and is mainly used when stepping through code
    // params:
    //   level: one of the values from the 'dbgeng.control.level' table:
    //     SOURCE: when single stepping, the size of a step will be one source line
    //     ASSEMBLY: when single stepping, the size of a step will be a single processor instruction
    // returns:
    //   true if successful
    int l_set_code_level( lua_State * L );

    // set_engine_options( options )
    // changes the engine's options. the given options will completely replace the existing options.
    // params:
    //   options: bitfield containing a combination of values from the 'dbgeng.control.engopt' table.
    // returns:
    //   true if successful
    int l_set_engine_options( lua_State * L );

    // set_expression_syntax( syntax )
    // sets the syntax that the engine will use to evaluate expressions
    // params:
    //   syntax: one of the values from the 'dbgeng.control.expr' table:
    //     MASM: expressions will be evaluated according to MASM syntax
    //     CPLUSPLUS: expressions will be evaluated according to C++ syntax
    // returns:
    //   true if successful
    int l_set_expression_syntax( lua_State * L );

    // set_expression_syntax_by_name( syntax )
    // sets the syntax that the engine will use to evaluate expressions, by syntax name
    // params:
    //   syntax: one of the following string values:
    //     "MASM": expressions will be evaluated according to MASM syntax
    //     "CPLUSPLUS": expressions will be evaluated according to C++ syntax
    // returns:
    //   true if successful
    int l_set_expression_syntax_by_name( lua_State * L );

    // set_interrupt( flags )
    // registers a user interrupt or breaks into the debugger
    // params:
    //   flags: one of the values from the 'dbgeng.control.interrupt' table:
    //     ACTIVE: if the target is running, the engine will request a break into the debugger. when
    //       the target is suspended, the engine will register a user interrupt.
    //     PASSIVE: the engine will register a user interrupt.
    //     EXIT: see https://msdn.microsoft.com/en-us/library/ff556722.aspx
    // returns:
    //   true if successful
    int l_set_interrupt( lua_State * L );

    // [-0,+0]
    // dbgeng::control api init
    void xlua_init( lua_State * L );

}} // namespace dbgeng::control
