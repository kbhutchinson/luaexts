////////////////////////////////////////////////////////////////////////////////
// Utility classes and functions
//

#pragma once

#include <string>
#include <memory>
#include <sstream>
#include <filesystem>
#include <lua.hpp>

namespace cppobj
{
    enum class kind_t;
}
class cppobj_impl;

enum SymTagEnum;

namespace util
{

namespace filesystem = std::tr2::sys;

// returns the full path, including DLL filename, that this DLL was loaded from
filesystem::path get_dll_path();

// returns a path to the user's home directory
filesystem::path get_home_path();

template < typename T >
struct buffer
{
    const size_t size;
    const std::unique_ptr< T [] > ptr;

    explicit buffer( size_t size )
        : size( size )
        , ptr( new T [ size ] )
    {
        memset( ptr.get(), 0, size );
    }
    ~buffer() {}

    operator T * () const
    {
        return ptr.get();
    }

    T & operator [] ( size_t pos ) const
    {
        return ptr[ pos ];
    }
};

typedef buffer< char > charbuf;

// enum to classify fundamental types
enum class basetype_t
{
    none_,
    bool_,
    char_,
    uchar_,
    wchar_t_,
    char16_t_,
    char32_t_,
    short_,
    ushort_,
    int_,
    uint_,
    long_,
    ulong_,
    long64_,
    ulong64_,
    float_,
    double_,
    ldouble_,

    last_ = ldouble_,
};

// convenience functions for working with basetype_t's
basetype_t type_basetype( std::string const & typname );
basetype_t object_basetype( cppobj_impl const & object );
bool is_integral_type( basetype_t t );
bool is_unsigned_type( basetype_t t );
ULONG basetype_id( basetype_t t );

namespace symtagenum
{
    std::string stringify( enum SymTagEnum val );

    // similar to the above, but the string returned more closely matches Lua's type() function
    std::string tostring( enum SymTagEnum val );

    // translated to cppobj's kind concept
    cppobj::kind_t tokind( enum SymTagEnum val );
}

////////////////////////////////////////////////////////////////////////////////
// make_string
// Easily make a std::string from anything streamable, in one line.
// Found at: http://stackoverflow.com/a/6656127/301729
class make_string
{
    std::ostringstream buffer;

public:
    template < typename T >
    make_string & operator<<( T const & obj )
    {
        buffer << obj;
        return *this;
    }

    std::string str() const
    {
        return buffer.str();
    }

    operator std::string() const
    {
        return str();
    }

    // this pointer should not be saved
    // copy the contents out immediately
    char const * const c_str() const
    {
        return str().c_str();
    }
};


////////////////////////////////////////////////////////////////////////////////
// sign_cast and push_signed_integral implementations

// takes an unsigned value holding a signed integral and the size of the signed value and
// casts it to a signed value of the desired result type by first casting it to a signed
// value of the correct size, so that negatives get converted correctly
template < typename T, typename U, typename V >
T sign_cast( U const unsigned_value, V const size )
{
    static_assert( std::is_signed< T >::value, "T must be a signed type" );
    static_assert( std::is_unsigned< U >::value, "U must be an unsigned type" );

    auto const value = static_cast< T >(
        sizeof( LONG64    ) == size ? static_cast< LONG64    >( unsigned_value ) :
        sizeof( long long ) == size ? static_cast< long long >( unsigned_value ) :
        sizeof( long      ) == size ? static_cast< long      >( unsigned_value ) :
        sizeof( int       ) == size ? static_cast< int       >( unsigned_value ) :
        sizeof( short     ) == size ? static_cast< short     >( unsigned_value ) :
        sizeof( char      ) == size ? static_cast< char      >( unsigned_value ) : 0
    );

    if ( 0 == value && 0 != unsigned_value )
        throw std::invalid_argument( make_string() << "sign_cast: size argument '" << size << "' did not match a known type" );

    return value;
}

// takes an unsigned integral holding a signed integral and the size of the signed value and
// tries to cast it and push it safely
// returns 0 if it failed, or the number of values pushed if successful
template < typename U, typename V >
int push_signed_integral( lua_State * L, U const unsigned_value, V const size )
{
    static_assert( std::is_unsigned< U >::value, "U must be an unsigned type" );

    if ( size <= sizeof( lua_Integer ) )
    {
        auto const value = sign_cast< lua_Integer >( unsigned_value, size );
        lua_pushinteger( L, value );
        return 1;
    }

    // okay, the size of our unsigned value is larger than lua_Integer, but maybe this
    // specific value can fit
    typedef std::make_signed_t< U > signed_t;
    auto const value = sign_cast< signed_t >( unsigned_value, size );
    if ( std::numeric_limits< lua_Integer >::min() <= value && value <= std::numeric_limits< lua_Integer >::max() )
    {
        lua_pushinteger( L, static_cast< lua_Integer >( value ) );
        return 1;
    }

    // okay, even though it may involve some loss of precision because lua_Number
    // is usually a floating-point type, see if it can fit in a lua_Number
    if ( size <= sizeof( lua_Number ) )
    {
        lua_pushnumber( L, static_cast< lua_Number >( value ) );
        return 1;
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////
// declval enables the use of non-static class members in a decltype expression
// without actually needing an object of the appropriate type or faking the
// construction of one. declval can only be used in an unevaluated context, such
// as decltype or sizeof. This one is part of C++11, but didn't make it into VS2010.
// Examples:
//   decltype( util::declval< someclass >().memberdata )
//   decltype( util::declval< someclass >().memberfunc() )
template < class T >
typename std::add_rvalue_reference< T >::type declval()
{
    static_assert( false, "declval can only be used in an unevaluated context, such as decltype or sizeof" );
}

} // namespace util
