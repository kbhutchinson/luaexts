#pragma once

struct lua_State;

// Lua projections for the ExtRemoteData class
namespace dbgeng { namespace extremotedata
{
    // C++ side of an extremotedata userdata
    typedef std::weak_ptr< ExtRemoteData > extremotedata_userdatum;

    // [-0,+0]
    // if the value at the given index is an extremotedata userdata, returns the associated
    // wrapped pointer. else reports error.
    extremotedata_userdatum xlua_check_extremotedata( lua_State * L, int arg );

    // [-0,+1]
    // pushes:
    //   new userdata representing an ExtRemoteData, with associated metatable
    void xlua_push_extremotedata( lua_State * L, std::shared_ptr< ExtRemoteData > sperd );

    // get_boolean( erd )
    //
    // Returns a BOOLEAN version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   BOOLEAN version of the extremotedata object, as a number
    int l_get_boolean( lua_State * L );

    // get_char( erd )
    //
    // Returns a CHAR version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   CHAR version of the extremotedata object, as a number
    int l_get_char( lua_State * L );

    // get_data( erd, size )
    //
    // Returns contents of the target's memory, as represented by the extremotedata object.
    // The size of the requested data must match the size specified with 'new()' or 'set()' methods.
    // params:
    //   erd: extremotedata to retrieve the value from
    //   size: size of requested data in bytes
    // returns:
    //   Contents of the target's memory, as a number
    int l_get_data( lua_State * L );

    // get_double( erd )
    //
    // Returns a double version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   double version of the extremotedata object, as a number
    int l_get_double( lua_State * L );

    // get_float( erd )
    //
    // Returns a float version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   float version of the extremotedata object, as a number
    int l_get_float( lua_State * L );

    // get_long( erd )
    //
    // Returns a LONG version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   LONG version of the extremotedata object, as a number
    int l_get_long( lua_State * L );

    // get_long_ptr( erd )
    //
    // Returns a signed integer version (extended to LONG64) of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   Signed integer version of the extremotedata object, as a number
    int l_get_long_ptr( lua_State * L );

    // get_long64( erd )
    //
    // Returns a LONG64 version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   LONG64 version of the extremotedata object, as a number
    int l_get_long64( lua_State * L );

    // get_ptr( erd )
    //
    // Returns a ULONG64 pointer version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   Pointer version of the extremotedata object, as a number
    int l_get_ptr( lua_State * L );

    // get_short( erd )
    //
    // Returns a SHORT version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   SHORT version of the extremotedata object, as a number
    int l_get_short( lua_State * L );

    // get_string( erd, num_chars )
    //
    // Reads a null-terminated string from the target's memory and returns it.
    // params:
    //   erd: extremotedata to retrieve the value from
    //   num_chars: maximum number of characters to read
    // returns:
    //   String located at the memory region represented by the extremotedata object
    int l_get_string( lua_State * L );

    // get_std_bool( erd )
    //
    // Returns a bool version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   bool version of the extremotedata object, as a Lua boolean
    int l_get_std_bool( lua_State * L );

    // get_uchar( erd )
    //
    // Returns a UCHAR version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   UCHAR version of the extremotedata object, as a number
    int l_get_uchar( lua_State * L );

    // get_ulong( erd )
    //
    // Returns a ULONG version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   ULONG version of the extremotedata object, as a number
    int l_get_ulong( lua_State * L );

    // get_ulong_ptr( erd )
    //
    // Returns an unsigned integer version (extended to ULONG64) of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   Unsigned integer version of the extremotedata object, as a number
    int l_get_ulong_ptr( lua_State * L );

    // get_ulong64( erd )
    //
    // Returns a ULONG64 version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   ULONG64 version of the extremotedata object, as a number
    int l_get_ulong64( lua_State * L );

    // get_ushort( erd )
    //
    // Returns a USHORT version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   USHORT version of the extremotedata object, as a number
    int l_get_ushort( lua_State * L );

    // get_w32_bool( erd )
    //
    // Returns a BOOL version of the extremotedata object.
    // params:
    //   erd: extremotedata to retrieve the value from
    // returns:
    //   BOOL version of the extremotedata object, as a number
    int l_get_w32_bool( lua_State * L );

    // new()
    // new( offset, size )
    // new( name, offset, size )
    //
    // Creates a new userdatum representing an ExtRemoteData object.
    // params:
    //   offset: memory offset of the target region
    //   size: size in bytes of the target region
    //   name: name of the target region. can be used to help identify the region and will inserted
    //     into error messages generated by the object
    // returns:
    //   new ExtRemoteData userdatum
    int l_new( lua_State * L );

    // [-0,+0]
    // dbgeng::extremotedata api init
    void xlua_init( lua_State * L );

}}
