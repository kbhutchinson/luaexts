// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define NOMINMAX

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <atlbase.h>

// Debugging stuff
#include <engextcpp.hpp>
#define _NO_CVCONST_H // to get SymTagEnum
#include <dbghelp.h>

// STL
#include <string>
#include <vector>
#include <map>
#include <deque>
#include <set>
#include <stack>
#include <memory>
#include <functional>
#include <algorithm>
#include <type_traits>
#include <utility>
#include <tuple>
#include <ostream>
#include <cstdint>
#include <iterator>
#include <iomanip>
#include <filesystem>
