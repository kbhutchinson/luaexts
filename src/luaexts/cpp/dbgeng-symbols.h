#pragma once

struct lua_State;

// Lua projections for the IDebugSymbols interface
namespace dbgeng { namespace symbols
{
    // create_symbol_group()
    // creates a new symbol group
    // returns:
    //   new userdata representing a symbol group
    int l_create_symbol_group( lua_State * L );

    // get_module_name_string( which, index, base )
    // returns the name of the specified module
    // params:
    //   which: which of the module's names to return; can be a value
    //     from the 'dbgeng.control.modname' table:
    //     IMAGE: name of the executable file, including the extension
    //     MODULE: usually just the file name without the extension
    //     LOADED_IMAGE: same as the image name
    //     SYMBOL_FILE: symbol file name; if no symbols have been loaded, this is the name of the executable file instead
    //     MAPPED_IMAGE: mapped image name
    //  index: index of the module; if set to 'dbgeng.ANY_ID', the 'base' parameter is used instead
    //  base: if 'index' parameter is 'dbgeng.ANY_ID', specifies the module base address; otherwise, it's ignored
    int l_get_module_name_string( lua_State * L );

    // get_symbol_type_id( symbol )
    // returns the module and type id for a symbol
    // params:
    //   symbol: symbol expression
    // returns:
    //   module base address
    //   type id
    int l_get_symbol_type_id( lua_State * L );

    // get_type_id( module, typename )
    // returns the type id for a type given its module and name
    // params:
    //   module: module base address
    //   typename: type name or symbol expression
    // returns:
    //   type id
    int l_get_type_id( lua_State * L );

    // get_type_name( module, typeid )
    // returns the name of a type specified by its module and type id
    // params:
    //   module: module base address
    //   typeid: type id
    // returns:
    //   type name
    int l_get_type_name( lua_State * L );

    // get_type_size( module, typeid )
    // returns the size of a type specified by its module and type id
    // params:
    //   module base address
    //   type id
    // returns:
    //   type size
    int l_get_type_size( lua_State * L );

    // [-0,+0]
    // dbgeng::symbols api init
    void xlua_init( lua_State * L );

}} // namespace dbgeng::symbols
